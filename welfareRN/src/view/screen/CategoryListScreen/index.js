
import React, {Component} from 'react';
import {FlatList, ActivityIndicator, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import WelfareListItem from 'src/view/components/WelfareListItem';

export default class CategoryListScreen extends Component<Props> {
  constructor(props){
	  super(props);
  }

  _onItemClick = (pos) => {
    this.props.onWelfareInfoClick(pos);
  }

  _renderItem = ({item}) => (
    <WelfareListItem
      title={item.welName}
      icon={item.icon}
      pos={item.i}
      desires={item.desires}
      onClick={this._onItemClick}
      targetCharacters={item.targetCharacters}
    />
  )

  render() {
    let {
      welfareList,
      category,
      keyword,
      loading,
      onEndReached,
    } = this.props;

  	let header = (
  	  <View>
         { (category && keyword) &&
  	  	  <BasicText title={category ? category + "복지 찾아보기" : "'" + keyword + "' 검색결과"}/>
         }
        <FlatList 
          data={welfareList}
          horizontal={false}
          renderItem={this._renderItem}
          onEndReached={onEndReached}
          style={{
            marginTop: 20,
            marginBottom: 20,
          }}
          />
      </View>
  	);

    let progress;

    if (loading) {
      progress = (
        <View 
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center', 
            aliginItems: 'center'}}>
          <ActivityIndicator size="large" color='#00cf7b'/>
        </View>
      );
    }

    return(
  		<View style={{
  			backgroundColor: '#f4f4f4',
  			flex: 1,
  			flexDirection: 'column',
  		}}>
  	      <View 
  		    style={{
  		      flex: 1,
  		      marginTop: 20,
  		      marginLeft: 20,
  		      marginRight: 20,
  		    }}>
          {header}
          {progress}
  		  </View>
  		</View>
  	);
  }
}