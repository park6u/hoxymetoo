import React, {Component} from 'react';
import {AsyncStorage, Dimensions, Animated, ActivityIndicator, ScrollView, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import PasswordNumericButton from 'src/view/components/PasswordNumericButton';
import InitialBackground from 'src/view/components/InitialBackground';

export default class PasswordScreen extends React.Component {
  state = {
    yTransition: new Animated.Value(0),
    page : 0,
    modal: undefined
  }

  constructor(props) {
  	super(props);
  }

  componentDidMount() {
    const {
      yTransition
    } = this.state;

    Animated.spring(yTransition, {
      toValue: 1,
      friction: 6
    }).start();
  }

  // Modal(Animated.View)의 Width 와 Height의 값을 State에 저장
  saveModalDimen = (e) => {
    if (this.state.modal) return; // already declared
    let {width, height} = e.nativeEvent.layout;
    this.setState({modal: {width, height}});
  }

  render() {
    const {
      onButtonClick,
      passwordLength
    } = this.props;

      let {
        yTransition
      } = this.state;

    let screenHeight = Dimensions.get('window').height;
    let negativeHeight = -screenHeight + 20;
    let modalMoveY = yTransition.interpolate({
         inputRange: [0, 1],
         outputRange: [0, negativeHeight]
      });

    if (this.state.modal) {
      var modalWidth;
      var modalHeight;
      modalWidth = this.state.modal.width;
      modalHeight = this.state.modal.height;
    }

	  return (
      <View 
        style={{
          flex: 1,
          justifyContent: 'center'
        }}>
        <InitialBackground title="비밀번호">
        </InitialBackground>
        <Animated.View 
          onLayout={this.saveModalDimen}
          style={{
            flex: 1,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            position: 'absolute',
            bottom: -screenHeight,
            alignSelf: 'center',
            backgroundColor: '#fff',
            justifyContent: 'center',
            width: '88.8%',
            height: '80%',
            alignItems: 'center',
            transform: [{translateY : modalMoveY}]
          }}>
          <View 
            style={{
              width: '100%',
              height: '20%',
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row'
            }}>
            <View 
              style={{
                width: 32,
                height: 32,
                borderRadius: 32,
                margin: 16,
                backgroundColor: passwordLength < 1 ? '#e5e5e5' : '#00cf7b'
              }}>
            </View>
            <View 
              style={{
                width: 32,
                height: 32,
                borderRadius: 32,
                margin: 16,
                backgroundColor: passwordLength < 2 ? '#e5e5e5' : '#00cf7b'
              }}>
            </View>
            <View 
              style={{
                width: 32,
                height: 32,
                borderRadius: 32,
                margin: 16,
                backgroundColor: passwordLength < 3 ? '#e5e5e5' : '#00cf7b'
              }}>
            </View>
            <View 
              style={{
                width: 32,
                height: 32,
                borderRadius: 32,
                margin: 16,
                backgroundColor: passwordLength < 4 ? '#e5e5e5' : '#00cf7b'
              }}>
            </View>
          </View>
          <View 
            style={{
              width: '100%',
              height: '80%',
              flexDirection: 'column',
            }}>
            <View 
              style={{
                flex: 1,
                flexDirection: 'row',   
              }}>
              <PasswordNumericButton title="9" onPress={onButtonClick}/>
              <PasswordNumericButton title="8" onPress={onButtonClick}/>
              <PasswordNumericButton title="7" onPress={onButtonClick}/>
            </View>
            <View 
              style={{
                flex: 1,
                flexDirection: 'row',
              }}>
              <PasswordNumericButton title="6" onPress={onButtonClick}/>
              <PasswordNumericButton title="5" onPress={onButtonClick}/>
              <PasswordNumericButton title="4" onPress={onButtonClick}/>
            </View>
            <View 
              style={{
                flex: 1,
                flexDirection: 'row',
              }}>
              <PasswordNumericButton title="3" onPress={onButtonClick}/>
              <PasswordNumericButton title="2" onPress={onButtonClick}/>
              <PasswordNumericButton title="1" onPress={onButtonClick}/>    
            </View>
            <View 
              style={{
                flex: 1,
                flexDirection: 'row',                
              }}>
              <PasswordNumericButton title=""/>
              <PasswordNumericButton title="0" onPress={onButtonClick}/>
              <PasswordNumericButton title="←" onPress={onButtonClick}/>
            </View>
          </View>
        </Animated.View>
      </View>
	  );
  }
}
