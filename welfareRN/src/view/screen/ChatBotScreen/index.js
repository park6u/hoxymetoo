/*
프로그램 ID:AC-2020-JS
프로그램명:MainScreen/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.14
버전:0.6
설명
- 챗봇 스크린
*/

import React, {Component} from 'react';
import {Alert, TextInput, KeyboardAvoidingView, Image, ScrollView, Platform, StyleSheet, Text, View, Button} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';
import MessageBox from 'src/view/components/MessageBox';
import MessageListBox from 'src/view/components/MessageBox/MessageListBox';
import MessageResultListBox from 'src/view/components/MessageBox/MessageResultListBox';

export default class ChatBotScreen extends React.Component {

  constructor(props){
	  super(props);
    // 노치 있는 ios 폰
    const fullsizeiOSDevice = ["iPhone10,3", "iPhone10,6", "iPhone11,2", "iPhone11,4", 
                               "iPhone11,6", "iPhone11,8", "iPhone12,1", "iPhone12,3", 
                               "iPhone12,5"];
    // navigationSpace,  iPhone X 이상 기종시 하단 입력바 Bottom Margin
    this.state = {beforeSender : '', navigationSpace: false, keyboardVerticalOffset: 65};
    if (Platform.OS == 'android') this.state.keyboardVerticalOffset = 0;
    DeviceInfo.getDeviceId().then(deviceId => {
      fullsizeiOSDevice.map((id) => {
        if (id == deviceId) {
          this.setState({navigationSpace: true});
          this.setState({keyboardVerticalOffset: 40})
        }
      })
    });

  }

  componentDidUpdate() {
    this.props.onChatBotMsgAddComplete();    
  }

  onSendClick = () => {
    if (this.props.textInput !== undefined && this.props.textInput !== null && this.props.textInput !== '') {
      this.props.onSendClick(this.props.textInput);
    }
  }

  scrollToEnd = () => {
    setTimeout(() => {
      this.refs._scollview.scrollToEnd();      
    }, 200);
  }

  render() {
    let {
      msgData,
      onSendClick,
      onChangeText,
      onMoreButtonClick,
      onMessageListClick,
      onMessageListResultClick,
    } = this.props;

    messageList = [];
    
    if (msgData) {
      for (var i = 0; i < msgData.length; ++i) {
        let tailStyle = false;
        if (i != 0) {
          tailStyle = this.state.beforeSender == msgData[i].sender ? true : false;
        }
        this.state.beforeSender = msgData[i].sender;

        if (msgData[i].sender == 'chatbot') {
          if (msgData[i].sendResultList){
            messageList.push(<MessageResultListBox 
                                messageList={msgData[i].sendResultList}
                                keyword={msgData[i].keyword}
                                queryString={msgData[i].queryString}
                                onMessageListResultClick={onMessageListResultClick}
                                onMoreButtonClick={onMoreButtonClick}/>);
          } else if (msgData[i].sendList) {
            messageList.push(<MessageListBox messageList={msgData[i].sendList} onMessageListClick={onMessageListClick}/>)
          } else {
            messageList.push(<MessageBox message={msgData[i].sendText} />);
          }
        } else {
          messageList.push(<MessageBox message={msgData[i].sendText} backgroundColor='#00cf7b' right={true} />);          
        }
      }
    }

    return(
    	<View style={{
    		backgroundColor: '#fff',
    		flex: 1,
    		flexDirection: 'column',
    	}}>
        <ScrollView 
          ref='_scollview'
          style={{
            marginBottom: this.state.navigationSpace ? 50 : 0,
          }}>
          {messageList}
        </ScrollView>
        <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : undefined} keyboardVerticalOffset={this.state.keyboardVerticalOffset}>
          <View
            style={{
              bottom: 0,
              borderTopWidth: 1,
              borderTopColor: '#e5e5e5',
              backgroundColor: '#fff',
              bottom: this.state.navigationSpace ? 50 : 0,
              width: '100%',
              height: 50,
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TextInput 
              placeholder="내용을 입력해주세요"
              value={this.props.textInput}
              onChangeText={onChangeText}
              style={{
                alignItems: 'center',
                paddingLeft: 20,
                width: '80%',
              }} />
            <View 
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                width:'20%'
              }}>
              <BasicButton 
                size={12}
                onPress={this.onSendClick}
                borderRadius={10} 
                title= '전송' 
                fontColor='#fff' />
            </View>
          </View>
        </KeyboardAvoidingView>
        {<View 
          style={{
            backgroundColor: '#fff',
            position: 'absolute',
            bottom: 0,
            height: 50,
          }}>
        </View> && this.state.navigationSpace }
    	</View>
    );
  }
}