/*
프로그램 ID:AC-2020-JS
프로그램명:navigation/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.06
버전:0.6
설명
- 스플래시 스크린
*/

import React, {Component} from 'react';
import {Image, Animated, Dimensions, StatusBar, StyleSheet, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import UserModel from 'src/model/UserModel';
import RegionModel from 'src/model/RegionModel';
import EntityModel from 'src/model/ChatBotModel/EntityModel';

export default class SplashScreen extends React.Component {
  state = {
    fadeInAnim: new Animated.Value(0),  // Initial value for opacity: 0
    loginCheckDone: false,
    waitDelayDone: false,
    registered: false,
    navigation: '',
  }

  // 생성자 
  constructor(props) {
	super(props);
	this._bootstrapAsync();
	this._getAllAddress();
	this._getAllEntity();
	this._update();
  }		

  // 애니메이션 로드, 0.1초, 투명도 0->1, 페이드인
  componentDidMount() {
	Animated.timing(
	  this.state.fadeInAnim,
	  {
		toValue: 1,
		duration: 400,
	  }
	).start();
  }

	// 최소 2초 이상 딜레이 후 화면 전환
  componentWillUpdate(nextProp, nextState) {
	if (nextState.loginCheckDone && nextState.waitDelayDone) {
	  this.props.navigation.navigate(nextState.navigation, {
	  	alreadyRegistered: this.state.registered,
      });
    }
  }

	// 로그인 체크: 내부 스토리지에서 사용자 토큰 확인 후 메인 화면 로딩
	_bootstrapAsync = async () => {
	  UserModel.getInternalMemkey((memkey) => {
	  	UserModel.getInternalName((name) => {
		  if (memkey !== null) {
	//	  	 this.setState({navigation: 'Password', loginCheckDone: true});
	        if (name == null) {
	          this.setState({navigation: 'Register', loginCheckDone: true, registered: true});
	        } else {
		  	  this.setState({navigation: 'Main', loginCheckDone: true});
		    }
		  } else {
	//          this.setState({navigation: 'Register', loginCheckDone: true});
		  	this.setState({navigation: 'Login', loginCheckDone: true});
		  }
	  	});
	  });
	}

	// 로그인 화면 전환
	_update = () => {
	  setTimeout(() => {
	  	this.setState({waitDelayDone: true});
	  }, 2000);
	}

    // 모든 주소정보를 불러옴
    _getAllAddress = async () => {
      RegionModel.getAllAddress((res) => {
        console.log('get all address complete'); 
      });
    }

    // 모든 Entity 정보를 불러옴
    _getAllEntity = async () => {
      EntityModel.getDesire(() => {});
      EntityModel.getHouseTypes(() => {});
      EntityModel.getLifeCycle(() => {});
      EntityModel.getTargetCharacters(() => {});
    }

	// 뷰 렌더링 
	render() {
		let {fadeInAnim} = this.state;
		let screenWidth = Dimensions.get('window').width;
		let screenHeight = Dimensions.get('window').height;
		return(
		 	<Animated.View
			 	style={{
			 		backgroundColor: '#00cf7b',
		 	        width: screenWidth,
		 	       	height: screenHeight,
		 	       	opacity: fadeInAnim,
		 	       	justifyContent: 'center',
		 	       	alignItmes: 'center'
		 	    }}
		 	 >
		 	 	<Image 
		 	 		source={require('assets/splash_logo2.png')}
		 	 		resizeMode="contain"
		 	 		style={{
		 	 			alignSelf: 'center',
		 	 			width: '30%',
		 	 			height: '30%'
		 	 		}}/>
		 	 </Animated.View>
			);
	}
}