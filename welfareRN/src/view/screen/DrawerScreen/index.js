/*
프로그램 ID:AC-2020-JS
프로그램명:DrawerScreen/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.14
버전:0.6
설명
-  생애주기 분류 드로워(Drawer) 스크린
*/

import React, {Component} from 'react';
import {ScrollView, TextInput, FlatList, ActivityIndicator, Platform, StyleSheet, Text, View, Button} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';
import LinearGradient from 'react-native-linear-gradient';
import BasicDropDown from 'src/view/components/BasicDropDown';
import BasicSlider from 'src/view/components/BasicSlider';
import BasicRadioButton from 'src/view/components/BasicRadioButton';
import BasicTextInput from 'src/view/components/BasicTextInput';

export default class DrawerScreen extends Component<Props> {
  
  state = { 
    drawer: undefined,
    singleSliderValues: [],
    multiSliderValues: [],
  }

  constructor(props){
	super(props);
    this._setCategory();
  }

  singleSliderValueCallback =(values) => {
    this.setState({singleSliderValues : values})
  }

  saveDrawerDimen = (e) => {
	if (this.state.drawer) return; // already declared
    let {width, height} = e.nativeEvent.layout;
	this.setState({drawer: {width, height}});
  }

  // ref로 Viewmodel에서 제어
  setGender = async (value) => {
  	if (value == 1) {
  	  this.refs._maleCheck._toggleCheck();
    } else {
      this.refs._femaleCheck._toggleCheck();    	
    }
  }

  // ref로 Viewmodel에서 제어
  setBohun = async (value) => {
  	if (value == 1) {
  	  this.refs._bohunTargetCheck._toggleCheck();
  	} else {
  	  this.refs._bohunNonTargetCheck._toggleCheck();
  	}
  }

  // 성별 체크시 호출
  _genderChangeListener = async (name, checked) => {
	  if (name == "male") {
	  	if (this.refs._femaleCheck.state.checked) {
	  		this.refs._femaleCheck._toggleCheck();
	  		this.props.onGenderChange(1);
	  	} else {
	  		if (checked)
		  		this.props.onGenderChange(1);
		  	else
		  		this.props.onGenderChange(-1);		  		
	  	}
	  }
	  else {
	  	if (this.refs._maleCheck.state.checked) {
	  		this.refs._maleCheck._toggleCheck();
	  		this.props.onGenderChange(2);
	  	} else {
	  		if (checked)
		  		this.props.onGenderChange(2);
		  	else
		  		this.props.onGenderChange(-1);		  		
	  	}
	  }
	}

  // 보훈 여부 체크시 호출 
  _bohunChangeListener = async (name, checked) => {
	 if (name == "bohunTarget") {
	  	if (this.refs._bohunNonTargetCheck.state.checked) {
	  		this.refs._bohunNonTargetCheck._toggleCheck();
	  		this.props.onBohunChange(1);
	  	} else {
	  		if (checked)
		  		this.props.onBohunChange(1);
		  	else
		  		this.props.onBohunChange(-1);		  		
	  	}
	  }
	  else {
	  	if (this.refs._bohunTargetCheck.state.checked) {
	  		this.refs._bohunTargetCheck._toggleCheck();
	  		this.props.onBohunChange(2);
	  	} else {
	  		if (checked)
		  		this.props.onBohunChange(2);
		  	else
		  		this.props.onBohunChange(-1);		  		
	  	}
	  }
  }  

  // 카테고리 값 초기화
  _setCategory = async () => {

  } 

  render() {
  	let {
  	  onSaveClick,
  	  saveEnabled,
  	  onGenderChange,
  	  gender,
  	  onYearChange,
  	  onMonthChange,
  	  onDayChange,
  	  onSidoChange,
  	  onSigunguChange,
  	  onBohunChange,
  	  onDesiresChange,
  	  onHouseTypeChange,  	  
  	  onTargetCharactersChange,
  	  onFamilyChange,
  	  onDisabledChange,
  	  years,
  	  yearsSelected,
  	  month,
  	  monthSelected,
  	  day,
  	  daySelected,
  	  disabled,
  	  disabledSelected,
  	  sido,
  	  sidoSelected,
  	  sigungu,
  	  sigunguSelected,
  	  bohun,
  	  desires,
  	  desiresSelected,
  	  houseType,
  	  houseTypeSelected,
  	  targetCharacters,
  	  targetCharactersSelected,
  	  family,
  	  familySelected
  	} = this.props;

	var drawerWidth = 10;
	if (this.state.drawer) {
//	  var height;
	  drawerWidth = this.state.drawer.width;
//	  height = this.state.drawer.height;
	}

	return(
		<View 
			onLayout={this.saveDrawerDimen}
			style={{
				flex: 1,
				flexDirection: 'column',
			}}>
			<View style={{
				flexDirection: 'column',
				width: '100%',
			}}>
			<LinearGradient
				colors={['#00cf7b', '#00b4a4']}
				start={{x: 0, y: 0}} end={{x: 1, y: 0}}
				style={{
					width: '100%',
					height: 50,
				}}>
			</LinearGradient>
			<LinearGradient
				colors={['#00cf7b', '#00b4a4']}
				start={{x: 0, y: 0}} end={{x: 1, y: 0}}
				style={{
					width: '100%',
					height: 60,
					justifyContent: 'center',
					alignItems: 'center',
					elevation: 4,
				}}> 
			<BasicText 
				title="개인맞춤정보" 
				size={20}
				fontColor='#fff'
				/>
			</LinearGradient>
			</View>
			<ScrollView>
			<View style={{
				marginTop: 20,
				marginStart: 20,
				marginEnd: 20,
			}}>	
				<View
			 	  style={{
			 	  	width: '100%',
			 	  	alignItems: 'center',
			 	  	marginTop: Platform.OS == 'android' ? 20 : 0,
			 	  	marginBottom: 20,
			 	  }}>
					<BasicButton title="변경 저장" onPress={onSaveClick} enabled={saveEnabled}/>
				</View>
				<BasicText title='성별' size={18}/>
				<View style={{
					marginTop: 10,
					marginBottom: 20,
					flexDirection: 'row',
					justifyContent: 'flex-start',
				}}>
					<BasicRadioButton ref="_maleCheck" name="male" onChange={this._genderChangeListener} />
					<BasicText title="남성" marginLeft={10}/> 
					<BasicRadioButton ref="_femaleCheck" name="female" onChange={this._genderChangeListener} marginLeft={30}/>
					<BasicText title="여성" marginLeft={10}/>
				</View>
				<BasicText title="생년월일" size={18}/>
				<View style={{marginTop: 10}}>
					<BasicDropDown
						onChangeText={onYearChange}
						value={yearsSelected} 
						data={years}/>
				</View>
				<View 
					style={{
						marginTop: 10, 
						marginEnd: 20,
						flexDirection: 'row'}}>
					<View style={{width: '50%', marginRight: 10}}>
					<BasicDropDown
						onChangeText={onMonthChange}
						value={monthSelected} 
						data={month}
						/>
					</View>
					<View style={{width: '50%', marginLeft: 10}}>
					<BasicDropDown
						onChangeText={onDayChange}
						value={daySelected} 
						data={day}
						/>
					</View>
				</View>
				<BasicText title="거주지" size={18} />
				<View 
					style={{
						marginTop: 10, 
						marginEnd: 20,
						flexDirection: 'row'}}>
					<View style={{width: '50%', marginRight: 10}}>
					<BasicDropDown
						onChangeText={onSidoChange}
						value={sidoSelected} 
						data={sido}
						/>
					</View>
					<View style={{width: '50%', marginLeft: 10}}>
					<BasicDropDown
						onChangeText={onSigunguChange}
						value={sigunguSelected}
						data={sigungu}
						/>
					</View>
				</View>
				<BasicText title="주거상황" size={18} />
				<View style={{marginTop: 10, marginBottom: 20,}}>
				<BasicDropDown
					onChangeText={onHouseTypeChange}
					value={houseTypeSelected} 
					data={houseType}
					/>					
				</View>

				<BasicText title="관심사" size={18} />
				<View style={{marginTop: 10, marginBottom: 20,}}>
				<BasicDropDown
					onChangeText={onDesiresChange}
					value={desiresSelected} 
					data={desires}
					/>					
				</View>

				<BasicText title="선정대상" size={18} />
				<View style={{marginTop: 10, marginBottom: 20,}}>
				<BasicDropDown
					onChangeText={onTargetCharactersChange}
					value={targetCharactersSelected} 
					data={targetCharacters}
					/>					
				</View>

				<BasicText title="장애여부" size={18} />
				<View style={{marginTop: 10, marginBottom: 20,}}>
				<BasicDropDown
					onChangeText={onDisabledChange}
					value={disabledSelected} 
					data={disabled}
					/>					
				</View>

				<BasicText title="가족관계" size={18} />
				<View style={{marginTop: 10, marginBottom: 20,}}>
				<BasicDropDown
					onChangeText={onFamilyChange}
					value={familySelected} 
					data={family}
					/>					
				</View>
				<BasicText title='보훈' size={18}/>
				<View style={{
					marginTop: 10,
					marginBottom: 20,
					flexDirection: 'row',
					justifyContent: 'flex-start',
				}}>
					<BasicRadioButton ref="_bohunTargetCheck" name="bohunTarget" onChange={this._bohunChangeListener} />
					<BasicText title="대상" marginLeft={10}/> 
					<BasicRadioButton ref="_bohunNonTargetCheck" name="bohunNonTarget" onChange={this._bohunChangeListener} marginLeft={30}/>
					<BasicText title="비대상" marginLeft={10}/>
				</View>
			</View>
			</ScrollView>
		</View>
	);
  }
}