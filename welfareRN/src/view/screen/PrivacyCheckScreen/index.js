import React, {Component} from 'react';
import {Animated, Dimensions, ActivityIndicator, ScrollView, Image, Platform, StyleSheet, Text, View, Button} from 'react-native';
import InitialBackground from 'src/view/components/InitialBackground';
import {DrawerActions} from 'react-navigation-drawer';
import {NavigationActions} from 'react-navigation';
import BackIcon from 'src/view/components/BackIcon';
import BasicText from 'src/view/components/BasicText';

export default class OpenSourceLicenseScreen extends React.Component {
  state = {
    yTransition: new Animated.Value(0),
    page : 0,
    modal: undefined
  }

  constructor(props){
    super(props)
  }

  componentDidMount(){
    const {
      yTransition
    } = this.state;

    Animated.spring(yTransition, {
    toValue: 1,
    friction: 6
    }).start();
  }

  // Modal(Animated.View)의 Width 와 Height의 값을 State에 저장
  saveModalDimen = (e) => {
    if (this.state.modal) return; // already declared
    let {width, height} = e.nativeEvent.layout;
    this.setState({modal: {width, height}});
  }

  render() {
    let {
      onCompletePress,
      onFirstNextPress,
      onSecondNextPress,
    } = this.props;

    let {
      yTransition,
    } = this.state;

    let screenHeight = Dimensions.get('window').height;
    let negativeHeight = -screenHeight + 20;
    let modalMoveY = yTransition.interpolate({
      inputRange: [0, 1],
      outputRange: [0, negativeHeight]
    });

    if (this.state.modal) {
    var modalWidth;
    var modalHeight;
    modalWidth = this.state.modal.width;
    modalHeight = this.state.modal.height;
    }

    return(
    <View 
      style={{
        flex: 1,
        justifyContent: 'center'
      }}>
      <InitialBackground>
      </InitialBackground>
      <Animated.View          
        onLayout={this.saveModalDimen}
        style={{
          flex: 1,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          position: 'absolute',
          bottom: -screenHeight,
          alignSelf: 'center',
          backgroundColor: '#fff',
          justifyContent: 'center',
          width: '88.8%',
          height: '80%',
          alignItems: 'center',
          transform: [{translateY : modalMoveY}]
        }}>
        <ScrollView>
        </ScrollView>
      </Animated.View>
    </View>
      );
  }  
}