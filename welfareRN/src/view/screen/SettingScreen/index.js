
import React, {Component} from 'react';
import {FlatList, Image, ScrollView, ActivityIndicator, Platform, StyleSheet, TouchableOpacity, Text, View, Dimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BasicText from 'src/view/components/BasicText';
import Card from 'src/view/components/CardBackground';

export default class SettingScreen extends Component<Props> {
  constructor(props){
	  super(props);
  }

  _renderItem = ({item}) => (
    <View 
      style={{
        flexDirection: "row"
      }}>
      <TouchableOpacity 
        onPress={() => {this.props.onSettingPress(item.index)}}
        style={{
          width: "80%",
          marginTop: 20,
        }}>
        <BasicText title={item.title} />
      </TouchableOpacity>
      <View 
        style={{
          width: "20%",
          marginTop: 20,
          alignItem: "end"
        }}>
        <BasicText title={item.subTitle} />
      </View>
    </View>
  )

  render() {
  	let {
  	  data,
  	} = this.props;

	return(
		<View>
      <Card title="앱 설정">
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
        />
      </Card>
		</View>
    );
  }
}