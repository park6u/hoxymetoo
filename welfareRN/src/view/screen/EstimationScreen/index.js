
import React, {Component} from 'react';
import {Alert, TextInput, FlatList, ActivityIndicator, Image, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';
import SlotText from 'src/view/components/SlotText';
import FloatingActionButton from 'src/view/components/FloatingActionButton';

export default class EstimationScreen extends Component<Props> {
  constructor(props){
	super(props);
  }

  componentDidMount() {
  	
  }

  render() {
  	let {
  	  loading,
  	  name,
  	  receivableMoney,
  	  onChatBotClick,
  	  onFindMoneyClick,
  	} = this.props;

  	let progress;
  	let estimation;
  	console.log(receivableMoney);

  	if (loading) {
  		progress = 
  		(<View 
            style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center', 
            aliginItems: 'center'}}>
          <ActivityIndicator size="large" color='#00cf7b'/>
        </View>);
  	} else {
  		estimation = 
  		(
  			<View style={{
				height: 400,
				backgroundColor: '#fff',
				justifyContent: 'center',
				alignItems: 'center',
			}}>
				<BasicText title= {name + '님이 수혜가능한'} size={18} alignItems='center' justifyContent='center' />
				<BasicText title='복지금액은 다음과 같습니다.' size={18} alignItems='center' justifyContent='center' />
				<View 
					style={{
						marginTop: 20,
						flexDirection:'row',
						alignItems: 'center',
					}}>
					<SlotText title={receivableMoney} />
					<BasicText title='원' />
				</View>
				<Image 
					resizeMode="contain"
					source={require('assets/illust/estimation.png')} 
					style={{
						marginTop: 30,
						height: 180,
						width: 180,
					}}/>
				<BasicButton title="찾으러가기" onPress={onFindMoneyClick} />
 			</View>
  		);
  	}

	return(
		<View style={{
			flex: 1,
			flexDirection: 'column',
			backgroundColor: '#e5e5e5',
		}}>
		  {progress}
		  {estimation}
	      <FloatingActionButton onClick={onChatBotClick} />			
		</View>
	);
  }
}