
import React, {Component} from 'react';
import {Alert, TextInput, FlatList, ActivityIndicator, Image, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';
import LinearGradient from 'react-native-linear-gradient';
import Card from 'src/view/components/CardBackground';
import SlotText from 'src/view/components/SlotText';
import FloatingActionButton from 'src/view/components/FloatingActionButton';

export default class DescriptionScreen extends Component<Props> {
  constructor(props){
	  super(props);
  }

  componentDidMount() {
  	
  }

  render() {
  	let {
  	  loading,
  	  name,
  	  receivableMoney,
      receivableMoneyDesc,
  	  onChatBotClick,
  	} = this.props;

	return(
		<View style={{
			flex: 1,
			flexDirection: 'column',
			backgroundColor: '#e5e5e5',
		}}>
      <LinearGradient 
        colors={['#00cf7b', '#00b4a4']}
        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
        style={{
          height: 120,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image source={require("assets/estimation_logo.png")} style={{width: 80, opacity: 0.6}} resizeMode="contain"/>
        <View 
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute'
          }}>
          <BasicText title={name + "님의 수혜가능 금액"} size={21} fontColor='#fff' textAlign='center'/>
        </View>
      </LinearGradient>
      <Card title='수혜가능금액'>
        <View> 
          <LinearGradient 
            colors={['#00cf7b', '#00b4a4']}
            start={{x: 0, y: 0}} end={{x: 1, y: 0}}
            style={{
              height: 120,
              width: '100%',
              borderRadius: 10,
              elevation: 4,
              marginTop: 20,
              alignItems: 'center',
              justifyContent: 'center',
          }}>
            <View style={{
              flexDirection:'row',
              alignItems: 'center',
            }}>
              <SlotText title={receivableMoney} color='#fff'/> 
              <BasicText title='원' fontColor='#fff'/>
            </View>
          </LinearGradient>
        </View>
      </Card>
      <Card title='금액상세'>
        <BasicText title={receivableMoneyDesc} marginTop={12} />
      </Card>
	   <FloatingActionButton onClick={onChatBotClick} />			
		</View>
	);
  }
}