
import React, {Component} from 'react';
import {View, Image} from 'react-native';
import BasicText from 'src/view/components/BasicText';

export default class AlarmListScreen extends Component<Props> {
  constructor(props) {
    super(props);
  }
 
  render() {
    return (
	  <View
	    style={{
		  backgroundColor: '#f4f4f4',
		  flex: 1,
		  flexDirection: 'column',
  		}}>
	    <View 
	      style={{
	      	flex: 1,
		    flexDirection: 'column',
		    alignItems: 'center',
		    justifyContent: 'center'
	      }}>
		  	<Image style={{height: 280, width: 140}} resizeMode="contain" source={require('assets/illust/empty.png')}/>
			<BasicText title="최신 복지 알림이 없습니다." marginTop={32} size={18} />
	  	</View>
	  </View>
	 );
  }
}