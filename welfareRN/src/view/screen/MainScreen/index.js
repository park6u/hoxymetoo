/*
프로그램 ID:AC-2020-JS
프로그램명:MainScreen/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.14
버전:0.6
설명
- 추천 복지 메인 스크린
*/

import React, {Component} from 'react';
import {Alert, TextInput, Image, FlatList, ActivityIndicator, Platform, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import FloatingActionButton from 'src/view/components/FloatingActionButton';
import WelfareListItem from 'src/view/components/WelfareListItem'


export default class MainScreen extends React.Component {

  constructor(props){
	  super(props);
  }

  _onItemClick = (pos) => {
    this.props.onWelfareInfoClick(pos);
  }

  _renderItem = ({item}) => (
    <WelfareListItem
      onClick={this._onItemClick}
      title={item.welName}
      pos={item.i}
      desires={item.desires}
      targetCharacters={item.targetCharacters}
    />
  )

  render() {
  	const {
      userName,
      onEndReached,
  	  onNavigationClick,
      onChatBotClick,
      welfareList,
      loading
  	} = this.props;


  let progress;
  let list;

  if (loading) {
    progress = (
      <View 
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center', 
          aliginItems: 'center'}}>
        <ActivityIndicator size="large" color='#00cf7b'/>
      </View>
    )
  } else {

    let illust = welfareList.length == 0 ? 
      (<View 
        style={{
          alignItems: 'center',
          justifyContent: 'center'
        }}>
        <Image style={{height: 280, width: 140}} resizeMode="contain" source={require('assets/illust/pull_drawer.png')} />
        <BasicText title="드로워를 당겨 다시 설정해보세요." marginTop={32} size={18} />
      </View>)
     : undefined;

    list = (
        <View 
          style={{
          }}>
          <BasicText title={userName+"님을 위한 맞춤복지"} />
          <FlatList 
            data={welfareList}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={onEndReached}
            renderItem={this._renderItem}
            style={{
              marginTop: 20,
              marginBottom: 20,
            }}
          />
        {illust}
        </View>
    )
  }


	return(
  		<View style={{
  			backgroundColor: '#f4f4f4',
  			flex: 1,
  			flexDirection: 'column',
  		}}>
        <View 
          style={{
            flex: 1,
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
          }}>
          {progress}
          {list}
        </View>
        <FloatingActionButton onClick={onChatBotClick}/>
  		</View>
	);
  }
}