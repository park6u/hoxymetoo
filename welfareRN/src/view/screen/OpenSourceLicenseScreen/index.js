import React, {Component} from 'react';
import {Image, Animated, Dimensions, StatusBar, StyleSheet, View} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {NavigationActions} from 'react-navigation';
import BackIcon from 'src/view/components/BackIcon';
import BasicText from 'src/view/components/BasicText';
import RNFS from 'react-native-fs';

export default class OpenSourceLicenseScreen extends React.Component {
  static navigationOptions = ({ navigation  }) => {

    const {state} = navigation;

    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

//    if(state.params != undefined){
        return { 
          headerTitle: <BasicText title="설정" size={18}/>,
          headerLeft: <BackIcon onPress={onBackIconClick} />,
          headerStyle: {
            elevation: 0,
            borderBottomWidth: 0,
          }
        }
//    }
  };

  constructor(props) {
    super(props);
    this.state = {text: undefined};
  }

  componentDidMount() {
  	RNFS.readFile(RNFS.DocumentDirectoryPath + "/assets/txt/opensource.txt", (res) => {
  	  console.log(res);
  	  this.setState({text: res});
  	});  	
  }

  render() {
    return(
      <BasicText title={this.state.text}/>
    );
  }
}