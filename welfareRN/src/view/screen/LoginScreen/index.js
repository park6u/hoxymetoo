/*
프로그램 ID:AC-2020-JS
프로그램명:LoginScreen/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.14
버전:0.6
설명
- 소셜 로그인 스크린
*/

import React, {Component} from 'react';
import {Alert, AsyncStorage, ActivityIndicator, TouchableHighlight, Animated, Image, Dimensions, StyleSheet, Text, View, Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import InitialBackground from 'src/view/components/InitialBackground';
import Ripple from 'react-native-material-ripple';

export default class LoginScreen extends React.Component {
	state = {
	  yTransition: new Animated.Value(0),
	}

	// 생성자
	constructor(props) {
	  super(props);
	}

	componentDidMount() {
	  const {
	    yTransition
	  } = this.state

	  Animated.spring(yTransition, {
	    toValue: 1,
		friction: 6
	  }).start();
	}

	render() {
	  //
      let {
      	buttonEnabled,
	    onGoogleLoginPress,
		onNaverLoginPress,
		onKakaoLoginPress,
		naverLoginEnabled,
		googleLoginEnabled,
		loading,
	  } = this.props;

	  let {
	  	yTransition
	  } = this.state;

	  let screenHeight = Dimensions.get('window').height;
	  let negativeHeight = -screenHeight + 20;
	  let modalMoveY = yTransition.interpolate({
        inputRange: [0, 1],
        outputRange: [0, negativeHeight]
      });

	  let indicator = null;
      if (loading) {
      	indicator = <ActivityIndicator size="large" color="#00cf7b" />;
      }
	
//	  let translateStyle = { transform: [{ translateY: modalMoveY }] };
		return(
			<View 
				style={{
					flex: 1,
					justifyContent: 'center'
				}}>
			<InitialBackground>
			</InitialBackground>
			<Animated.View 
				style={{
					flex: 1,
					borderTopLeftRadius: 10,
					borderTopRightRadius: 10,
					position: 'absolute',
					bottom: -screenHeight,
					alignSelf: 'center',
		 			backgroundColor: '#fff',
		 			justifyContent: 'center',
		 			width: '88.8%',
		 			height: '80%',
		 			alignItems: 'center',
		 			transform: [{translateY : modalMoveY}]
				}}>
				<Ripple
					onPressOut={onKakaoLoginPress}
					rippleContainerBorderRadius={30}
					disabled={!(buttonEnabled == undefined ||  buttonEnabled == true)}
					style={{
						width: '90%',
						paddingTop: 15,
						paddingBottom: 15,
						marginLeft: 40,
						marginRight: 40,
						borderRadius: 30,
						backgroundColor: '#fbe500',
						alignItems: 'center'
					}}
					>
					<View 
						style={{
						flexDirection: 'row',
						justifyContent: 'center',
						alignItems: 'center'
					}}>
						<Image 
						source={require('assets/logo/kakao.png')}
						style={{
							marginRight: 15,
							width: 30,
							height: 30
						}}/>
						<Text style={{
							fontWeight: 'bold',
							fontFamily: 'NanumSquareB',
							fontSize: 15,
							color: '#412e34',	
						}}>카카오톡 로그인</Text>
					</View>
				</Ripple>
				<Ripple 
					onPressOut={onNaverLoginPress}
					rippleContainerBorderRadius={30}
					disabled={!(buttonEnabled == undefined ||  buttonEnabled == true) || naverLoginEnabled}
					style={{
						width: '90%',
						paddingTop: 15,
						paddingBottom: 15,
						marginTop: 20,
						marginLeft: 40,
						marginRight: 40,
						borderRadius: 30,
						backgroundColor: '#00cd00',
						opacity: !naverLoginEnabled ? 0.6 : 1,
						alignItems: 'center'				
					}}>
					<View style={{
						flexDirection: 'row',
						justifyContent: 'center',
						alignItems: 'center'
					}}>
					<Image 
						source={require('assets/logo/naver.png')}
						style={{
							marginRight: 15,
							width: 30,
							height: 30
						}}/>
					<Text style={{
						fontWeight: 'bold',
						fontFamily: 'NanumSquareB',
						fontSize: 15,
						color: '#fff',	
					}}>네이버 로그인</Text>
					</View>
				</Ripple>
				<Ripple 
					onPressOut={onGoogleLoginPress}
					rippleContainerBorderRadius={30}
					disabled={!(buttonEnabled == undefined ||  buttonEnabled == true) || googleLoginEnabled}
					style={{
						width: '90%',
						paddingTop: 15,
						paddingBottom: 15,
						marginTop: 20,
						marginLeft: 40,
						marginRight: 40,
						borderRadius: 30,
						opacity: !naverLoginEnabled ? 0.6 : 1,
						backgroundColor: '#d74f35',
						alignItems: 'center'				
					}}>
					<View 
						style={{
						flexDirection: 'row',
						justifyContent: 'center',
						alignItems: 'center'
					}}>
						<Image 
							source={require('assets/logo/google.png')}
							resizeMode="contain"
							style={{
								marginRight: 15,
								width: 30,
								height: 30
							}}/>						
						<Text style={{
							fontWeight: 'bold',
							fontFamily: 'NanumSquareB',
							fontSize: 15,
							color: '#fff',	
						}}>구글 로그인</Text>
					</View>
				</Ripple>
				<View style={{width: '90%', marginTop: 20, alignItems: "center", justifyContent: "center"}}>
					{indicator}
				</View>
				<View 
					style={{
						position: 'absolute',
						top: -65,
						borderRadius: 65,
						backgroundColor: '#fff',
						width: 130,
						height: 130							
					}}>
				</View>
				<Image
					source={require('assets/logo/logo.png')}
					resizeMode="contain"
					style={{
						position: 'absolute',
						top: -40,
						width: 170,
						height: 170						
					}}
					/>
			</Animated.View>
			</View>	
		)
	}

}