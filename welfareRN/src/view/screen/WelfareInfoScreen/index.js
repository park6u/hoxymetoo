
import React, {Component} from 'react';
import {Alert, Image, ScrollView, ActivityIndicator, Platform, StyleSheet, Linking, Text, View, Dimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BasicText from 'src/view/components/BasicText';
import Ripple from 'react-native-material-ripple';
import Card from 'src/view/components/CardBackground';

export default class WelfareInfoScreen extends Component<Props> {

  constructor(props){
	super(props);
  }

  getWelNameFontSize = (len) => {
	// 복지 제목의 폰트사이즈: 24 ~ 32; 10 ~ 50   	
	const fontSizeList = [32, 31, 30, 29, 28, 27, 26, 25, 24];

	if (len < 45) {
		return fontSizeList[parseInt(len / 5)];
	} else {
		// 45 이상
		return 24;
	}
  } 

  render() {
  	let {
  	  data,
  	} = this.props;
	let {height, width} = Dimensions.get('window');


	return(
		<View style={{height: '100%'}}>
		<ScrollView style={{marginBottom: 80}}>
		<View 
			style={{
				backgroundColor: '#e5e5e5',
			}}>
			<LinearGradient 
				colors={['#00cf7b', '#00b4a4']}
				start={{x: 0, y: 0}} end={{x: 1, y: 0}}
				style={{
					height: 100,
					justifyContent: 'center',
					alignItems: 'center',
				}}>
				<View 
					style={{
						justifyContent: 'center',
						alignItems: 'center',
					}}>
				<BasicText title={data.welName} size={this.getWelNameFontSize(data.welName.length)} fontColor='#fff' textAlign='center'/>
				</View>
			</LinearGradient>
			<Card title='복지개요'>
				<View 
					style={{
						marginTop: 20,
					}}>
					<BasicText title={data.welSummary} style={{width:'80%'}}/>					
				</View>
			</Card>
			<Card title='복지상세'>
				<View 
					style={{
						marginTop: 20,
					}}>
					<BasicText title='대상'/>
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title={data.targetDetail} style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title='지원금' />
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title={data.supportMoney} style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title='지원방법' />
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title={data.howToApply} style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>
			</Card>
			{data.formName !== null && <Card title='관련서류' > 
				<View 
					style={{
						marginTop: 20,
					}}>
					<BasicText title={data.formName} onTextPress={()=>{ Linking.openURL(data.formLink)}} underline={true} fontColor='#00cf7b' style={{width:'80%', marginTop: 10}} bold={false}/>					
				</View>
			</Card>}
			{data.howToApplyImageLink !== null && <Card title='복지 지원절차'>
				<View 
					style={{
						marginTop: 20,
					}}>
					<BasicText title={data.howToApplyImageLink}/>
				</View>
			</Card>}
			<Card title='기타' >
				<View 
					style={{
						marginTop: 20,
					}}>
					<BasicText title='담당기관' />
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title={data.contactInfoName} style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title='기관전화번호' />
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title={data.contactInfoNumber} style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>
				{ data.siteName !== null && <View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title='관련사이트' />
				</View>}
				{ data.siteName !== null && <View 
					style={{
						marginTop: 10,
					}}>
					<BasicText onTextPress={()=>{Linking.openURL(data.siteLink)}}  underline={true} fontColor='#00cf7b' title={data.siteName} style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>}
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title='관련법령' />
				</View>
				<View 
					style={{
						marginTop: 10,
					}}>
					<BasicText title={data.lawName} onTextPress={()=>{Linking.openURL(data.lawLink)}} underline={true} fontColor='#00cf7b' style={{width:'80%', marginTop: 10}} bold={false}/>
				</View>
			</Card>
		</View>
		</ScrollView>
		<Ripple
			onPress={()=>{ Linking.openURL(data.crawlingSiteLink)}}
			style={{
				height: 70,
				position: 'absolute',
				width: '100%',
				bottom: 0,
				justifyContent: 'center',
				alignItems: 'center',
			}}
			>
			<LinearGradient 
					colors={['#00cf7b', '#00b4a4']}
					start={{x: 0, y: 0}} end={{x: 1, y: 0}}
					style={{
						height: 70,
						position: 'absolute',
						width: '100%',
						bottom: 0,
						justifyContent: 'center',
						alignItems: 'center',
					}}>
					<BasicText title='바로 신청하기' size={24} fontColor='#fff'/>
			</LinearGradient>
		</Ripple>
		</View>
	);
  }
}