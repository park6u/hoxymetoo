
import React, {Component} from 'react';
import {Alert, FlatList, ScrollView, ActivityIndicator, Image, Platform, TextInput, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import SearchIcon from 'src/view/components/SearchIcon';
import FloatingActionButton from 'src/view/components/FloatingActionButton';
import WelfareCategoryListItem from 'src/view/components/WelfareCategoryListItem'

export default class CategoryScreen extends Component<Props> {
  constructor(props){
	  super(props);
    this.state = {recommandKeywords:["실업수당", "육아", "건강보험", "교육비", "문화누리", "어르신"]}
  }

  _renderItem = ({item}) => (
    <WelfareCategoryListItem
      onClick={this.props.onCategoryClick}
      category={item.category}
      title={item.title}
      icon={item.icon}
      order={item.order}
    />
  )

  _searchIconClick = () => {
    this.props.onWelfareSearchIconClick(this.props.searchKeyword);
  }

  getRecommandKeywords = () => {
    return this.state.recommandKeywords[parseInt(Math.random() * 100) % this.state.recommandKeywords.length];
  }

  render() {
    const {
      onClick,
      onWelfareSearchIconClick,
      onCategoryClick,
      onChatBotClick,
      onSearchKeywordChange,
      searchKeyword,
    } = this.props;

  	let data =[
  		{title: '건강복지', category: '건강', icon: require('assets/icon/category_health.png'), order: 1},
  		{title: '고용복지', category: '고용', icon: require('assets/icon/category_employment.png'), order: 2},
  		{title: '교육복지', category: '교육', icon: require('assets/icon/category_education.png'), order: 3},
  		{title: '금융복지', category: '금융', icon: require('assets/icon/category_finance.png'), order: 4},
  		{title: '다문화복지', category: '다문화', icon: require('assets/icon/category_foreigner.png'), order: 5},
  		{title: '문화복지', category: '문화', icon: require('assets/icon/category_culture.png'), order: 6},
  		{title: '장애인복지', category: '장애인', icon: require('assets/icon/category_disabled.png'), order: 7},
  		{title: '주거복지', category: '주거', icon: require('assets/icon/category_house.png'), order: 8},
  	];


  	let list = (
  	  <View style={{marginTop: 20}}>
        <BasicText title="카테고리별 복지"/>
        <FlatList 
          data={data}
          horizontal={false}
          numColumns={2}
          renderItem={this._renderItem}
          style={{
            marginTop: 10,
            marginBottom: 20,
          }}
          />
      </View>
  	);

	return(
    <View>
    <ScrollView 
      showsVerticalScrollIndicator={false}>
  		<View style={{
  			backgroundColor: '#f4f4f4',
  			flexDirection: 'column',
  		}}>
  	      <View 
  		    style={{
  		      marginTop: 20,
  		      marginLeft: 20,
  		      marginRight: 20,
  		    }}>
            <BasicText title="복지 찾아보기"/>
            <View style={{
              marginTop: 20,
              height: 60,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#fff',
              borderRadius: 8,
              paddingLeft: 20,
              paddingRight: 20,
             }}>
               <TextInput 
                 placeholder={this.getRecommandKeywords()} 
                 value={searchKeyword} 
                 onChangeText={onSearchKeywordChange}
                 style={{height: 40, fontSize: Platform.OS == 'ios' ? 22 : 14, width: '90%'}}/>
               <SearchIcon onPress={this._searchIconClick} />
            </View>
            {list}
  		  </View>
  		</View>
    </ScrollView>
    <FloatingActionButton onClick={onChatBotClick}/>
    </View>
	);
  }
}