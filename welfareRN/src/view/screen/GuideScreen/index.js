/*
프로그램 ID:AC-2020-JS
프로그램명:GuideScreen/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
- 앱 가이드 스크린, 처음 회원가입 후 앱 사용 안내
*/

import React, {Component} from 'react';
import {Animated, Dimensions, ActivityIndicator, ScrollView, Image, Platform, StyleSheet, Text, View, Button} from 'react-native';
import InitialBackground from 'src/view/components/InitialBackground';
import ViewPager from 'src/view/components/ViewPager';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';

export default class GuideScreen extends Component<Props> {
	state = {
	  yTransition: new Animated.Value(0),
	  page : 0,
	  modal: undefined
	}

	constructor(props){
	  super(props)
	}

	componentDidMount(){
	  const {
	  	yTransition
	  } = this.state;

	  Animated.spring(yTransition, {
		toValue: 1,
		friction: 6
	  }).start();
	}

	setPageTo = (page) => {
	  const pageWidth = page * this.state.modal.width;
	  this.refs._scrollview.scrollTo({x: pageWidth, animated: true});
	}

	// Modal(Animated.View)의 Width 와 Height의 값을 State에 저장
	saveModalDimen = (e) => {
	  if (this.state.modal) return; // already declared
	  let {width, height} = e.nativeEvent.layout;
	  this.setState({modal: {width, height}});
	}

	render() {
	  let {
	  	onCompletePress,
	  	onFirstNextPress,
	  	onSecondNextPress,
	  } = this.props;

	  let {
	  	yTransition,
	  } = this.state;

	  let screenHeight = Dimensions.get('window').height;
	  let negativeHeight = -screenHeight + 20;
	  let modalMoveY = yTransition.interpolate({
	    inputRange: [0, 1],
	    outputRange: [0, negativeHeight]
	  });

	  if (this.state.modal) {
		var modalWidth;
		var modalHeight;
		modalWidth = this.state.modal.width;
		modalHeight = this.state.modal.height;
	  }

	  return(
		<View 
			style={{
				flex: 1,
				justifyContent: 'center'
			}}>
			<InitialBackground>
			</InitialBackground>
			<Animated.View 					
				onLayout={this.saveModalDimen}
				style={{
					flex: 1,
					borderTopLeftRadius: 10,
					borderTopRightRadius: 10,
					position: 'absolute',
					bottom: -screenHeight,
					alignSelf: 'center',
		 			backgroundColor: '#fff',
		 			justifyContent: 'center',
		 			width: '88.8%',
		 			height: '80%',
		 			alignItems: 'center',
	 				transform: [{translateY : modalMoveY}]
				}}>
				<ScrollView
					ref='_scrollview'
					horizontal={true}
					pagingEnabled={true}
					scrollEnabled={false}
					showsHorizontalScrollIndicator={false}
					scrollIndicatorInsets={{top: 10, left: 10, bottom: 10, right: 10}}
					scrollEventThrottle={10}
					>
					<View
						style={{
							backgroundColor: '#fff',
							width: modalWidth,
							flex: 1,
						}}>
						<View 
							style={{
								flexDirection: 'column',
								alignItems: 'center'
							}}>
						<BasicText title="챗봇으로 해결하는 복지서비스" marginTop={32} size={22} />
						<Image style={{width: 180, height: 360}} resizeMode="contain" source={require('assets/illust/screen1.png')}/>
						<BasicButton title="다음" onPress={onFirstNextPress} />
						</View>
					</View>
					<View
						style={{
							backgroundColor: '#fff',
							width: modalWidth,
							flex: 1,
						}}>
						<View 
							style={{
								flexDirection: 'column',
								alignItems: 'center'
							}}>
						<BasicText title="나에게 알맞는 맞춤복지" marginTop={32} size={22} />
						<Image style={{width: 180, height: 360}} resizeMode="contain" source={require('assets/illust/screen2.png')}/>
						<BasicButton title="다음" onPress={onSecondNextPress} />
						</View>
					</View>
					<View
						style={{
							backgroundColor: '#fff',
							width: modalWidth,
							flex: 1,
						}}>
						<View 
							style={{
								flexDirection: 'column',
								alignItems: 'center'
							}}>
						<BasicText title="맞춤복지 푸쉬 알림서비스" marginTop={32} size={22} />
						<Image style={{width: 180, height: 360}} resizeMode="contain" source={require('assets/illust/screen3.png')}/>
						<BasicButton title="복지 확인하기" onPress={onCompletePress}/>
						</View>
					</View>						
				</ScrollView>
			</Animated.View>
		</View>
		  );
	}
}