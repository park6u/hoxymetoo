/*
프로그램 ID:AC-2020-JS
프로그램명:Register/RegisterFirst/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 - DB 상에 회원정보가 이미 존재시 디바이스 내부에 저장하기 위한 이름 입력 스크린
*/

import React, {Component} from 'react';
import {Alert, TextInput, FlatList, ActivityIndicator, Platform, StyleSheet, Text, View, Button} from 'react-native';
import CheckBox from 'react-native-check-box';
import BasicText from 'src/view/components/BasicText';
import AlarmText from 'src/view/components/AlarmText';
import BasicButton from 'src/view/components/BasicButton';
import BasicTextInput from 'src/view/components/BasicTextInput';
import BasicCheckBox from 'src/view/components/BasicCheckBox';
import BasicRadioButton from 'src/view/components/BasicRadioButton';

export default class NameRegister extends Component<Props> {
	state = {
		name: '',
	}

	constructor(props){
	  super(props);
	}


	render() {
	  let {
	  	onFirstNextPress,
	  	nextEnabled,
	  	name,
	  	onNameChange,
	  	onNameRegister,
	  } = this.props;
	  
	  return(
		<View style={{
			flex: 1,
			flexDirection: 'column',
			alignItems: 'center',
		}}>
			<View style={{
				marginTop: 20,
				width: '90%',
				height: 150,
			}}>
				<BasicText title="이름"/>
				<View style={{marginTop: 10}}>
				<BasicTextInput 
					placeholder="이름"
					value={name}
					onChangeText={onNameChange}
					path={require('assets/icon/name.png')}/>
				</View>
				<AlarmText title="이름정보는 수집되지 않습니다."/>
			</View>
			<BasicButton title="완료" onPress={onNameRegister} enabled={nextEnabled}>
			</BasicButton>
		</View>
	  );
	}
}