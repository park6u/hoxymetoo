/*
프로그램 ID:AC-2020-JS
프로그램명:Register/RegisterFirst/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
- 회원가입 화면 첫번째 스크린 
*/

import React, {Component} from 'react';
import {Alert, TextInput, FlatList, ActivityIndicator, Platform, StyleSheet, Text, View, Button} from 'react-native';
import CheckBox from 'react-native-check-box';
import BasicText from 'src/view/components/BasicText';
import AlarmText from 'src/view/components/AlarmText';
import BasicButton from 'src/view/components/BasicButton';
import BasicTextInput from 'src/view/components/BasicTextInput';
import BasicCheckBox from 'src/view/components/BasicCheckBox';
import BasicRadioButton from 'src/view/components/BasicRadioButton';

export default class RegisterFirst extends Component<Props> {
	state = {
		name: '',
		email: '',
	}

	constructor(props){
	  super(props);
	}

	// 성별 변경 RadioCheckButton 
	_genderChangeListener = (name, checked) => {
	  if (name == "male") {
	  	if (this.refs._femaleCheck.state.checked) {
	  		this.refs._femaleCheck._toggleCheck();
	  		this.props.onGenderChange(1);
	  	} else {
	  		if (checked)
		  		this.props.onGenderChange(1);
		  	else
		  		this.props.onGenderChange(-1);		  		
	  	}
	  }
	  else {
	  	if (this.refs._maleCheck.state.checked) {
	  		this.refs._maleCheck._toggleCheck();
	  		this.props.onGenderChange(2);
	  	} else {
	  		if (checked)
		  		this.props.onGenderChange(2);
		  	else
		  		this.props.onGenderChange(-1);		  		
	  	}
	  }
	}

	render() {
	  let {
	  	onFirstNextPress,
	  	nextEnabled,
	  	name,
	  	onNameChange,
	  	gender,
	  	onGenderChange,
	  	email,
	  	onEmailChange,
	  	onEmailLaterCheckChange,
	  } = this.props;
	  return(
		<View style={{
			flex: 1,
			flexDirection: 'column',
			alignItems: 'center',
			marginBottom: 80
		}}>
			<View style={{
				marginTop: 20,
				width: '90%',
				height: 150,
			}}>
				<BasicText title="이름"/>
				<View style={{marginTop: 10}}>
				<BasicTextInput 
					placeholder="이름"
					value={name}
					onChangeText={onNameChange}
					path={require('assets/icon/name.png')}/>
				</View>
				<AlarmText title="이름정보는 수집되지 않습니다."/>
			</View>
			<View style={{
				width: '90%',
				height: 100,
				flexDirection: 'column',
			}}>
				<BasicText title="성별"/>
				<View style={{
					marginTop: 20,
					flex: 1, 
					flexDirection: 'row',
					justifyContent: 'flex-start',
				}}>
					<BasicRadioButton ref="_maleCheck" name="male" onChange={this._genderChangeListener}/>
					<BasicText title="남성" marginLeft={10}/>
					<BasicRadioButton ref="_femaleCheck" name="female" onChange={this._genderChangeListener} marginLeft={30}/>
					<BasicText title="여성" marginLeft={10}/>
				</View>
			</View>
			<View style={{
				width: '90%',
				height: 180,
			}}>
				<BasicText title="이메일"/>
				<View style={{marginTop: 10}}>
				<BasicTextInput 
					placeholder="swm@fkii.org" 
					value={email}
					maxLength={80}
					onChangeText={onEmailChange}
					path={require('assets/icon/email.png')}/>
				</View>
				<View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center'}}>
					<BasicCheckBox onChange={onEmailLaterCheckChange}/>
					<BasicText title="나중에 입력하기" marginLeft={10}/> 
				</View>
			</View>
			<BasicButton title="다음으로" onPress={onFirstNextPress} enabled={nextEnabled}>
			</BasicButton>
		</View>
	  );
	}
}