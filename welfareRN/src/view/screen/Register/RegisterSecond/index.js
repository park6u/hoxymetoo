/*
프로그램 ID:AC-2020-JS
프로그램명:Register/RegisterSecond/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
- 회원가입 화면 두번째 스크린 
*/

import React, {Component} from 'react';
import {Picker, StyleSheet, Text, View, Button, Image} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import AlarmText from 'src/view/components/AlarmText';
import BasicButton from 'src/view/components/BasicButton';
import BasicTextInput from 'src/view/components/BasicTextInput';
import BasicDropDown from 'src/view/components/BasicDropDown';
import { Dropdown } from 'react-native-material-dropdown';

export default class RegisterSecond extends Component<Props> {
  constructor(props){
	super(props);
  }

  render() {
  	const {
	  onSecondNextPress,
	  birthday,
	  onBirthdayChange,
	  sido,
	  sidoSelected,
	  onSiDoChange,
	  sigungu,
	  sigunguSelected,
	  onSiGunGuChange,
	  disabled,
	  onDisabledChange,
	  disabledKind,
	  onDisabledKindChange,
	  nextEnabled,
	} = this.props;

    let disabledKindDropDown = disabled == '장애인' ? <BasicDropDown
					ref='_disabledDropdown'
					onChangeText={onDisabledKindChange}
					value={disabledKind}
					data={disabledKindList}>
				</BasicDropDown> : null;


	return(
		<View 
			style={{
				flex: 1,
				flexDirection: 'column',
				alignItems: 'center',
				marginBottom: 80
			}}>
			<View style={{
				marginTop: 20,
				width: '90%',
				height: 120,
			}}>
				<BasicText title="주민등록번호 앞자리"/>
				<View style={{marginTop: 10}}>
				<BasicTextInput
					placeholder='881020'
					dataDetectorTypes='phoneNumber'
					maxLength={6}
					value={birthday}
					onChangeText={onBirthdayChange}
					>
				</BasicTextInput>
				<AlarmText title="생년월일은 복지혜택 맞춤 선정을 위한 용도로 수집됩니다."/>
				</View>
			</View>
			<View style={{
				marginTop: 20,
				marginBottom: 30,
				width: '90%',
				height: 110,
			}}>
				<BasicText title="주소"/>
				<View style={{
					flexDirection: 'row',
					marginTop: 10,
					width: '90%',
					flex: 1,
				}}>
					<View style={{width: '50%', marginRight: 10}}>
					<BasicDropDown
						ref='_addrDropdown1'
						value={sidoSelected}
						onChangeText={onSiDoChange}
						data={sido}>
					</BasicDropDown>
					</View>
					<View style={{width: '50%', marginLeft: 10}}>
					<BasicDropDown
						ref='_addrDropdown2'
						value={sigunguSelected}
						onChangeText={onSiGunGuChange}
						data={sigungu}>
					</BasicDropDown>					
					</View>
				</View>
				<AlarmText title="주소지는 복지헤택 맞춤선정을 위한 용도로 수집됩니다."/>
			</View>
			<BasicButton title="다음으로" onPress={onSecondNextPress} enabled={nextEnabled}>
			</BasicButton>
		</View>
	  );
  }
}