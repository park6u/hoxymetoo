/*
프로그램 ID:AC-2020-JS
프로그램명:Register/RegisterThird/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
- 회원가입 화면 세번째 스크린 
*/

import React, {Component} from 'react';
import {Alert, TextInput, FlatList, Linking, ActivityIndicator, Platform, StyleSheet, Text, View, Button} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import AlarmText from 'src/view/components/AlarmText';
import BasicButton from 'src/view/components/BasicButton';
import BasicDropDown from 'src/view/components/BasicDropDown';
import BasicCheckBox from 'src/view/components/BasicCheckBox';
import BasicRadioButton from 'src/view/components/BasicRadioButton';

export default class RegisterThird extends Component<Props> {
	constructor(props){
	  super(props);
	}

	_bohunChangeListener = (name, checked) => {
	  if (name == "target") {
	  	if (this.refs._nonTargetCheck.state.checked) {
	  		this.refs._nonTargetCheck._toggleCheck();
	  		this.props.onBohunChange(1);
	  	} else {
	  		if (checked)
		  		this.props.onBohunChange(1);
		  	else
		  		this.props.onBohunChange(-1);		  		
	  	}
	  }
	  else {
	  	if (this.refs._targetCheck.state.checked) {
	  		this.refs._targetCheck._toggleCheck();
	  		this.props.onBohunChange(2);
	  	} else {
	  		if (checked)
		  		this.props.onBohunChange(2);
		  	else
		  		this.props.onBohunChange(-1);		  		
	  	}
	  }
	}

	render() {
	  const {
        onRegisterComplete,
        family,
        onFamilyChange,
        disease,
        onDiseaseChange,
        bohun,
        onBohunChange,
        onPrivacyChange,
        nextEnabled,
	  } = this.props;


	  let familyList = [{
	  	value: '가족관계',
	  }, {
	  	value: '0명',
	  }, {
	  	value: '1명',
	  }, {
	  	value: '2명',
	  }, {
	  	value: '3명',
	  }, {
	  	value: '4명',
	  }, {
	  	value: '5명',
	  }, {
	  	value: '6명',
	  }];

	  return(
		<View style={{
			flex: 1,
			flexDirection: 'column',
			alignItems: 'center',
			marginBottom: 80
		}}>
			<View style={{
				marginTop: 20,
				width: '90%',
				height: 120,
			}}>
				<BasicText title="가족관계"/>
				<View style={{marginTop: 10}}>
				<BasicDropDown
					ref='_familyDropdown'
					onChangeText={onFamilyChange}
					value={family}
					data={familyList}>
				</BasicDropDown>
				</View>
				<AlarmText title="가족관계 정보는 복지혜택 맞춤 선정을 위한 용도로 사용됩니다." />
			</View>		 
			<View style={{
				marginTop: 20,
				width: '90%',
				height: 150,
			}}>
				<BasicText title="보훈대상"/>
				<View style={{
					marginTop: 20,
					flex: 1, 
					flexDirection: 'row',
					justifyContent: 'flex-start',
				}}>
					<BasicRadioButton ref="_targetCheck" name="target" onChange={this._bohunChangeListener}/>
					<BasicText title="대상" marginLeft={10}/> 
					<BasicRadioButton ref="_nonTargetCheck" name="non-target" onChange={this._bohunChangeListener} marginLeft={30}/>
					<BasicText title="비대상" marginLeft={10}/>
				</View>
			</View>
			<View style={{
				marginTop: 20,
				width: '90%',
				height: 150,
			}}>
			  <BasicText title="개인정보이용동의"/>
				<View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center'}}>
					<BasicCheckBox onChange={onPrivacyChange}/>
					<BasicText title="개인정보이용약관에 동의합니다." onTextPress={()=>{Linking.openURL("https://hoxymetoo.com/ko/privacy")}} underline={true} fontColor='#00cf7b' marginLeft={10}/> 
				</View>
			</View>
			<BasicButton title="가입완료" onPress={onRegisterComplete} enabled={nextEnabled}>
			</BasicButton>
		</View>
	  );
	}
}