/*
프로그램 ID:AC-2020-JS
프로그램명:Register/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
- 회원가입 스크린
*/

import React, {Component} from 'react';
import {Alert, AsyncStorage, Dimensions, Animated, ActivityIndicator, ScrollView, StyleSheet, Text, View, Button} from 'react-native';
import InitialBackground from 'src/view/components/InitialBackground';
import NameRegister from 'src/view/screen/Register/NameRegister';
import RegisterFirst from 'src/view/screen/Register/RegisterFirst';
import RegisterSecond from 'src/view/screen/Register/RegisterSecond';
import RegisterThird from 'src/view/screen/Register/RegisterThird';

export default class Register extends React.Component {
	state = {
	  yTransition: new Animated.Value(0),
	  page : 0,
	  modal: undefined
	}

	// 생성자
	constructor(props){
	  super(props);
	}		

	componentDidMount(){
	  const {
	  	yTransition
	  } = this.state;

	  Animated.spring(yTransition, {
		toValue: 1,
		friction: 6
	  }).start();
	}

	// Modal(Animated.View)의 Width 와 Height의 값을 State에 저장
	saveModalDimen = (e) => {
	  if (this.state.modal) return; // already declared
	  let {width, height} = e.nativeEvent.layout;
	  this.setState({modal: {width, height}});
	}

	setPageTo = (page) => {
	  const pageWidth = page * this.state.modal.width;
	  this.refs._scrollview.scrollTo({x: pageWidth, animated: true});
	}

	render() {
		const {
		  alreadyRegistered,
		  onNameRegister,
		  onFirstNextPress,
		  onSecondNextPress,
		  onRegisterComplete,
		  nextFirstEnabled,
		  name,
		  onNameChange,
		  gender,
		  onGenderChange,
		  email,
		  onEmailChange,
		  onEmailLaterCheckChange,
		  nextSecondEnabled,
		  birthday,
		  onBirthdayChange,
		  sido,
		  sidoSelected,
		  onSiDoChange,
		  sigungu,
		  sigunguSelected,
		  onSiGunGuChange,
		  disabled,
		  onDisabledChange,
		  disabledKind,
		  onDisabledKindChange,
		  nextCompleteEnabled,
		  family,
		  onFamilyChange,
		  disease,
		  onDiseaseChange,
		  bohun,
		  onBohunChange,
		  onPrivacyChange,
		  nextNameEnabled,
		} = this.props;

		const {
		  page
		} = this.state;

 	    let {
	  	  yTransition
	    } = this.state;

		let screenHeight = Dimensions.get('window').height;
		let negativeHeight = -screenHeight + 20;
		let modalMoveY = yTransition.interpolate({
	       inputRange: [0, 1],
	       outputRange: [0, negativeHeight]
	    });

		if (this.state.modal) {
			var modalWidth;
			var modalHeight;
			modalWidth = this.state.modal.width;
			modalHeight = this.state.modal.height;
		}

		return(
			<View 
				style={{
					flex: 1,
					justifyContent: 'center'
				}}>
				<InitialBackground title="회원가입">
				</InitialBackground>
				<Animated.View 
					onLayout={this.saveModalDimen}
					style={{
						flex: 1,
						borderTopLeftRadius: 10,
						borderTopRightRadius: 10,
						position: 'absolute',
						bottom: -screenHeight,
						alignSelf: 'center',
			 			backgroundColor: '#fff',
			 			justifyContent: 'center',
			 			width: '88.8%',
			 			height: '80%',
			 			alignItems: 'center',
		 				transform: [{translateY : modalMoveY}]
					}}>
					{!alreadyRegistered ? <ScrollView 
						ref='_scrollview'
						horizontal={true}
						pagingEnabled={true}
						scrollEnabled={false}
						showsHorizontalScrollIndicator={false}
						scrollIndicatorInsets={{top: 10, left: 10, bottom: 10, right: 10}}
						scrollEventThrottle={10}
						>
						<View
							style={{
								backgroundColor: '#fff',
								width: modalWidth,
								flex: 1,
							}}>
							<ScrollView>
							<RegisterFirst 
								ref='_registerFirst'
								name={name}
								onNameChange={onNameChange}
								email={email}
								onEmailChange={onEmailChange}
								onEmailLaterCheckChange={onEmailLaterCheckChange}
								gender={gender}
								onGenderChange={onGenderChange}
								nextEnabled={nextFirstEnabled}
								onFirstNextPress={onFirstNextPress}>
							</RegisterFirst>
							</ScrollView>
						</View>
						<View
							style={{
								backgroundColor: '#fff',
								width: modalWidth,
								flex: 1,
							}}>
							<ScrollView>							
							<RegisterSecond 
								ref='_registerSecond'
								birthday={birthday}
								onBirthdayChange={onBirthdayChange}
								sido={sido}
								sidoSelected={sidoSelected}
								onSiDoChange={onSiDoChange}
								sigungu={sigungu}
								sigunguSelected={sigunguSelected}
								onSiGunGuChange={onSiGunGuChange}
								disabled={disabled}
								onDisabledChange={onDisabledChange}
								disabledKind={disabledKind}
								onDisabledKindChange={onDisabledKindChange}
								nextEnabled={nextSecondEnabled}
								onSecondNextPress={onSecondNextPress}>
							</RegisterSecond>
							</ScrollView>							
						</View>
						<View
							style={{
								backgroundColor: '#fff',
								width: modalWidth,
								flex: 1,
							}}>
							<ScrollView>							
							<RegisterThird 
								ref='_registerComplete'
								family={family}
								onFamilyChange={onFamilyChange}
								disease={disease}
								onDiseaseChange={onDiseaseChange}
								bohun={bohun}
								onBohunChange={onBohunChange}
								onPrivacyChange={onPrivacyChange}
								nextEnabled={nextCompleteEnabled}
								onRegisterComplete={onRegisterComplete}>
							</RegisterThird>
							</ScrollView>							
						</View>
					</ScrollView> 
					:
					<View
						style={{
							backgroundColor: '#fff',
							width: modalWidth,
							flex: 1,
						}}>
						<NameRegister
						name={name}
						onNameChange={onNameChange}
						nextEnabled={nextNameEnabled}
						onNameRegister={onNameRegister}
						>
					</NameRegister> 
					</View>
					}
				</Animated.View>
			</View>
			);
	}
}