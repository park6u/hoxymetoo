
import React, {Component} from 'react';
import {Alert, TextInput, FlatList, Image, ActivityIndicator, Platform, StyleSheet, Text, View} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';

export default class MyPageScreen extends Component<Props> {
  constructor(props){
	super(props);	
  }

  render() {
  	let {
  	  loading,
  	  name,
  	  email,
  	  addr,
  	  gender,
  	  birthday,
  	  bohun,
  	  disabled,
  	  onSettingClick,
  	} = this.props;

  	let progress;
  	let info;
  	if (loading) {
  	  progress = (
  	  <View 
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center', 
          aliginItems: 'center'}}>
        <ActivityIndicator size="large" color='#00cf7b'/>
      </View>);
  	} else {
  	  info = (
  	  	<View 
			style={{
				width: '100%',
				margin: 20,
			}}>
			<View style={{
				flexDirection: 'row',
				width: '100%',
			}}>
			    <Image style={{width: 80, height: 80}} resizeMode="contain" source={require('assets/illust/profile.png')}/>
				<BasicText title={name} marginTop={20} marginLeft={20} size={32} spacing={10}/>
			</View>
			<View style={{
				width: '100%',				
				flexDirection: 'row',
				marginTop: 20,
				marginLeft: 20,
			}}>
				<View style={{width: '50%'}}>
				<BasicText title="성별" />
				</View>
				<View style={{width: '50%'}}>
				<BasicText title={gender == 1 ? '남성' : '여성'} bold={false} />
				</View>
			</View>			
			<View style={{
				width: '100%',
				flexDirection: 'row',
				marginTop: 20,
				marginLeft: 20,
			}}>
				<View style={{width: '50%'}}>
				<BasicText title="생년월일" />
				</View>
				<View style={{width: '50%'}}>
				<BasicText title={birthday ? birthday : undefined} bold={false}/>
				</View>
			</View>					
			<View style={{
				width: '100%',				
				flexDirection: 'row',
				marginTop: 20,
				marginLeft: 20,
			}}>
				<View style={{width: '50%'}}>
				<BasicText title="주소" />
				</View>
				<View style={{width: '50%'}}>
				<BasicText title={addr ? addr : undefined} bold={false} />
				</View>
			</View>					
			<View style={{
				width: '100%',				
				flexDirection: 'row',
				marginTop: 20,
				marginLeft: 20,				
			}}>
				<View style={{width: '50%'}}>
				<BasicText title="장애여부" />
				</View>
				<View style={{width: '50%'}}>
				<BasicText title={disabled ? '비대상' : '대상'} bold={false} />
				</View>
			</View>					
			<View style={{
				width: '100%',				
				flexDirection: 'row',
				marginTop: 20,
				marginLeft: 20,				
			}}>
				<View style={{width: '50%'}}>
				<BasicText title="보훈대상" />
				</View>
				<View style={{width: '50%'}}>
				<BasicText title={bohun == '1' ? '대상' : '비대상'} bold={false} />
				</View>
			</View>
			<View style={{
				width: '100%',				
				flexDirection: 'row',
				marginTop: 20,
				marginLeft: 20,				
			}}>
				<View style={{width: '50%'}}>
				<BasicText title="비밀번호" />
				</View>
				<View style={{width: '50%'}}>
				<BasicText title={bohun == '1' ? '대상' : '비대상'} bold={false} />
				</View>
			</View>
			<View style={{
				width: '100%',				
				flexDirection: 'row',
				marginTop: 40,			
			}}>
				<BasicButton title='앱 설정' onPress={onSettingClick}/>
			</View>
		</View>
  	  	);
  	}

	return(
		<View 
			style={{
				flex: 1,
				flexDirection: 'column',
				backgroundColor: '#e5e5e5',
				width: '100%',
			}}>
			<View style={{
				width: '100%',
				backgroundColor: '#fff',
			}}>
			{progress}
			{info}
			</View>
		</View>
	);
  }
}