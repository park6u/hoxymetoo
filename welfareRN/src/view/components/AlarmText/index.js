/*
프로그램 ID:AC-2020-JS
프로그램명:BasicText/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 기본 텍스트 컴포넌트
*/

import React, {Component} from 'react';
import {View, Text} from 'react-native';

export default class AlarmText extends React.Component {
  constructor(props){
  	super(props);
  }

  render(){
  	const {
  		title,
  		color,
  		size,
  		marginTop,
  		marginBottom,
  		marginLeft,
  		marginRight,
  		margin
  	} = this.props;

  	return(
        <Text 
          style={{
          marginTop: 10,
          fontSize: 12,
          fontFamily: 'NanumSquareR', 
          color: '#00cf7b'
          }}>
          {title}
        </Text>
  		);
  }
}