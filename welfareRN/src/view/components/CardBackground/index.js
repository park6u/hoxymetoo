
import React, {Component} from 'react';
import {Alert, StyleSheet, Text, View, Dimensions} from 'react-native';
import BasicText from 'src/view/components/BasicText';

export default class WelfareInfoScreen extends Component<Props> {
  constructor(props) {
  	super(props);
  }

  render() {
  	let {
  	  title,
  	  alignItems,
  	  ...otherProps
  	} = this.props;

  	let alignItemsStyle = alignItems;

  	return (
  		<View 
			style={{
				backgroundColor: '#fff',
				marginTop: 20,
				paddingLeft: 15,
				paddingRight: 15,
				paddingBottom: 20,
			}}>
			<View 
				style={{
					marginTop: 20,
				}}>
				<BasicText title={title} size={20}/>
			</View>	
			<View {...otherProps}>
			</View>		
		</View>
  	);
  }
}