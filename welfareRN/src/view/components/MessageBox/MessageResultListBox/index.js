/*
프로그램 ID:AC-2020-JS
프로그램명:MessageBox/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 
*/

import React, {Component} from 'react';
import {Alert, TextInput, FlatList, Image, Dimensions, StyleSheet, Text, View, Button} from 'react-native';
import Ripple from 'react-native-material-ripple';
import BasicText from 'src/view/components/BasicText';
import BasicButton from 'src/view/components/BasicButton';

export default class MessageResultListBox extends React.Component {
  constructor(props) {
  	super(props);
  }

  _onMessageListResultClick = (pos) => {
    this.props.onMessageListResultClick(pos);
  }

  _renderItem = ({item}) => (
    <Ripple
      onPress={() => (this._onMessageListResultClick(item.index))}
      rippleContainerBorderRadius={4}
      style={{
        margin: 6,        
        width: '90%'
      }}>
      <View 
        style={{
          backgroundColor: '#fff',
          borderRadius: 4,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <BasicText title={item.text.welName} fontColor='#000' size={18} margin={6} />
      </View>
    </Ripple>
  )

  _onMoreButtonClick = () => {
    this.props.onMoreButtonClick({
      keyword: this.props.keyword,
      queryString: this.props.queryString
    });
  }

  render() {
  	let {
  	  right,
  	  message,
      messageList,
      keyword,
      queryString,
  	  backgroundColor,
  	  tail,
      onMessageListClick,
      onMoreButtonClick,
  	} = this.props;

  	let backgroundColorStyle = backgroundColor ? backgroundColor : '#eef0f3'

    let {height, width} = Dimensions.get('window');

    // 메시지 박스의 가로길이; 
    let messageBoxMaxWitdth = width * 0.7;
  	let tailStyleBoxLeftMargin = (tail == undefined || tail == true) ? 20 : 60;
  	let tailStyleBoxRightMargin = (tail == undefined || tail == true) ? 20 : 60;
  	let tailStyleBoxTopMargin = (tail == undefined || tail == true) ? 10 : 5;
  	let tailStyleBoxBottomMargin = (tail == undefined || tail == true) ? 10 : 5;


  	let messageRender;
    let messageListRender = [];
    let messageListView = [];

    let listLength = messageList.length > 3 ? 3 : messageList.length;
    for (var i = 0; i < listLength; ++i) {
      messageListView.push({text: messageList[i], index: i});
    }
       
    messageListRender = (
      <FlatList
        data={messageListView}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
        style={{
          marginBottom: 10
        }}
      />
    );

    // 왼쪽 메시지
		messageRender = (
	  <View 
          style={{
            marginLeft: 20,
            marginTop: tailStyleBoxTopMargin,
            marginBottom: tailStyleBoxBottomMargin,
            flexDirection: 'row',
          }}>
          {(tail == undefined || tail == true) && <Image style={{
            width: 40,
            height: 40,
            borderWidth: 1,
            borderRadius: 20,
          }} resizeMode="contain" source={require('assets/illust/profile.png')} />}
          {(tail == false) && <View style={{height: 40}}></View>}            
          <View style={{
          	marginLeft: tailStyleBoxLeftMargin,
              flexDirection: 'row',
        }}>
            <View 
              style={{
              	left: 10,
                alignItems: 'center',
                maxWidth: messageBoxMaxWitdth,
                borderRadius: 4,
                backgroundColor: backgroundColorStyle,
              }}>
              {(tail == undefined || tail == true)  && <Image 
              source={require('assets/images/message_box_tail.png')}
              style={{
              	position: 'absolute',
              	height: 24,
              	left: -8,
              	top: 5,
              }}/>}
              {messageListRender}
              <BasicButton title="더 보기" onPress={this._onMoreButtonClick} margin={6} />
            </View>
          </View>
        </View>
			);
  	

  	return(
  		<View>
  		{messageRender}
  		</View>
  	);
  }
}