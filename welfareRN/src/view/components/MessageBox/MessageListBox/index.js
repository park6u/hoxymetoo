/*
프로그램 ID:AC-2020-JS
프로그램명:MessageBox/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 
*/

import React, {Component} from 'react';
import {Alert, TextInput, FlatList, Image, Dimensions, StyleSheet, Text, View, Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Ripple from 'react-native-material-ripple';
import BasicText from 'src/view/components/BasicText';

export default class MessageListBox extends React.Component {
  constructor(props) {
  	super(props);
  }

  _onMessageListClick = (pos) => {
    this.props.onMessageListClick(pos);
  }

  _renderItem = ({item}) => (
    <Ripple
      onPress={() => (this._onMessageListClick(item.index))}
      rippleContainerBorderRadius={4}
      style={{
        margin: 6,            
        width: '90%'
      }}>
      <LinearGradient 
        colors={['#00cf7b', '#00b4a4']}
        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
        style={{
          borderRadius: 4,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <BasicText title={item.text} fontColor='#fff' size={18} margin={6} />
      </LinearGradient>
    </Ripple>
  )

  render() {
  	let {
  	  right,
  	  message,
      messageList,
  	  backgroundColor,
  	  tail,
      onMessageListClick
  	} = this.props;

  	let backgroundColorStyle = backgroundColor ? backgroundColor : '#eef0f3'

    let {height, width} = Dimensions.get('window');

    // 메시지 박스의 가로길이; 
    let messageBoxMaxWitdth = width * 0.7;
  	let tailStyleBoxLeftMargin = (tail == undefined || tail == true) ? 20 : 60;
  	let tailStyleBoxRightMargin = (tail == undefined || tail == true) ? 20 : 60;
  	let tailStyleBoxTopMargin = (tail == undefined || tail == true) ? 10 : 5;
  	let tailStyleBoxBottomMargin = (tail == undefined || tail == true) ? 10 : 5;


  	let messageRender;
    let messageListRender = [];

    for (var i = 0; i < messageList.length; ++i) {
      messageList[i] = {text: messageList[i], index: i};
    }
       
    messageListRender = (
      <FlatList
        data={messageList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
    );

    // 왼쪽 메시지
		messageRender = (
	  <View 
          style={{
            marginLeft: 20,
            marginTop: tailStyleBoxTopMargin,
            marginBottom: tailStyleBoxBottomMargin,
            flexDirection: 'row',
          }}>
          {(tail == undefined || tail == true) && <Image style={{
            width: 40,
            height: 40,
            borderWidth: 1,
            borderRadius: 20,
          }} resizeMode="contain" source={require('assets/illust/profile.png')} />}
          {(tail == false) && <View style={{height: 40}}></View>}            
          <View style={{
          	marginLeft: tailStyleBoxLeftMargin,
              flexDirection: 'row',
        }}>
            <View 
              style={{
              	left: 10,
                alignItems: 'center',
                maxWidth: messageBoxMaxWitdth,
                borderRadius: 4,
                backgroundColor: backgroundColorStyle,
              }}>
              {(tail == undefined || tail == true)  && <Image 
              source={require('assets/images/message_box_tail.png')}
              style={{
              	position: 'absolute',
              	height: 24,
              	left: -8,
              	top: 5,
              }}/>} 
              <BasicText bold={false} title={'다음과 같이 질문해보세요'} size={18} margin={12}/>
              {messageListRender}
            </View>
          </View>
        </View>
			);
  	

  	return(
  		<View>
  		{messageRender}
  		</View>
  	);
  }
}