/*
프로그램 ID:AC-2020-JS
프로그램명:BasicText/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 기본 텍스트 컴포넌트
*/

import React, {Component} from 'react';
import {View, Text} from 'react-native';

export default class BasicButton extends React.Component {
  constructor(props){
  	super(props);
  }

  render(){
  	const {
  		title,
  		fontColor,
  		size,
  		marginTop,
  		marginBottom,
  		marginLeft,
  		marginRight,
  		margin,
      underline,
      bold,
      textAlign,
      justifyContent,
      spacing,
      onTextPress
  	} = this.props;

    let font = bold == false ? 'NanumSquareR' : 'NanumSquareB';

    let marginStyle = margin ? {
      margin: margin,
    } : null;

    let marginLeftStyle = marginLeft ? {
      marginLeft: marginLeft,
    } : null;

    let marginRightStyle = marginRight ? {
      marginRight: marginRight,
    } : null;

    let marginTopStyle = marginTop ? {
      marginTop: marginTop,
    } : null;

    let marginBottomStyle = marginBottom ? {
      marginBottom: marginBottom,
    } : null;

    let fontSizeStyle = size ? {
      fontSize: size,
    } : null;

    let colorStyle = fontColor ? {
      color: fontColor
    } : null;

    let underlineStyle = underline ? {
      textDecorationLine: 'underline',
    } : null;

    let textAlignStyle = textAlign ? {
      textAlign: textAlign,
    } : null;

    let justifyContentStyle = justifyContent ? {
      justifyContent: justifyContent,
    } : null;

    let spacingStyle = spacing ? {
      letterSpacing: spacing,
    } : null;

  	return(
  		<Text 
        onPress={onTextPress}
        style={[{fontFamily: font}, fontSizeStyle, colorStyle, underlineStyle, textAlignStyle,
        marginLeftStyle, marginRightStyle, marginTopStyle, marginBottomStyle, marginStyle,
        justifyContentStyle]}>
  		 {title ? title.replace('<br/>', '\n') : null}
  		</Text>
  		);
  }
}