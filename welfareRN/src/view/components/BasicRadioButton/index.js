
import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Image, TouchableHighlight, View } from 'react-native';
import _ from 'underscore';

var BACKGROUND_COLOR, BORDER_RADIUS, BORDER_WIDTH, COLOR, MARGIN, SIZE, BORDER_COLOR;

class BasicRadioButton extends React.Component {
  constructor(props) {
	super(props);
    this.state = {
        backgroundColor: '#fff',
        borderRadius: 4,
        borderWidth: 1,
        checked: false,
        color: '#00cf7b',
        margin: 2,
        name: '',
        onChange: null,
        size: 20,
		borderColor: '#e5e5e5'
      };		
  }

  componentDidMount() {
    this.setState(_.extend({}, this.props.style, _.omit(this.props, 'style')))
  }

  _toggleCheck = () => {
	  var checked = !this.state.checked;
    this.setState({ checked: checked });
    this.props.onChange && this.props.onChange(this.props.name, checked);
  }

  render() {
	  BACKGROUND_COLOR = this.state.backgroundColor;
    BORDER_RADIUS = this.state.borderRadius;
    BORDER_WIDTH = this.state.borderWidth;
    COLOR = this.state.color;
    MARGIN = this.state.margin;
    SIZE = this.state.size;
	  BORDER_COLOR = this.state.borderColor;

    const {
      margin,
      marginLeft,
      marginRight,
      marginTop,
      marginBottom,
    } = this.props;

    let marginStyle = margin ? {
      margin: margin,
    } : null;

    let marginLeftStyle = marginLeft ? {
      marginLeft: marginLeft,
    } : null;

    let marginRightStyle = marginRight ? {
      marginRight: marginLeft,
    } : null;

    let marginTopStyle = marginTop ? {
      marginTop: marginTop,
    } : null;

    let marginBottomStyle = marginBottom ? {
      marginBottom: marginBottom,
    } : null;


  	return(
  		<TouchableHighlight 
  			underlayColor='transparent'
  			onPress={this._toggleCheck}
  			style={[{
  				backgroundColor: BACKGROUND_COLOR,
  				borderWidth: BORDER_WIDTH,
  				borderColor: BORDER_COLOR,
  				borderRadius: BORDER_RADIUS,
  				width: SIZE,
  				height: SIZE,
  				margin: MARGIN,
  			}, marginStyle, marginLeftStyle, marginRightStyle, marginTopStyle, marginBottomStyle]}>
  			<View 
  				style={{
  					flex: 1,
  					justifyContent: 'center',
  					alignItems: 'center',
	  			}}>
	  			{this.state.checked && <Image 
	  				resizeMode="contain"
	  				source={require('assets/components/radio_checked.png')}
	  				style={{
	  					alignSelf: 'center',
	  					width: SIZE - 10,
	  					height: SIZE - 10,
	  				}}/>}
  			</View>
  		</TouchableHighlight>
  		);
  }
}

BasicRadioButton.propTypes = {
    checked: PropTypes.bool,
    name: PropTypes.string,
    onChange: PropTypes.func,
    size: PropTypes.number,
    style: PropTypes.object,
}

export default BasicRadioButton;