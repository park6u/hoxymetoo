import React, {Component} from 'react';
import {TouchableHighlight, Image} from 'react-native';

export default class SearchIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      onPress
    } = this.props;

    const source = require('assets/icon/search.png');
    return (
      <TouchableHighlight 
      	onPress={onPress}
      	underlayColor='transparent'>
      <Image
        resizeMode="contain"
        source={source}
        style={{
          marginRight: 16,
          width: 24,
          height: 24,
        }}
      />
      </TouchableHighlight>
    );
  }
}