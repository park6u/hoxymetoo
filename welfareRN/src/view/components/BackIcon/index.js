import React, {Component} from 'react';
import {TouchableOpacity, Image} from 'react-native';

export default class BackIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      onPress
    } = this.props;

    const source = require('assets/icon/back.png');
    return (
      <TouchableOpacity
      	onPress={onPress}
        style={{
          marginLeft: 16,
          width: 24,
          height: 24,          
        }}>
      <Image
        resizeMode="contain"
        source={source}
        style={{
          width: 24,
          height: 24,
        }}
      />
      </TouchableOpacity>
    );
  }
}