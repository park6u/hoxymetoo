import React, {Component} from 'react';
import {TouchableHighlight, Image} from 'react-native';

export default class NotificationIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  _onClick = (e) => {
    this.props.onClick();
  }

  render() {

    const source = require('assets/icon/notification.png');
    return (
      <TouchableHighlight 
        onPress={this._onClick}
        underlayColor='transparent'>
      <Image
        resizeMode="contain"
        source={source}
        style={{
          marginRight: 16,
          width: 24,
          height: 24,
        }}
      />
      </TouchableHighlight>
    );
  }  
}
