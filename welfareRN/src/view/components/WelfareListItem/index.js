
import React, {Component} from 'react';
import {View, Image} from 'react-native';
import Ripple from 'react-native-material-ripple';
import BasicText from 'src/view/components/BasicText'

export default class WelfareListItem extends React.Component {
  constructor(props){
  	super(props);
    this._appendDesireTag();
//    this._setItemIcon();
  }

  _onClick = () => {
  	this.props.onClick(this.props.pos);
  }

  // 리스트 아이템 태그 추가
  _appendDesireTag = async () => {
  	this.props.desires;
  }

  // 리스트 아이템 아이콘 추가
  _setItemIcon = async () => {
    const desires = this.props.desires;

    if (desires[0] == "경제") {
    }
  }

  render() {
  	const {
  	  title,
      desires,
      targetCharacters,
      isClicked,
  	  onClick,
  	  pos
  	} = this.props;

    let icon = undefined;
    if (desires[0] == "경제") {
      icon = require('assets/icon/category_finance.png');
    }
    else if (desires[0] == "건강") {
      icon = require('assets/icon/category_health.png');      
    }
    else if (desires[0] == "교육") {
      icon = require('assets/icon/category_education.png');
    } 
    else if (targetCharacters[0] == "장애") {
      icon = require('assets/icon/category_disabled.png');      
    }
    else if (targetCharacters[0] == "실업자") {
      icon = require('assets/icon/category_employment.png');      
    }
    else {
      icon = require('assets/icon/category_unknown.png');
    }

  	return (
  		<View
  			style={{
  				marginTop: 20,
  				backgroundColor: '#fff',
  				borderRadius: 8,
	  		}}>
	  		<Ripple 
	  			onPress={this._onClick}
	  			style={{
  				flex: 1,
  				height: 100,
  				width: '100%',
  				flexDirection: 'row',	  				
		  		}}>
        <View 
          style={{
            position: 'absolute',
            top: 12,
            left: 12,
            width: 6,
            height: 6,
            borderRadius: 6,
            backgroundColor: '#00cf7b',
          }}>
        </View>
	  		<View 
	  			style={{
	  				flex: 2,
	  				justifyContent: 'center',
	  				alignItems: 'center',
		  		}}>
		  		<Image 
            source={icon}
            resizeMode='contain'
		  			style={{
		  				width: 50,
		  				height: 50,
		  			}}/>
	  		</View>
	  		<View
	  			style={{
	  				flex: 4,	  				
	  				justifyContent: 'center',
		  		}}>
		  		<BasicText title={title} marginRight={4} size={16} numberOfLines={3} ellipsizeMode='tail' />
	  		</View>
	  		</Ripple>
  		</View>
  	  );
  }
}