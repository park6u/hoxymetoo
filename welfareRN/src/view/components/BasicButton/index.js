/*
프로그램 ID:AC-2020-JS
프로그램명:BasicButton/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 기본 버튼 컴포넌트
*/

import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Ripple from 'react-native-material-ripple';
import LinearGradient from 'react-native-linear-gradient';

export default class BasicButton extends React.Component {
  constructor(props){
  	super(props);
  }

  render(){
  	const {
  		title,
  		size,
  		width,
  		margin,
  		onPress,
  		enabled,
  		fontColor,
  		borderRadius,
  	} = this.props;

  	let widthValue = width ? width : '90%';

  	let marginValue = margin ? margin : 0;

  	let basicColor = (enabled == undefined ||  enabled == true) ?
  		 ['#00cf7b', '#00b4a4'] : ['#4b9c92', '#54b08a'];

  	let borderRadiusValue = borderRadius ? borderRadius : 30;

  	let fontSizeValue = size ? size : 16;

  	return(
  		<Ripple 
			onPress={(enabled == undefined ||  enabled == true) ? onPress : undefined}
			disabled={!(enabled == undefined ||  enabled == true)}
			rippleContainerBorderRadius={borderRadiusValue}
			style={{
				width: widthValue,
				alignItems: 'center',
				margin: marginValue,
			}}>
			<LinearGradient 
				colors={basicColor}
				start={{x: 0, y: 0}} end={{x: 1, y: 0}}
				style={{
				paddingTop: 15,
				paddingBottom: 15,
				marginLeft: 40,
				marginRight: 40,
				borderRadius: borderRadiusValue,
				width: '100%',
				flexDirection: 'row',
				justifyContent: 'center',
				alignItems: 'center',
				shadowColor: "#000",
				shadowOffset: {
					width: 0,
					height: 1,
				},
				shadowOpacity: 0.22,
				shadowRadius: 2.22,
				elevation: 3,
				}}>
				<Text style={{
					fontWeight: 'bold',
					fontFamily: 'NanumSquareB',
					fontSize: fontSizeValue,
					color: '#fff',	
				}}>{title}</Text>
			</LinearGradient>
  		</Ripple>
  		);
  }
}