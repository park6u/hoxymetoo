
import React, {Component} from 'react';
import {Image, StyleSheet, Text, View, Button} from 'react-native';
import Ripple from 'react-native-material-ripple';

export default class FloatingActionButton extends Component<Props> {
  constructor(props){
	super(props);
  }

  render() {
  	const {
  	  onClick
  	} = this.props;

	return(
		<View
			style={{
				position: 'absolute',
				bottom: 30,
				right: 20,
				width: 60,
				height: 60,
				backgroundColor: '#00cf7b',
				borderRadius: 30,
			}}>
			<Ripple 
				rippleContainerBorderRadius={30}
				onPress={onClick} 
				style={{
					width: 60,
					height: 60,
					justifyContent: 'center',
					alignItems: 'center',
				}}>
				<Image 
					resizeMode='contain'
					source={require('assets/icon/chatbot.png')}
					style={{
						width: 30,
						height: 30,
					}}/>
			</Ripple>
		</View>
	);
  }
}