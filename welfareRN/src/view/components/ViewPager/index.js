/*
프로그램 ID:AC-2020-JS
프로그램명:HorizontalScrollView/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 앱 가이드 스크린 ViewPager
*/

import React, {Component} from 'react';
import {Text, Button, ScrollView, Dimensions, View} from 'react-native';

export default class ViewPager extends React.Component {
  constructor(props){
	super(props);
  }

	render() {
		const {
			onPress
		} = this.props;

		let screenWidth = Dimensions.get('window').width;
		let screenHeight = Dimensions.get('window').height;

		return (
			<ScrollView
				horizontal={true}
				pagingEnabled={true}
				showsHorizontalScrollIndicator={false}
				scrollIndicatorInsets={{top: 10, left: 10, bottom: 10, right: 10}}
				scrollEventThrottle={10}
			>
				<View
					style={{
						backgroundColor: '#783387',
						height: screenHeight,
						width: screenWidth,
						justifyContent: 'center',
						flex: 1,
						alignItems: 'center'
					}}
				>
					<Text>
						Hello
					</Text>
				</View>
				<View
					style={{
						backgroundColor: '#59c2d3',
						height: screenHeight,
						width: screenWidth,
						justifyContent: 'center',
						flex: 1,
						alignItems: 'center'
					}}
				>
				</View>
				<View
					style={{
						backgroundColor: '#323223',
						height: screenHeight,
						width: screenWidth,
						justifyContent: 'center',
						flex: 1,
						alignItems: 'center'
					}}
				>
				<Button title="click Here" onPress={onPress}></Button>
				</View>
			</ScrollView>
		)
	}
}