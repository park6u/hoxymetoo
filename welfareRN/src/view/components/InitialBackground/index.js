
import React, {Component} from 'react';
import {Alert, AsyncStorage, TouchableHighlight, Animated, Image, Dimensions, StyleSheet, Text, View, Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default class InitialBackground extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const {
			title
		} = this.props;

		let screenHeight = Dimensions.get('window').height;

		let titleRender;
		if (title){
			titleRender = <Text 
							style={{
								alignSelf: 'center', 
								color: '#fff',
								marginBottom: (screenHeight / 10),
								fontSize: 30,
								fontFamily: 'NanumSquareB'}}>
								{title}
						  </Text>;
		}
		return (
			<View 
				style={{
					flex: 1,
					justifyContent: 'center'
				}}>
				<LinearGradient 
					colors={['#00cf7b', '#00b4a4']}
				 	style={{
				 		flex: 1,
				 		justifyContent: 'center',
				 		alignItems: 'center'
				 	}}>
				 	{titleRender}
				</LinearGradient>
				<LinearGradient  
					colors={['#157ee6', '#4dd7ff']}
					style={{
						flex: 2, 
						alignItems: 'center',
						opacity: 0.3
					}}>
				</LinearGradient>
			</View>
		)
	}
}