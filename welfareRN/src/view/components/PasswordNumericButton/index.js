import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Ripple from 'react-native-material-ripple';
import BasicText from 'src/view/components/BasicText';

export default class PasswordNumericButton extends React.Component {
  constructor(props){
  	super(props);
  }

  _onPressButton = () => {
    this.props.onPress(this.props.title);
  }

  render(){
  	const {
      title,
      enabled,
      onPress,
      borderRadiusValue,
  	} = this.props;

  	let titleValue = title;
  	return(
	  <View 
	    style={{
	      flex: 1,
	      alignItems: 'center',
	    }}>	
  		<Ripple 
			onPress={(enabled == undefined ||  enabled == true) ? this._onPressButton : undefined}
			disabled={!(enabled == undefined ||  enabled == true)}
			rippleContainerBorderRadius={80}
			style={{
			  width: '100%',
			  height: '100%',
			  alignItems: 'center',
			  justifyContent: 'center',
			}}
			>
	        <BasicText title={titleValue} size={32}/>
  		</Ripple>
  	  </View>
	);
  }
}