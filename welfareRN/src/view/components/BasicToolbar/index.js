/*
프로그램 ID:AC-2020-JS
프로그램명:BasicToolbar/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.15
버전:0.6
설명
 기본 툴바 컴포넌트
*/

import React, {Component} from 'react';
import {View, Text} from 'react-native';
import BasicText from 'src/view/components/BasicText';

export default class BasicToolbar extends React.Component {
  constructor(props){
  	super(props);
  }

  render(){
  	const {
  		title,
      navigation,
      onNavigationClick,
  	} = this.props;

  	return(
       <View 
        style={{
          flex: 1,
          height: 56,
          backgroundColor: '#fff',
          flexDirection: 'row',
        }}>
          <View style={{}}>

          </View>
          <BasicText title={title}/>
          <View>

          </View>
       </View>
  		);
  }
}