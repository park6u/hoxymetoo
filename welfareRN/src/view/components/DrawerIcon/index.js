import React, {Component} from 'react';
import {TouchableHighlight, Image} from 'react-native';

export default class DrawerIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  _onClick = (e) => {
    this.props.openDrawer();
  }

  render() {
    const source = require('assets/icon/drawer.png');
    return (
      <TouchableHighlight 
      	onPress={this._onClick}
      	underlayColor='transparent'>
      <Image
        resizeMode="contain"
        source={source}
        style={{
          marginLeft: 16,
          width: 24,
          height: 24,
        }}
      />
      </TouchableHighlight>
    );
  }
}