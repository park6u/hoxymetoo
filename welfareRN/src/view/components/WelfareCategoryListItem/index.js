
import React, {Component} from 'react';
import {View, Image} from 'react-native';
import Ripple from 'react-native-material-ripple';
import BasicText from 'src/view/components/BasicText'

export default class WelfareCategoryListItem extends React.Component {
  constructor(props){
  	super(props);
  }

  onItemClick = () => {
    this.props.onClick(this.props.category);
  }

  render() {
  	const {
      onClick,
      category,
  	  title,
  	  icon,
  	  order
  	} = this.props;

  	let layoutMargin;
  	if (order % 2 == 0) {
  		layoutMargin = {marginLeft: 10};
  	} else {
  		layoutMargin = {marginRight: 10};
  	}

  	return (
  		<View 
  			style={[{
  				marginTop: 20,
  				backgroundColor: '#fff',
  				borderRadius: 8,
  				flex: 1
	  		}, layoutMargin]}>
	  		<Ripple
          onPress={this.onItemClick}
	  			style={{
	  				flex: 1,
  					height: 140,
	  				flexDirection: 'column',	  				
		  		}}>
		  		<View 
		  			style={{
		  				flex: 3,
		  				justifyContent: 'center',
		  				alignItems: 'center',
			  		}}>
			  		<Image
			  			source={icon} 
			  			resizeMode="contain"
			  			style={{
			  				width: 70,
			  				height: 70,
			  			}}/>
			  		<BasicText title={title} bold={false}/>
		  		</View>
	  		</Ripple>
  		</View>
  	  );
  }
}