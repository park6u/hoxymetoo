/*
프로그램 ID:AC-2020-JS
프로그램명:BasicDropDown/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.17
버전:0.6
설명
 기본 드롭다운 컴포넌트
*/

import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';

export default class BasicDropDown extends React.Component {
  constructor(props){
  	super(props);
  }

  _incomeDropdownChange = (value, index, data) => {
    this.refs._dropdown.value = value;    
  }

  _renderIncomeDropDownAcc = () => {
    return(
      <View style={{
        width: 24,
        height: 24,
        justifyContent: 'center',
        alignItems: 'center',       
      }}>
        <Image 
          resizeMode="contain"
          style={{
            width: 10,
            height: 10,
          }}
          source={require('assets/icon/dropdown_up.png')}/>
        <Image 
          resizeMode="contain"
          style={{
            width: 10,
            height: 10,
          }}
          source={require('assets/icon/dropdown_down.png')}/>
      </View>
      );
  }

  render(){
  	let {
      ref,
      data,
  		title,
  		color,
      value,
      onChangeText,
  	} = this.props;

  	return(
        <View 
          ref={ref}>
          <Dropdown
            ref='_dropdown'
            value={value}
            dropdownOffset={{top: 12.5, left: 0}}
            rippleInsets={{top:0, left: 0}}
            pickerStyle={{
              width: '90%',
            }}
            containerStyle={{
              height: 50,
              marginBottom: 12.5,
              paddingBottom: 12.5,
              padding: 0,
            }}
            inputContainerStyle={{
              height: 50,
              paddingLeft: 10,
              paddingBottom: 12.5,
              borderRadius: 4,
              borderColor: '#e5e5e5',
              borderWidth: 1,
            }}
            itemTextStyle={{fontFamily: 'NanumSquareB'}}
            onChangeText={onChangeText}
            renderAccessory={this._renderIncomeDropDownAcc}
            data={data}>
          </Dropdown>
        </View>
  		);
  }
}