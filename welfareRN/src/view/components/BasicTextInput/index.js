/*
프로그램 ID:AC-2020-JS
프로그램명:BasicTextInput/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.09
버전:0.6
설명
 기본 텍스트 입력 컴포넌트
*/

import React, {Component} from 'react';
import {View, TextInput, Image} from 'react-native';

export default class BasicTextInput extends React.Component {
  constructor(props){
  	super(props);
  }

  render(){
  	const {
  	  placeholder,
  	  path,
  	  value,
  	  maxLength,
  	  dataDetectorTypes,
  	  onChangeText,
  	} = this.props;

  	let imagePadding = path ? {paddingLeft: 36} : {paddingLeft: 12};

  	return(
		<View style={{}}>
			<TextInput 
				placeholder={placeholder}
				value={value}
				onChangeText={onChangeText}
				dataDetectorTypes={dataDetectorTypes}
				selectionColor='#00cf7b'
				maxLength={maxLength}
				style={[{
					fontFamily: 'NanumSquareR',
					borderRadius: 4,
					borderColor: '#e5e5e5',
					borderWidth: 1, 
					color: '#000',
					height: 50,
				}, imagePadding]}>
			</TextInput>
			<Image 
			    resizeMode="contain"
				source={path} 
				style={{
					position: 'absolute',
					top: 15,
					left: 10,
					width: 20,
					height: 20,
				}}/>
		</View>

  		);
  }
}