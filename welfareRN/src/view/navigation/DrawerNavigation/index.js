/*
프로그램 ID:AC-2020-JS
프로그램명:BottomNavigation/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.15
버전:0.6
설명
- 우측 드로워
- https://reactnavigation.org/docs/en/drawer-navigator.html 참조
*/

import {createStackNavigator, createDrawerNavigator} from 'react-navigation';
import DrawerScreenViewModel from 'src/viewmodel/DrawerScreenViewModel';
import BottomNavigation from 'src/view/navigation/BottomNavigation';
import ChatBotScreen from 'src/viewmodel/ChatBotScreenViewModel';
import AlarmListScreenViewModel from 'src/viewmodel/AlarmListScreenViewModel';
import WelfareInfoScreenViewModel from 'src/viewmodel/WelfareInfoScreenViewModel';
import CategoryListScreenViewModel from 'src/viewmodel/CategoryListScreenViewModel';
import DescriptionScreenViewModel from 'src/viewmodel/EstimationScreenViewModel/DescriptionScreenViewModel';
import SettingScreenViewModel from 'src/viewmodel/SettingScreenViewModel';
import OpenSourceScreen from 'src/view/screen/OpenSourceLicenseScreen';

const DrawerMainNavigation = createDrawerNavigator({
  Home: BottomNavigation
},{
  initialRouteName: 'Home',
  contentComponent: DrawerScreenViewModel,
});

const ChatBotNavigation = createStackNavigator({
	Home: ChatBotScreen,
}, {headerLayoutPreset: 'center'});

const InfoNavigation = createStackNavigator({
	Home: WelfareInfoScreenViewModel,
}, {headerLayoutPreset: 'center'});

const ListNavigation = createStackNavigator({
	Home: CategoryListScreenViewModel,
}, {headerLayoutPreset: 'center'});

const AlarmNavigation = createStackNavigator({
  Alarm: AlarmListScreenViewModel,
}, {headerLayoutPreset: 'center'});

const DescriptionNavigation = createStackNavigator({
  Description: DescriptionScreenViewModel,
}, {headerLayoutPreset: 'center'});

const SettingNavigation = createStackNavigator({
  Setting: SettingScreenViewModel,
}, {headerLayoutPreset: 'center'});

const OpenSourceNavigation = createStackNavigator({
  OpenSource: OpenSourceScreen
}, {headerLayoutPreset: 'center'});

const DrawerNavigation = createStackNavigator({
  Home: DrawerMainNavigation,
  ChatBot: ChatBotNavigation,
  Info: InfoNavigation,
  Alarm: AlarmNavigation,
  List: ListNavigation,
  Description: DescriptionNavigation,
  Setting: SettingNavigation,
  OpenSource: OpenSourceNavigation,
 },{
	headerMode: 'none',
});

export default DrawerNavigation;