/*
프로그램 ID:AC-2020-JS
프로그램명:navigation/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.06
버전:0.6
설명
- 스크린 네비게이션
*/

import {createStackNavigator, createSwitchNavigator, createAppContainer} from 'react-navigation';
import SplashScreen from 'src/view/screen/SplashScreen';
import LoginScreen from 'src/viewmodel/LoginViewModel';
import MainNavigation from 'src/view/navigation/DrawerNavigation';
import RegisterScreen from 'src/viewmodel/RegisterViewModel';
import GuideScreen from 'src/viewmodel/GuideScreenViewModel';
import PasswordScreen from 'src/viewmodel/PasswordScreenViewModel';

const AppNavigator = createSwitchNavigator({
		Register: RegisterScreen,
		Password: PasswordScreen,
		Splash: SplashScreen,
		Login: LoginScreen,
		Guide: GuideScreen,
		Main: MainNavigation,
	}, 
	{
		initialRouteName: 'Splash',
	}
);

const Navigator = createAppContainer(AppNavigator);

export default Navigator;