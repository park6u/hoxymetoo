/*
프로그램 ID:AC-2020-JS
프로그램명:BottomNavigation/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.15
버전:0.6
설명
- 하단 탭 네비게이션
- https://reactnavigation.org/docs/en/bottom-tab-navigator.html 참조
*/

import React from 'react';
import {Text, View, Image, TouchableHighlight} from 'react-native';
import BasicText from 'src/view/components/BasicText';
import {createStackNavigator, createSwitchNavigator, createBottomTabNavigator, BottomTabBar} from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import MainScreenViewModel from 'src/viewmodel/MainScreenViewModel';
import CategoryMainScreenViewModel from 'src/viewmodel/CategoryScreenViewModel';
import EstimationMainScreenViewModel from 'src/viewmodel/EstimationScreenViewModel';
import MyPageScreenViewModel from 'src/viewmodel/MyPageScreenViewModel';

const stackNavigatorConfig = {
  headerLayoutPreset: 'center',
};

const MainTab = createStackNavigator({
    Home: {
    screen: MainScreenViewModel,
    },
}, stackNavigatorConfig);

const CategoryTab = createStackNavigator({
  Home: {
    screen: CategoryMainScreenViewModel,
  }
}, stackNavigatorConfig);

const EstimationTab = createStackNavigator({
  Home: EstimationMainScreenViewModel,
}, stackNavigatorConfig);

const MyPageTab = createStackNavigator({
  Home: MyPageScreenViewModel
}, stackNavigatorConfig);

const iconSize = 30;

const iconDir = 'assets/icon/';
var selectedIcons = ['home_selected.png','category_selected.png', 
                  'estimation_selected.png', 'mypage_selected.png'];
var unselectedIcons = ['home.png', 'category.png', 'estimation.png', 'mypage.png'];

const BottomNavigation = createBottomTabNavigator({
    Main: MainTab,
    Category: CategoryTab,
    Estimation: EstimationTab,
    MyPage: MyPageTab
  },
  {
    initialRouteName: 'MyPage',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName, routes} = navigation.state;
        if (routeName == 'Main'){
          return (
            <View>
            	<Image 
                source={tintColor == '#fff' ? require('assets/icon/home.png') : require('assets/icon/home_selected.png')}
                resizeMode="contain"
                style={{
                  width: iconSize,
                  height: iconSize,
                }}/>
                <View 
                  style={{
                    position: 'absolute',
                    right: -1,
                    top: 2,
                    width: 15,
                    height: 15,
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#00cf7b',
                  }}>
                  <BasicText title={routes[0].params ? routes[0].params.badgeCount : undefined} size={12} fontColor='#eee' />
                </View>
            </View>
            );
        } else if (routeName == 'Category'){
          return (
              <Image 
                source={tintColor == '#fff' ? require('assets/icon/category.png') : require('assets/icon/category_selected.png')}
                resizeMode="contain"
                style={{
                  width: iconSize,
                  height: iconSize,
                }}/>
          	);
        } else if (routeName == 'Estimation'){
          return (
            <Image 
              source={tintColor == '#fff' ? require('assets/icon/estimation.png') : require('assets/icon/estimation_selected.png')}
              resizeMode="contain"
              style={{
                width: iconSize,
                height: iconSize,
              }}/>
            );
        } else {
          return (
            <Image 
              source={tintColor == '#fff' ? require('assets/icon/mypage.png') : require('assets/icon/mypage_selected.png')}
              resizeMode="contain"
              style={{
                width: iconSize,
                height: iconSize,
              }}/>
          	);
        }
      }
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '00cf7b',
      inactiveTintColor: '#fff',
      labelStyle: {
        fontSize: 11,
        fontFamily: 'NanumSquareB',
      },
    },
  });

const MainStack = createStackNavigator({
  Home: {
    screen: BottomNavigation,
    navigationOptions : {
      header: null
    }
  },  
}, stackNavigatorConfig);

export default MainStack;
