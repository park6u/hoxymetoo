/*
프로그램 ID:AC-2010-JS
프로그램명:App.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.06
버전:0.6
설명
- 프로그램 메인 화면 호출
*/

import React, {Component} from 'react';
//import { Provider as StoreProvider } from 'react-redux';
import Navigator from './view/navigation'
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import drawer from 'src/model/ReduxModel/DrawerReducer'

//const AppContainer = createAppContainer()
const store = createStore(drawer);

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
    <Provider store={store}>
      <Navigator navigation={this.props.navigation}>
      </Navigator>
    </Provider>
    );
  }
}
