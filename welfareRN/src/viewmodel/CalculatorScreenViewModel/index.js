
import React, {Component} from 'react';
import CalculatorScreen from 'src/view/screen/CalculatorScreen';

export default class CalculatorScreenViewModel extends React.Component {
	constructor(props) {
	  super(props);
	}

	render() {
	  return (
			<CalculatorScreen
				onPress={this.onPress}
			>
			</CalculatorScreen>
	  );
	}
}
