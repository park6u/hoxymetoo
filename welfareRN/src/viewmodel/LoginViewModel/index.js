/*
프로그램 ID:AC-2020-JS
프로그램명:LoginViewModel/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.11
버전:0.6
설명
- 로그인 스크린 뷰모델
- 구글과 네이버는 배포후에 Oauth 토큰이 발급, 알파버전에는 카카오로그인만 지원
*/

import React, {Component} from 'react';
import LoginScreen from 'src/view/screen/LoginScreen';
import {GoogleSignin, statusCodes} from 'react-native-google-signin';
import RNKakaoLogins from 'react-native-kakao-logins';
import UserModel from 'src/model/UserModel';
import WelfareModel from 'src/model/WelfareModel';


export default class LoginScreenViewModel extends React.Component {
  constructor(props) {
	  super(props);
    const googleClientIdiOs = '982265184100-emqhhpirmckbqigjqjgg39b4jgicd87v.apps.googleusercontent.com';
    const googleClientIdWeb = '982265184100-7fvnv48j97hl6e30aqd2idquoor9a1vc.apps.googleusercontent.com';
  	GoogleSignin.configure({
  	  scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  	  webClientId: googleClientIdWeb, // client ID of type WEB for your server (needed to verify user ID and offline access)
  	  offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  	  hostedDomain: '', // specifies a hosted domain restriction
  	  loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  	  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
  	  accountName: '', // [Android] specifies an account name on the device that should be used
  	  iosClientId: googleClientIdiOs, // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
  	});
    this.state = {loading: false, buttonEnabled: true, naverLoginEnabled: false, googleLoginEnabled: false};
  }

  // 카카오 로그인
  kakaoSingIn = async () => {  	
  	try {
      this.setState({buttonEnabled: false, loading: true});
	    RNKakaoLogins.login((err, result) => {
      	if (err) {
          this.setState({buttonEnabled: true, loading: false});
          return;
        }
        this.kakaoGetProfile(result.token);
      });
  	} catch (error) {
      this.setState({buttonEnabled: true, loading: false});
  		console.log(error);
  	}
  }

  // 카카오 프로필 가져오기
  kakaoGetProfile = async (token) => {
   RNKakaoLogins.getProfile((err, result) => {
    if (err) {
      console.log(err.toString());
      return;
    }

    UserModel.postSocialId('kakao_' + result.email, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      let registered = false;
      console.log(res);
      
      // 이미 등록된 아이디인 경우 {message: "해당하는 socialId가 존재하지 않습니다."}
      if (res) {
        if (!res.message) {
          registered = true;
          UserModel.registerInternalMemkey(res.memKey);
        }
      }

      this.props.navigation.navigate('Register', {
          socialId: 'kakao_'+ result.email,
          pushToken: token, 
          email: result.email,
          name: result.nickname,
          alreadyRegistered: registered,
      });
    });

   });
  }

  // 카카오 로그아웃
  kakaoSignOut = async () => {

  }

  // 구글 로그인
  // TODO OAuth 인증 필요함
  googleSignIn = async () => {
	try {
	await GoogleSignin.hasPlayServices();
	const userInfo = await GoogleSignin.signIn();
	this.setState({ userInfo });
	} catch (error) {
	if (error.code === statusCodes.SIGN_IN_CANCELLED) {
	  // 구글 로그인 취소
	} else if (error.code === statusCodes.IN_PROGRESS) {
	  // 이미 로그인 진행중
	} else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
	  // 구글플레이 서비스가 사용 불가능
	} else {
	  // 기타 에러
	}
	}
  };

  googleSignOut = async () => {
	try {
	  await GoogleSignin.revokeAccess();
	  await GoogleSignin.signOut();
	  this.setState({ user: null });
	} catch (error) {
	  console.error(error);
	}
  };

  getCurrentUserInfo = async () => {
  try {
    const userInfo = await GoogleSignin.signInSilently();
    this.setState({ userInfo });
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_REQUIRED) {
      // 아직 로그인이 되지 않음
    } else {
      // 기타에러
    }
  }
  };

  isMemberRegistered = () => {
    UserModel.getMember()
  }

  onKakaoLoginPress = (e) => {
  	this.kakaoSingIn();
  }

  onNaverLoginPress = (e) => {

  }

  onGoogleLoginPress = (e) => {
  	//this.googleSignIn();
  }

  render() {
	return (
		<LoginScreen
      buttonEnabled={this.state.buttonEnabled}
      naverLoginEnabled={this.state.naverLoginEnabled}
      googleLoginEnabled={this.state.googleLoginEnabled}
			onKakaoLoginPress={this.onKakaoLoginPress}
			onNaverLoginPress={this.onNaverLoginPress}
			onGoogleLoginPress={this.onGoogleLoginPress}
      loading={this.state.loading}
		>
		</LoginScreen>
	)	
  }
}