/*
프로그램 ID:AC-2020-JS
프로그램명:DrawerScreenViewModel/index.js
작성자:박준우(pkjoho95@gmail.com)
버전:0.6
설명
-  생애주기 분류 드로워(Drawer) 스크린
*/

import React, {Component} from 'react';
import DrawerScreen from 'src/view/screen/DrawerScreen';
import RegionModel from 'src/model/RegionModel';
import UserModel from 'src/model/UserModel';
import {connect} from 'react-redux';
import {drawerChanged} from 'src/model/ReduxModel/DrawerAction'

class DrawerScreenViewModel extends React.Component {
  constructor(props) {
	  super(props);
    this.state = {
      socialId: undefined,
      saveEnabled: false,
      sido: [], 
      sidoIdMap: {},
      sidoSelected: undefined,
      sigungu: [],
      sigunguIdMap: {},
      sigunguSelected: undefined,
      addressId: undefined,
      years: undefined, 
      yearsSelected: undefined,
      month: undefined, 
      monthSelected: undefined,
      day: undefined,
      daySelected: undefined,
      disabled: undefined, 
      disabledSelected: "해당없음",
      desires: undefined,
      desiresSelected: "해당없음",
      houseType: undefined,
      houseTypeSelected: "해당없음",
      targetCharacters: undefined,
      targetCharactersSelected: "해당없음",
      bohun: undefined,
      family: undefined,
      familySelected: undefined,
      renderDelayDone: false, // Render가 두번 호출되는 것을 방지하기 위해 추가
      refreshDelayDone: true, // Render가 두번 호출되는 것을 방지하기 위해 추가
    };
    setTimeout(() => {
      this.state.renderDelayDone = true;
    }, 3000);
    this.initData();
    UserModel.getInternalMemkey((memkey) => {
      UserModel.getMember(memkey, (err, res) => {
        if (err) {
          console.log(err)
          return;
        }
        this.setState({socialId: res.socialId});

        // 가져온 값에 따라 생애주기 설정
        // ViewModel에서 selected value를 설정
        if (res.memGender) {
          this.refs._drawer.setGender(res.memGender);
        }
        if (res.memBohun) {
          this.refs._drawer.setBohun(res.memBohun);
        }

        if (res.memFamily) {
          this.setState({familySelected: res.memFamily + "명"});
        }
        if (res.disables) { 
          this.setState({disabledSelected: res.memDisabled});
        }
        if (res.memBirthday) {
          let year = res.memBirthday.substr(0, 4);
          let month = res.memBirthday.substr(4, 2);
          let day = res.memBirthday.substr(6, 2);
          this.setState({yearsSelected: year+"년", monthSelected: month+"월", daySelected: day+"일"});
        }
        if (res.memAddressId) {
          let addrs = res.memAddressId.split(" ");
          this.setState({sidoSelected: addrs[0], sigunguSelected: addrs[1]});
          this.getRegion(addrs[0], addrs[1]);
        }
        if (res.targetCharacters[0]) {
          this.setState({targetCharactersSelected: res.targetCharacters[0]});
        }
        if (res.houseTypes[0]) {
          this.setState({houseTypeSelected: res.houseTypes[0]});
        }
        if (res.disables[0]) {
          this.setState({disabledSelected: res.disables[0]})
        }
        if (res.desires[0]) {
          this.setState({desiresSelected: res.desires[0]});
        }
      });
    });
  }

  onGenderChange = (next) => {
    this.setState({gender: next}, () => {
      this.setState({saveEnabled: true});      
    });
  }

  onYearChange = (next) => {
  	this.setState({yearsSelected: next}, () => {
      this.setState({saveEnabled: true});      
    });
  }

  onMonthChange = (next) => {
    this.setState({monthSelected: next}, () => {
      this.setState({saveEnabled: true});
    });
  }

  onDayChange = (next) => {
    this.setState({daySelected: next}, () => {
      this.setState({saveEnabled: true});
    });
  }

  onSidoChange = (next) => {
    this.setState({sidoSelected: next}, () => {
      this.state.sigungu = [];
      this.getSiGunGu(this.state.sidoIdMap[next]);
    });
  }

  onSigunguChange = (next) => {
    this.setState({sigunguSelected: next}, () => {
      RegionModel.getAddressId(this.state.sidoIdMap[this.state.sidoSelected], 
        this.state.sigunguIdMap[this.state.sigunguSelected], (id) => {
        console.log(id);
        this.state.addressId = String(id);
        this.setState({saveEnabled: true});
      });
    });
  }

  onIncomeChange = (next) => {
    this.setState({income: next}, () => {
      this.setState({saveEnabled: true});
    });
  }

  onBohunChange = (next) => {
    this.setState({bohun: next}, () => {
      this.setState({saveEnabled: true});
    });
  }

  onDesiresChange = (next) => {
    this.setState({desiresSelected: next}, () => {
      this.setState({saveEnabled: true});
    });    
  }

  onHouseTypeChange = (text) => {
    this.setState({houseTypeSelected: text}, () => {
      this.setState({saveEnabled: true});
    });
  }

  onTargetCharactersChange = (text) => {
    this.setState({targetCharactersSelected: text}, () => {
      this.setState({saveEnabled: true});
    });    
  }

  onDisabledChange = (next) => {
    this.setState({disabledSelected: next}, () => {
      this.setState({saveEnabled: true});
    });
  }

  onFamilyChange = (next) => {
    this.setState({familySelected: next}, () => {
      this.setState({saveEnabled: true});
    });
  }

  getLifeCycle = (age) => {
    if (0 <= age && age <= 7) {
      return "001";
    } else if (7 < age && age <= 12) {
      return "002";
    } else if (12 < age && age <= 18) {
      return "003";
    } else if (18 < age && age <= 38) {
      return "004";
    } else if (38 < age && age <= 64) {
      return "005";
    } else if (65 < age) {
      return "006";
    }
  }

  getHouseType = (houseType) => {
    if (houseType == "해당없음") {
      return "001";
    } else if (houseType == "한부모") {
      return "002";
    } else if (houseType == "다문화") {
      return "003";
    } else if (houseType == "조손") {
      return "004";
    } else if (houseType == "새터민") {
      return "005";
    } else if (houseType == "소년소녀가장") {
      return "006";
    } else if (houseType == "독거노인") {
      return "007";
    }
  }

  getTargetCharacters = (targetCharacters) => {
    if (targetCharacters == "해당없음") {
      return "001";
    } else if (targetCharacters == "임산부") {
      return "003";
    } else if (targetCharacters == "장애") {
      return "004";
    } else if (targetCharacters == "국가유공자") {
      return "005";
    } else if (targetCharacters == "실업자") {
      return "006";
    }  
  }

  getDesires = (desires) => {
    switch(desires) {
      case "가족관계":
        return "3000000";
      case "건강":
        return "1000000";
      case "경제":
        return "5000000";
      case "고용":
        return "7000000";
      case "교육":
        return "6000000";
      case "법률 및 권익보장":
        return "9000000";
      case "사회적 관계":
        return "4000000";
      case "생활환경":
        return "8000000";
      case "안전":
        return "0000000";
      case "일상생활유지":
        return "2000000";
      default:
        return "";
    }
  }

  getDisabled = (disabled) => {
    switch(disabled) {
      case "간":
        return "C0";
      case "간질":
        return "F0";
      case "뇌병변":
        return "60";
      case "시각":
        return "20";
      case "신장":
        return "90";
      case "심장":
        return "A0";
      case "안면":
        return "D0";
      case "언어":
        return "40";
      case "자폐성":
        return "70";
      case "장루":
        return "E0";
      case "정신":
        return "80";
      case "지적":
        return "50";
      case "지체":
        return "10";
      case "청각":
        return "30";
      case "호흡기":
        return "B0";
    }
  }

  // 저장 버튼 클릭시
  onSaveClick = async () => {
    this.refresh();
  }

  // 정보 재설정 및 전송
  refresh = async () => {
    if (!this.state.renderDelayDone) return; 
    if (!this.state.refreshDelayDone) return;
    this.state.refreshDelayDone = false;
    let targetCharacters = [];
    let lifeCycles = [];
    let disables = [];
    let desires = [];
    let jobs = [];
    let houseTypes = [];
    let families = [];

    let nowDate = new Date();
    let nowYear = nowDate.getYear() + 1900;
    let nowMonth = nowDate.getMonth();
    let nowDay = nowDate.getDate();

    // 만나이 계산
    let year = Number(this.state.yearsSelected.substr(0, 4));
    let month = Number(this.state.monthSelected.substr(0, 2));
    let day = Number(this.state.daySelected.substr(0, 2));
    let age = nowYear - year;
    if (month <= nowMonth) {
      if (month == nowMonth && day < nowDay) {
        age -= 1;
      } 
    } else {
      age -= 1;
    }

    lifeCycles.push(this.getLifeCycle(age));
    houseTypes.push(this.getHouseType(this.state.houseTypeSelected));
    targetCharacters.push(this.getTargetCharacters(this.state.targetCharactersSelected));
    let disablesId = this.getDisabled(this.state.disabledSelected);
    if (disablesId != null){
      disables.push(disablesId);
    }
    desires.push(this.getDesires(this.state.desiresSelected));



    let info = {
      "socialId": this.state.socialId,
    };

    if (this.state.gender) {
      info["memGender"] = this.state.gender;
    }
    if (this.state.family) {
      info["memFamily"] = this.state.familySelected.split("명")[0];
    }
    if (this.state.addressId) {
      info["memAddressId"] = this.state.addressId;
    }
    
    info["memBirthday"] = this.state.yearsSelected.substr(0,4) + this.state.monthSelected.substr(0, 2) + this.state.daySelected.substr(0, 2);
    info["lifeCycles"] = lifeCycles;    
    info["houseTypes"] = houseTypes;
    info["desires"] = desires;    
    info["disables"] = disables;
    info["targetCharacters"] = targetCharacters;
    info["jobs"] = jobs;
    info["families"] = families;


    console.log(info);
    UserModel.getInternalMemkey((memkey) => {
      UserModel.putMember(memkey, info, (err, res) => {
        if (err) {
          console.log(err);
        }
        // 메인 스크린의 복지정보 업데이트
        this.state.refreshDelayDone = true;
        this.props.onDrawerRefresh();
        this.setState({saveEnabled: false});
      });
    });
  }

  initData = () => {
    let _years = [];
    for (var i = 0; i < 120; ++i) {
      _years.push({value: 1900+i + '년'});
    }

    this.state.years = _years;

    // 월; 생년월일
    let _month= [];
    for (var i = 1; i < 10; ++i) 
      _month.push({value:  '0' + i + '월'});
    _month.push({value: '11월'});
    _month.push({value: '12월'});

    this.state.month = _month;

    // 일; 생년월일
    let _day = [];
    let date = new Date();
    for (var i = 1; i < 10; ++i)  
      _day.push({value: "0" + i + "일"})
    for (var i = 10; i < 31; ++i) 
      _day.push({value: i + "일"});
  
    this.state.day = _day;

    let _disabled = [{value: "해당없음"}, {value: "간"}, {value: "간질"}, {value: "뇌병변"}, {value: "시각"}, 
                     {value: "신장"}, {value: "심장"}, {value: "안면"}, {value: "언어"}, 
                     {value: "자폐성"}, {value: "장루"}, {value: "정신"}, {value: "지적"},
                     {value: "지체"}, {value: "청각"}, {value: "호흡기"}];

    this.state.disabled = _disabled;


    let _houseType = [{value: "해당없음"}, {value: "다문화"}, {value: "독거노인"}, {value: "새터민"}, 
                      {value: "소년소녀가장"}, {value: "조손"}, {value: "한부모"}]

    this.state.houseType = _houseType;

    let _desisres = [{value: "가족관계"}, {value: "건강"}, {value: "경제"}, {value: "고용"},
                     {value: "교육"}, {value: "법률 및 권익보장"}, {value: "사회적 관계"}, {value: "생활환경"},
                     {value: "안전"}, {value: "일상생활유지"}];

    this.state.desires = _desisres;

    let _family = [{value: "1명"}, {value: "2명"}, {value: "3명"}, {value: "4명"},
                   {value: "5명"}, {value: "6명"}, {value: "7명"}, {value: "8명"}];

    this.state.family = _family;

    let _targetCharacter = [{value: "해당없음"}, {value: "국가유공자"}, {value: "임산부"}, {value: "실업자"}, {value: "장애"}];

    this.state.targetCharacters = _targetCharacter;
  }

  getRegion = async (initSido, initSigungu) => {
    RegionModel.getSiDo(1, (err, res) => {
      let _sido = this.state.sido;
      for (var i = 0; i < res.results.length; ++i){
        _sido.push({value: res.results[i].siDoName, sidoId: res.results[i].siDoId});
        this.state.sidoIdMap[res.results[i].siDoName] = res.results[i].siDoId;
      }
      RegionModel.getSiDo(2, (err, res) => {
        for (var i = 0; i < res.results.length; ++i) {
          _sido.push({value: res.results[i].siDoName, sidoId: res.results[i].siDoId});
          this.state.sidoIdMap[res.results[i].siDoName] = res.results[i].siDoId;
        }
        this.setState({sido: _sido});
        console.log(this.state.sidoIdMap);
        console.log(initSido);
        console.log(this.state.sidoIdMap[initSido]);
        this.getSiGunGu(this.state.sidoIdMap[initSido]);
      });
    });
    
  }

  getSiGunGu = async (sidoId, page=1) => {
    RegionModel.getSiGunGu(page, sidoId, (err, res) => {
        if (err) {
           console.log(err);
           return;
        }
        let _sigungu = this.state.sigungu;
        for (var i = 0; i < res.results.length; ++i){
          _sigungu.push({value: res.results[i].siGunGuName});
          this.state.sigunguIdMap[res.results[i].siGunGuName] = res.results[i].siGunGuId;
        }
        this.state.sigungu = _sigungu;

        if (res.next != null) {
          this.getSiGunGu(sidoId, page+1);        
        } else {
          this.setState({sigungu: _sigungu, sigunguSelected: _sigungu.length > 0 ? undefined : "----"});
        }
    });
  }

  render() {
  	return (
  		<DrawerScreen
        ref='_drawer'
        onSaveClick={this.onSaveClick}
        saveEnabled={this.state.saveEnabled}
  			onGenderChange={this.onGenderChange}
        years={this.state.years}
        yearsSelected={this.state.yearsSelected}
        onYearChange={this.onYearChange}
        month={this.state.month}
        monthSelected={this.state.monthSelected}
        onMonthChange={this.onMonthChange}
        day={this.state.day}
        daySelected={this.state.daySelected}
        onDayChange={this.onDayChange}
        sido={this.state.sido}
        sidoSelected={this.state.sidoSelected}
        onSidoChange={this.onSidoChange}
        sigungu={this.state.sigungu}
        sigunguSelected={this.state.sigunguSelected}
        onSigunguChange={this.onSigunguChange}
        sigungu={this.state.sigungu}
        onIncomeChange={this.onIncomeChange}
        disabled={this.state.disabled}
        disabledSelected={this.state.disabledSelected}
        onDisabledChange={this.onDisabledChange}
        onBohunChange={this.onBohunChange}
        bohun={this.state.bohun}
        onDesiresChange={this.onDesiresChange}
        desiresSelected={this.state.desiresSelected}
        desires={this.state.desires}
        onHouseTypeChange={this.onHouseTypeChange}
        houseTypeSelected={this.state.houseTypeSelected}
        houseType={this.state.houseType}
        onTargetCharactersChange={this.onTargetCharactersChange}
        targetCharactersSelected={this.state.targetCharactersSelected}
        targetCharacters={this.state.targetCharacters}
  			onPress={this.onPress}
        family={this.state.family}
        familySelected={this.state.familySelected}
        onFamilyChange={this.onFamilyChange}
  		>
  		</DrawerScreen>
  	);
    }
}

// 메인화면 복지 정보 업데이트를 위한 Dispatcher
let mapDispatchToProp = (dispatch) => {
  return {
    onDrawerRefresh: () => dispatch(drawerChanged())
  };
}

DrawerScreenViewModel = connect(undefined, mapDispatchToProp)(DrawerScreenViewModel);

export default DrawerScreenViewModel;