
import React, {Component} from 'react';
import {View} from 'react-native';
import { NavigationActions } from 'react-navigation';
import AlarmListScreen from 'src/view/screen/AlarmListScreen';
import BasicText from 'src/view/components/BasicText';
import BackIcon from 'src/view/components/BackIcon';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

export default class AlarmListScreenViewModel extends Component<Prop> {
  static navigationOptions = ({ navigation }) => {

    const {state} = navigation;
  
    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

	  return { 
		  headerTitle: <BasicText title="나도혹시" size={18}/>,
	    headerLeft: <BackIcon onPress={onBackIconClick} />,
	  }    
  };

  constructor(props) {
	  super(props);
    this.state = {loading: true, page: 1, data: []};
    PushNotification.configure({
      permissions: {
        alret: true,
        badge: true,
        sound: true
      },
      senderID: "982265184100",
      onNotification: (notification) => {
        console.log('notification received');
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      requestPermissions: true,
    });

    // PushNotification.localNotification({
    //   title: 'Hello',
    //   message: 'Hello World',
    // });

  }

  render() {
	  return (
	    <AlarmListScreen 
		    data={this.state.data}>
	    </AlarmListScreen>
	  );
  }
}