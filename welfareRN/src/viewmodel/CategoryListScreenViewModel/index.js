
import React, {Component} from 'react';
import CategoryListScreen from 'src/view/screen/CategoryListScreen';
import { NavigationActions } from 'react-navigation';
import {DrawerActions} from 'react-navigation-drawer';
import BasicText from 'src/view/components/BasicText';
import NotificationIcon from 'src/view/components/NotificationIcon';
import BackIcon from 'src/view/components/BackIcon';
import WelfareModel from 'src/model/WelfareModel';

export default class CategoryListScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation }) => {

    const {state} = navigation;
  
    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

    return { 
    	headerTitle: <BasicText title="나도혹시" size={18}/>,
      headerLeft: <BackIcon onPress={onBackIconClick} />,
      headerStyle: {
        elevation: 0,
        borderBottomWidth: 0,
      },
    }    
  };

  constructor(props) {
  	super(props);
    this.state = {welfareList: [], welfareListCount: 0, queryString: "", keyword: "", viewLoading: true, internalLoading: true, page: 1};
    let category = this.props.navigation.state.params.category;
    let keyword = this.props.navigation.state.params.keyword;
    let queryString = this.props.navigation.state.params.queryString;
    let curURL = this.props.navigation.state.params.curURL;
    let nextURL = this.props.navigation.state.params.nextURL;
    if (category) {
      if (category == '교육') {
        queryString = "?desire=6000000";
      } else if (category == '금융') {
        queryString = "?desire=5000000";
      } else if (category == '다문화') {
        queryString = "?houseType=003";
      } else if (category == '장애인') {
        queryString = "?targetCharacter=004";
      } else if (category == '고용') {
        queryString = "?desire=7000000";
      } else if (category == '건강') {
        queryString = "?desire=1000000";
      } else if (category == '문화') {
        queryString = "?desire=8000000";
      } else if (category == '주거') {
        queryString = "?";
      } else {}
      this.state.queryString = queryString;
      this.state.keyword = undefined;
      this.getWelfare(queryString);
    } else if (keyword) {
      this.state.keyword = keyword;
      this.state.queryString = undefined;
      this.getWelfareWithKeyword(keyword);
    } else {
      this.state.keyword = undefined;
      this.state.queryString = queryString;
      this.getWelfare(queryString);      
    }
  }

  getWelfare = (queryString, page=1) => {
    this.state.internalLoading = true;
    WelfareModel.getWelfareWithQS(queryString, page, (err, res) => {
      if (err) {
        return;
      }

      let recv = this.state.welfareList;
      for (var i = 0; i < res.results.length; ++i) {
        res.results[i]['i'] = this.state.welfareListCount;
        recv.push(res.results[i]);
        this.state.welfareListCount += 1;
      }

      this.setState({welfareList: recv, viewLoading: false, internalLoading: false});
    });
  }

  getWelfareWithKeyword = (keyword, page=1) => {
    this.state.internalLoading = true;
    WelfareModel.getWelfareWithKeyword(keyword, page, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }

      let recv = this.state.welfareList;
      for (var i = 0; i < res.results.length; ++i) {
        res.results[i]['i'] = this.state.welfareListCount;
        recv.push(res.results[i]);
        this.state.welfareListCount += 1;
      }

      this.setState({welfareList: recv, viewLoading: false, internalLoading: false});      
    });
  }

  getChatBotWelfare = () => {
    this.state.internalLoading = true;
    if (this.state.curURL == undefined) return;
    WelfareModel.getWithURL(this.state.curURL, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }

      let recv = this.state.welfareList;
      for (var i = 0; i < res.results.length; ++i) {
        res.results[i]['i'] = this.state.welfareListCount;
        recv.push(res.results[i]);
        this.state.welfareListCount += 1;
      }

      this.setState({welfareList: recv, viewLoading: false, internalLoading: false, curURL: this.state.nextURL, nextURL: res.next});      

    });
  }

  onEndReached = () => {
    if (!this.state.internalLoading) {
      if (this.state.queryString) {
        this.getWelfare(this.state.queryString, this.state.page+1);
        this.setState({page: this.state.page+1});
      }
      if (this.state.keyword) {
        this.getWelfareWithKeyword(this.state.keyword, this.state.page+1);
        this.setState({page: this.state.page+1});
      }
    }
  }

onWelfareInfoClick = (pos) => {
  this.props.navigation.navigate('Info', this.state.welfareList[pos]);
}

  render() {
	  return (
		  <CategoryListScreen
			  onPress={this.onPress}
        onWelfareInfoClick={this.onWelfareInfoClick}
        onEndReached={this.onEndReached}
        category={this.props.navigation.state.params.category}
        keyword={this.state.keyword}
        welfareList={this.state.welfareList}
        loading={this.state.viewLoading}
		  >
		  </CategoryListScreen>
	  );
  }
}
