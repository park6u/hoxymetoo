/*
프로그램 ID:AC-2020-JS
프로그램명:RegisterViewModel/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.14
버전:0.6
설명
- 회원가입 스크린 뷰모델
*/

import React, {Component} from 'react';
import {Alert} from 'react-native';
import Register from 'src/view/screen/Register';
import UserModel from 'src/model/UserModel';
import RegionModel from 'src/model/RegionModel';

export default class RegisterViewModel extends React.Component {
  constructor(props) {
	  super(props);
    // console.log(this.props.navigation.getParam('alreadyRegistered', ''));
    this.state = {
      alreadyRegistered: this.props.navigation.getParam('alreadyRegistered', ''),
      nextNameEnabled: false,
      socialId: '',
      pushToken: '',
      nextFirstEnabled: false,
      name: '복지',
      gender: -1,
      email: 'pkjoho95',
      emailLaterCheck: false,
      nextSecondEnabled: false,
      birthday: -1,
      sido: [],
      sidoIdMap: {},
      sidoSelected: undefined,
      sigungu: [],
      sigunguIdMap: {},
      sigunguSelected: undefined,
      addressId: 2,
      disabled: '비장애인',
      disabledKind: '',
      nextCompleteEnabled: false,
      birthday: '',
      family: '가족관계',
      disease: '없음',
      bohun: -1,
      privacyCheck: false,
    };
    this.getRegion();
  }

  componentDidMount() {
    let socialId = this.props.navigation.getParam('socialId', '');
    let pushToken = this.props.navigation.getParam('pushToken', '');
    // 유효하지 않은 접근
    if (socialId == '' || pushToken == '') {

    } else {
      this.state.socialId = socialId;
      this.state.pushToken = pushToken;
    }
    let emailHolder = this.props.navigation.getParam('email', '');
    let nameHolder = this.props.navigation.getParam('name', '');
    this.onNameChange(nameHolder);
    this.onEmailChange(emailHolder);
  }

  // 이미 등록된 회원의 경우 이름만 입력후 저장
  onNameRegister = () => {
    UserModel.registerInternalName(this.state.name);
    this.props.navigation.navigate('Main');
  }

  // 첫번째 회원가입 화면(이름, 성별, 이메일) 다음으로 넘어가기 버튼
  onFirstNextPress = () => {
    this.setState({nextSecondEnabled: false});
    this.refs._register.setPageTo(1);
  }

  // 회원가입 폼 체크
  checkCompleteFirst = async (_name, _gender, _email) => {
    let name = _name ? _name : this.state.name;
    let gender = _gender ? _gender : this.state.gender;
    let email = _email ? _email : this.state.email;

    const regexName = '^[가-힣A-Za-z]{2,8}$';
    if (name == null || name == undefined || name.match(regexName) == null) {
      this.setState({nextFirstEnabled: false});
      return;
    }

    if (gender != 1 && gender != 2) {
      this.setState({nextFirstEnabled: false});
      return;
    }

    const emailRegex = '^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$';
    if (!this.state.emailLaterCheck && email.match(emailRegex) == null) {
      this.setState({nextFirstEnabled: false});
      return;
    } 

    this.setState({nextFirstEnabled: true});
  }

  // 회원가입 폼 체크
  checkCompleteSecond = async (_birthday, _sido, _sigungu, _disabled, _disabledKind) => {
    let birthday = _birthday ? _birthday : this.state.birthday;
    let sido = _sido ? _sido : this.state.sido;
    let sigungu = _sigungu ? _sigungu : this.state.sigungu;
    let disabled = _disabled ? _disabled : this.state.disabled;
    let disabledKind = _disabledKind ? _disabledKind : this.state.disabledKind;

    const regexBirthday = "^[0-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$";
    if (birthday.match(regexBirthday) == null) {
      this.setState({nextSecondEnabled: false});
      return;
    }

    this.setState({nextSecondEnabled: true});
  }

  // 회원가입 폼 체크
  checkCompleteRegister = async (_family, _disease, _bohun) => {
    let family = _family ? _family : this.state.family;
    let disease = _disease ? _disease : this.state.disease;
    let bohun = _bohun ? _bohun : this.state.bohun;

    if (family == '가족관계') {
      this.setState({nextCompleteEnabled: false});
      return;
    }

    if (bohun == -1) {
      this.setState({nextCompleteEnabled: false});
      return;
    }

    if (!this.state.privacyCheck) {
      this.setState({nextCompleteEnabled: false});
      return;
    }

    this.setState({nextCompleteEnabled: true});    
  }

  // 이름입력 폼 체크
  checkCompleteName = async (_name) => {
    let name = _name ? _name : this.state.name;

    let regexName = '^[가-힣A-Za-z]{2,8}$';
    if (name == null || name == undefined || name.match(regexName) == null) {
      this.setState({nextNameEnabled: false});
      return;
    }

    this.setState({nextNameEnabled: true});
  }

   /*
    {
        "socialId": "",
        "pushToken": "",
        "memEmail": "",
        "memBirthday": "",
        "memGender": "",
        "memFamily": "",
        "membirthday": "",
        "memBohun": "",
        "memAddressId": null
    }
   */
  // 현재 state의 값들을 api 포맷에 맞춤
  getFilteredState = () => {
    let familyCount = Number(this.state.family.charAt(0));

    return {
      pushToken: this.state.pushToken,
      socialId: this.state.socialId,
      memEmail: this.state.email,
      memGender: String(this.state.gender),
      memFamily: String(familyCount),
      memBohun: String(this.state.bohun),
      memBirthday: (1900 + Number(this.state.birthday.substr(0,2))) + this.state.birthday.substr(2, 4),
      memAddressId: String(this.state.addressId),
    }
  }

  getRegion = async () => {
    RegionModel.getSiDo(1, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }

      let _sido = this.state.sido;
      this.state.sidoIdMap = {};
      for (var i = 0; i < res.results.length; ++i){
        _sido.push({value: res.results[i].siDoName, sidoId: res.results[i].siDoId});
        this.state.sidoIdMap[res.results[i].siDoName] = res.results[i].siDoId;
      }
      RegionModel.getSiDo(2, (err, res) => {
        for (var i = 0; i < res.results.length; ++i) {
          _sido.push({value: res.results[i].siDoName, sidoId: res.results[i].siDoId});
          this.state.sidoIdMap[res.results[i].siDoName] = res.results[i].siDoId;
        }
        this.setState({sido: _sido, sidoSelected: '서울특별시'});
        this.getSiGunGu(this.state.sidoIdMap['서울특별시']);
      });
    }); 
  }

  getSiGunGu = async (sidoId, page=1) => {
    RegionModel.getSiGunGu(page, sidoId, (err, res) => {
        if (err) {
           console.log(err);
           return;
        }
        let _sigungu = this.state.sigungu;
        for (var i = 0; i < res.results.length; ++i){
          _sigungu.push({value: res.results[i].siGunGuName, siGunGuId: res.results[i].siGunGuId});
          this.state.sigunguIdMap[res.results[i].siGunGuName] = res.results[i].siGunGuId;
        }
        this.state.sigungu = _sigungu;

        if (res.next != null) {
          this.getSiGunGu(sidoId, page+1);
        } else {
          this.setState({sigunguSelected: _sigungu.length > 0 ? _sigungu[0].value : ""});
        }
    });
  }

  // 회원가입 api/members POST; 회원가입 실행 로직
  completeRegister = async () => {
    let filtered = this.getFilteredState();
    console.log(filtered)
    UserModel.registerUser(filtered, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }

      this.props.navigation.navigate('Guide');
    });
    UserModel.registerInternalName(this.state.name);
  }

  // 두번쨰 회원가입 화면(소득분위, 주소, 비장애인 여부) 다음으로 넘어가기 버튼
  onSecondNextPress = () => {
    this.refs._register.setPageTo(2);
  }

  // 세번째 회원가입 화면() 회원가입 완료 버튼 
  onRegisterComplete = () => {
    this.completeRegister();
  }

  onNameChange = (text) => {
    this.setState({name: text});
    this.checkCompleteFirst(text, false, false);
    this.checkCompleteName(text);
  }

  onGenderChange = (sex) => {
    this.setState({gender: sex});
    this.checkCompleteFirst(false, sex, false);
  }

  onEmailChange = (text) => {
    this.setState({email: text});
    this.checkCompleteFirst(false, false, text);
  }

  onEmailLaterCheckChange = (name, checked) => {
    this.setState({emailLaterCheck: checked}, () => {
      this.checkCompleteFirst(false, false, "");
    });
  }

  onBirthdayChange = (num) => {
    this.setState({birthday: num});   
    this.checkCompleteSecond(num, false, false, false, false);
  }

  onSiDoChange = (text) => {
    this.state.sigungu =[];
    this.state.sigunguIdMap = {};
    this.state.sidoSelected = text;
    this.getSiGunGu(this.state.sidoIdMap[text]);
  }

  onSiGunGuChange = (text) => {
    this.state.sigunguSelected = text;
//    console.log(this.state.sidoIdMap, this.state.sigunguIdMap);
//    console.log(this.state.sidoIdMap[this.state.sidoSelected], this.state.sigunguIdMap[this.state.sigunguSelected]);
    RegionModel.getAddressId(this.state.sidoIdMap[this.state.sidoSelected], this.state.sigunguIdMap[this.state.sigunguSelected], (res) => {
      console.log(res);
      this.setState({addressId: res});
    });
  }

  // LEGACY
  onDisabledChange = (text) => {
    this.setState({disabled: text});
    if (text == '장애인') {
      this.setState({disabledKind: '청각'});
    } else {
      this.setState({disabledKind: ''});      
    }
    this.checkCompleteSecond(false, false, false, text, false);
  }

  // LEGACY
  onDisabledKindChange = (text) => {
    this.setState({disabledKind: text});
    this.checkCompleteSecond(false, false, false, false, text);
  }

  onFamilyChange = (text) => {
    this.setState({family: text});
    this.checkCompleteRegister(text, false, false);
  }

  // LEGACY
  onDiseaseChange = (text) => {
    this.setState({disease: text});
    this.checkCompleteRegister(false, text, false);
  }

  onBohunChange = (text) => {
    this.setState({bohun: text});
    this.checkCompleteRegister(false, false, text);
  }

  onPrivacyChange = (name, checked) => {
    this.setState({privacyCheck: checked}, () => {
      this.checkCompleteRegister(false, false, false);
    });

  }

  render() {
  	return (
  		<Register
        ref = '_register'
        alreadyRegistered={this.state.alreadyRegistered}
  			onPress={this.onPress}
  			onFirstNextPress={this.onFirstNextPress}
  			onSecondNextPress={this.onSecondNextPress}
  			onRegisterComplete={this.onRegisterComplete}
        nextFirstEnabled={this.state.nextFirstEnabled}
        name={this.state.name}
        onNameChange={this.onNameChange}
        email={this.state.email}
        onEmailLaterCheckChange={this.onEmailLaterCheckChange}
        onEmailChange={this.onEmailChange}
        gender={this.state.gender}
        onGenderChange={this.onGenderChange}
        nextSecondEnabled={this.state.nextSecondEnabled}
        birthday={this.state.birthday}
        onBirthdayChange={this.onBirthdayChange}
        sido={this.state.sido}
        sidoSelected={this.state.sidoSelected}
        onSiDoChange={this.onSiDoChange}
        sigungu={this.state.sigungu}
        sigunguSelected={this.state.sigunguSelected}
        onSiGunGuChange={this.onSiGunGuChange}
        disabled={this.state.disabled}
        onDisabledChange={this.onDisabledChange}
        disabledKind={this.state.disabledKind}
        onDisabledKindChange={this.onDisabledKindChange}
        nextCompleteEnabled={this.state.nextCompleteEnabled}
        family={this.state.family}
        onFamilyChange={this.onFamilyChange}
        disease={this.state.disease}
        onDiseaseChange={this.onDiseaseChange}
        bohun={this.state.bohun}
        onBohunChange={this.onBohunChange}
        onPrivacyChange={this.onPrivacyChange}
        nextNameEnabled={this.state.nextNameEnabled}
        onNameRegister={this.onNameRegister}
  		/>
  	)
  }
}