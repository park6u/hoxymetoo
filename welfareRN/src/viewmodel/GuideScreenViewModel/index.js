
import React, {Component} from 'react';
import GuideScreen from './../../view/screen/GuideScreen'

export default class GuideScreenViewModel extends React.Component {
  constructor(props) {
    super(props)
  }

  onCompletePress = (e) => {
	this.props.navigation.navigate('Main')
  }

  onFirstNextPress = () => {
  	this.refs._guide.setPageTo(1);
  }

  onSecondNextPress = () => {
    this.refs._guide.setPageTo(2);
  }

  render() {
	return (
		<GuideScreen
			ref='_guide'
			onCompletePress={this.onCompletePress}
			onFirstNextPress={this.onFirstNextPress}
      onSecondNextPress={this.onSecondNextPress}
		>
		</GuideScreen>
	)	
  }
}