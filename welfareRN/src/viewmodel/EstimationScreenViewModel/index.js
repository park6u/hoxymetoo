
import React, {Component} from 'react';
import EstimationScreen from 'src/view/screen/EstimationScreen';
import {DrawerActions} from 'react-navigation-drawer';
import BasicText from 'src/view/components/BasicText';
import NotificationIcon from 'src/view/components/NotificationIcon';
import DrawerIcon from 'src/view/components/DrawerIcon';
import UserModel from 'src/model/UserModel';

export default class EstimationScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation  }) => {

    const {state} = navigation;

    if(state.params != undefined){
        return { 
        	headerTitle: <BasicText title="혹시나도" size={18}/>,
            headerLeft: <DrawerIcon openDrawer={state.params.onNavigationClick}/>,
            headerRight: <NotificationIcon onClick={state.params.onAlarmListClick}/>,
            headerStyle: {
              elevation: 0,
              borderBottomWidth: 0,
            }

        }
    }
    
  };

  constructor(props) {
  	super(props);
    this.state = {loading: true, receivableMoney: "0", name: ""};
  	this.props.navigation.setParams({
  	  onNavigationClick: this.onNavigationClick,
      onAlarmListClick: this.onAlarmListClick,
  	});
    this.getMember();
  }

  onNavigationClick = () => {
	  this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  onAlarmListClick = () => {
    this.props.navigation.navigate('Alarm');    
  }

  onChatBotClick = () => {
    this.props.navigation.navigate('ChatBot'); 
  }

  onFindMoneyClick = () => {
    this.props.navigation.navigate('Description');
  }

  // 문자열 반전
  _reverse = (string) => {
    let reversed = "";
    for (var i = 0; i < string.length; ++i) {
      reversed += string[string.length - i - 1];
    }
    return reversed;
  }

  // 금액 Delimiter
  _makeDelim = (string) => {
    let delimiteredString = "";

    for (var i = 1; i <= string.length; ++i) {
      delimiteredString += string[string.length - i];
      if (i % 3 == 0 && i !== string.length) {
        delimiteredString += ",";
      }
    }

    return this._reverse(delimiteredString);
  }

  // 유저 정보 가져옴
  getMember = () => {
    UserModel.getInternalMemkey((memKey) => {
      UserModel.getMember(memKey, (err, res) => {
        if (err) {
          return;
        }
        UserModel.getInternalName((_name) => {
          this.setState({receivableMoney: this._makeDelim(String(res.memReceivableMoney)), name: _name, loading: false});          
        })
      });
    });
  }

  render() {
	  return (
		  <EstimationScreen
        loading={this.state.loading}
        name={this.state.name}
        receivableMoney={this.state.receivableMoney}
			  onFindMoneyClick={this.onFindMoneyClick}
        onChatBotClick={this.onChatBotClick}
		  >
		  </EstimationScreen>
	  );
  }
}
