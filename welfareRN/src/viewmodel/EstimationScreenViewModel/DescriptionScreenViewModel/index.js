
import React, {Component} from 'react';
import DescriptionScreen from 'src/view/screen/EstimationScreen/DescriptionScreen';
import {DrawerActions} from 'react-navigation-drawer';
import { NavigationActions } from 'react-navigation';
import BasicText from 'src/view/components/BasicText';
import BackIcon from 'src/view/components/BackIcon';
import UserModel from 'src/model/UserModel';

export default class DescriptionViewModel extends React.Component {
  static navigationOptions = ({ navigation  }) => {

    const {state} = navigation;
    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

    if(state.params != undefined){
        return { 
          headerTitle: <BasicText title="나도혹시" size={18}/>,
          headerLeft: <BackIcon onPress={onBackIconClick}/>,
        }
    }
    
  };

  constructor(props) {
    super(props);
    this.state = {loading: true, receivableMoney: "0", receivableMoneyDesc: "", name: ""};
    this.props.navigation.setParams({
      onNavigationClick: this.onNavigationClick,
      onAlarmListClick: this.onAlarmListClick,
    });
    this.getMember();
  }

  onNavigationClick = () => {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  // 문자열 반전
  _reverse = (string) => {
    let reversed = "";
    for (var i = 0; i < string.length; ++i) {
      reversed += string[string.length - i - 1];
    }
    return reversed;
  }

  // 금액 Delimiter
  _makeDelim = (string) => {
    let delimiteredString = "";

    for (var i = 1; i <= string.length; ++i) {
      delimiteredString += string[string.length - i];
      if (i % 3 == 0 && i !== string.length) {
        delimiteredString += ",";
      }
    }

    return this._reverse(delimiteredString);
  }

  // 유저 정보 가져옴
  getMember = () => {
    UserModel.getInternalMemkey((memKey) => {
      UserModel.getMember(memKey, (err, res) => {
        if (err) {
          return;
        }
        UserModel.getInternalName((_name) => {
          this.setState({receivableMoney: this._makeDelim(String(res.memReceivableMoney)), 
            receivableMoneyDesc: res.memReceivableMoneyDescribe,
            name: _name, loading: false});          
        })
      });
    });
  }

  render() {
    return (
      <DescriptionScreen
        loading={this.state.loading}
        name={this.state.name}
        receivableMoney={this.state.receivableMoney}
        receivableMoneyDesc={this.state.receivableMoneyDesc}
        onPress={this.onPress}
        onChatBotClick={this.onChatBotClick}
      >
      </DescriptionScreen>
    );
  }
}
