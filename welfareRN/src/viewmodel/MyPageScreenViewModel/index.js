
import React, {Component} from 'react';
import MyPageScreen from 'src/view/screen/MyPageScreen';
import {DrawerActions} from 'react-navigation-drawer';
import BasicText from 'src/view/components/BasicText';
import NotificationIcon from 'src/view/components/NotificationIcon';
import DrawerIcon from 'src/view/components/DrawerIcon';
import UserModel from 'src/model/UserModel';

export default class MyPageScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation }) => {

    const {state} = navigation;

    if(state.params != undefined){
        return { 
        	headerTitle: <BasicText title="혹시나도" size={18}/>,
            headerLeft: <DrawerIcon openDrawer={state.params.onNavigationClick}/>,
            headerRight: <NotificationIcon onClick={state.params.onAlarmListClick} />,
            headerStyle: {
              elevation: 0,
              borderBottomWidth: 0,
            }
        }
    }
    
  };

  constructor(props) {
	  super(props);
	  this.props.navigation.setParams({
	    onNavigationClick: this.onNavigationClick,
      onAlarmListClick: this.onAlarmListClick,
 	  });
    this.state = {loading: true};
    UserModel.getInternalName((_name) => {
      this.setState({name: _name});
      UserModel.getInternalMemkey((memkey) => {
        UserModel.getMember(memkey, (err, res) => {
          this.setState({loading: false, 
            disabled: res.disables,
            birthday: res.memBirthday,
            gender: res.memGender,
            bohun: res.memBohun,
            addr: res.memAddressId});
        });
      });
    });

  }

  onNavigationClick = () => {
	  this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  onAlarmListClick = () => {
    this.props.navigation.navigate('Alarm');    
  }

  onChatBotClick = () => {
    this.props.navigation.navigate('ChatBot');
  }

  onSettingClick = () => {
    this.props.navigation.navigate('Setting');
  }

  render() {
	return (
		<MyPageScreen
			onPress={this.onPress}
      name={this.state.name}
      addr={this.state.addr}
      gender={this.state.gender}
      birthday={this.state.birthday}
      disabled={this.state.disabled}
      bohun={this.state.bohun}      
      onChatBotClick={this.onChatBotClick}
      onSettingClick={this.onSettingClick}
		>
		</MyPageScreen>
	);
  }
}
