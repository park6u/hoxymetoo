/*
프로그램 ID:AC-2020-JS
프로그램명:MainScreenViewModel/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.15
버전:0.6
설명
 메인 스크린 뷰모델
*/

import React, {Component} from 'react';
import {View, Text, TouchableHighlight, Image} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import MainScreen from 'src/view/screen/MainScreen';
import BasicText from 'src/view/components/BasicText';
import NotificationIcon from 'src/view/components/NotificationIcon';
import DrawerIcon from 'src/view/components/DrawerIcon';
import UserModel from 'src/model/UserModel';
import WelfareModel from 'src/model/WelfareModel';
import {connect} from 'react-redux';

class MainScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation }) => {

    const {state} = navigation;

    if(state.params != undefined){
        return { 
        	headerTitle: <BasicText title="혹시나도" size={18}/>,
          headerLeft: <DrawerIcon openDrawer={state.params.onNavigationClick}/>,
          headerRight: <NotificationIcon onClick={state.params.onAlarmListClick}/>,
          headerStyle: {
            elevation: 0,
            borderBottomWidth: 0,
          }
        }
    }
    
  };

  constructor(props) {
    super(props);
    this.state = {
      viewLoading: true, 
      internalLoading: true, 
      page: 1, 
      welfareIdList: [], 
      welfareList: [], 
      welfareListCount: 0, 
      beforeRefreshCount: 0,
    };
  }

  // Drawer에서 Refresh 되는 경우 Reducer를 통해 Prop Change가 트리거된다.
 componentWillReceiveProps(nextProps) {
    if (this.state.beforeRefreshCount !== nextProps.count) {
      this.closeNavigation();
      this.setState({viewLoading: true, beforeRefreshCount: nextProps.count, beinternalLoading: true, page: 1, welfareIdList: [], welfareList: [], welfareListCount: 0}, () => {
      this.getMemberWelfare();
    });
    }
 }

  componentDidMount() {
  	this.props.navigation.setParams({
	    onNavigationClick: this.onNavigationClick,
      onAlarmListClick: this.onAlarmListClick,
 	  });
    this.getMemberWelfare();
    UserModel.getInternalName((name) => {
      this.setState({userName: name});        
    });
    
  }

  getMemberWelfare = async () => {
    UserModel.getInternalMemkey((memkey) => {
      WelfareModel.getMemberWelfare(memkey, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }
          this.setState({welfareIdList: res}, () => {
            if (res.length > 0) {  
              this.getEachWelfare();
            } else {
              this.setState({interanlLoading: true, viewLoading: false});
            }
          });
          this.props.navigation.setParams({
            badgeCount: this.state.welfareIdList.length > 9 ? '9+' : String(this.state.welfareIdList.length) 
          });
      });
    });
  }

  getEachWelfare = async (page=1) => {
    // 받아온 각 WelfareList에 대해 호출
    this.state.internalLoading = true;
    let last = page * 20  > this.state.welfareIdList.length ? this.state.welfareIdList.length : page * 20;
    for (var i = (page - 1) * 20; i < last; ++i) {
      WelfareModel.getWelfareWithId(this.state.welfareIdList[i], (err, res) => {
        if (err) {
          return;
        }
        let recv = this.state.welfareList;
        res['i'] = this.state.welfareListCount;
        recv.push(res);
        this.setState({welfareList: recv});
        this.state.welfareListCount += 1;
        if (this.state.welfareListCount == last - 1){
          this.setState({internalLoading: false, viewLoading: false});
        }
      });
    }
  }

  onWelfareInfoClick = (pos) =>  {
    this.props.navigation.navigate('Info', this.state.welfareList[pos]);
  }

  onEndReached = () => {
    if (!this.state.internalLoading) {
      this.getEachWelfare(this.state.page+1);
      this.setState({page: this.state.page+1});
    }
  }

  onNavigationClick = () => {
	  this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  closeNavigation = () => {
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  }

  onAlarmListClick = () => {
    this.props.navigation.navigate('Alarm');    
  }

  onChatBotClick = () => {
    this.props.navigation.navigate('ChatBot');
  }

  render() {
	  return (
			<MainScreen
        userName={this.state.userName}
        onWelfareInfoClick={this.onWelfareInfoClick}
        onEndReached={this.onEndReached}
        onChatBotClick={this.onChatBotClick}
				onNavigationClick={this.onNavigationClick}
				onPress={this.onPress}
        loading={this.state.viewLoading}
        welfareList={this.state.welfareList}
			>
			</MainScreen>
	  );
  }
}

let mapStateToProp = (state) => {
  return {
    count: state.refresh.count
  };
}

MainScreenViewModel = connect(mapStateToProp)(MainScreenViewModel);

export default MainScreenViewModel;
