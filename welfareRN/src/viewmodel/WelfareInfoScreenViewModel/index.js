/*
프로그램 ID:AC-2020-JS
프로그램명:WelfareInfoScreenViewModel/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.26
버전:0.6
설명
- 복지 정보 스크린 뷰모델
*/

import React, {Component} from 'react';
import { NavigationActions } from 'react-navigation';
import WelfareInfoScreen from 'src/view/screen/WelfareInfoScreen';
import BackIcon from 'src/view/components/BackIcon';
//import WelfareModel from 'src/model/WelfareModel';

export default class WelfareInfoScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation }) => {

    const {state} = navigation;

    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

    if(state.params != undefined){
        return { 
            headerLeft: <BackIcon onPress={onBackIconClick}/>
        }
    }
  };

  constructor(props) {
	  super(props)
  }

  // 복지 신청하기 클릭
  onApplyWelfare = () => {
    
  }

  render() {    
  	return (
  		<WelfareInfoScreen
        ref = '_welfareInfo'
        data={this.props.navigation.state.params}
  			onPress={this.onPress}
  			onApplyWelfare={this.onApplyWelfare}
  		/>
  	)	
  }
}