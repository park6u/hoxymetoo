import React, {Component} from 'react';
import PasswordScreen from 'src/view/screen/PasswordScreen';
import {DrawerActions} from 'react-navigation-drawer';
import {NavigationActions} from 'react-navigation';
import BasicText from 'src/view/components/BasicText';
import UserModel from 'src/model/UserModel';

export default class PasswordScreenViewModel extends React.Component {
  constructor(props) {
  	super(props);
  	this.state = {password: "", passwordLength: 0};
  }

  onButtonClick = (value) => {
  	switch(value) {
  	  case "←":
	    this.setState({passwordLength: this.state.passwordLength-1});
  	    break;
  	  default:
		this.setState({passwordLength: this.state.passwordLength+1, password: this.state.password + value});  	  
  	  	break
  	}
  }


  render() {
	  return (
		  <PasswordScreen
		    passwordLength={this.state.passwordLength}
		    onButtonClick={this.onButtonClick}
		  >
		  </PasswordScreen>
	  );
  }
}
