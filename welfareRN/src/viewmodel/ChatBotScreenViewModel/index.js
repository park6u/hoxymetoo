/*
프로그램 ID:AC-2020-JS
프로그램명:ChatBotScreenViewModel/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.08.11
버전:0.6
설명
 - 챗봇 스크린 ViewModel
*/

import React, {Component} from 'react';
import ChatBotScreen from 'src/view/screen/ChatBotScreen';
import { NavigationActions } from 'react-navigation';
import {DrawerActions} from 'react-navigation-drawer';
import SearchIcon from 'src/view/components/SearchIcon';
import BasicText from 'src/view/components/BasicText';
import BackIcon from 'src/view/components/BackIcon';
import ChatBotModel from 'src/model/ChatBotModel';
import WelfareModel from 'src/model/WelfareModel';
import RegionModel from 'src/model/RegionModel';
import EntityModel from 'src/model/ChatBotModel/EntityModel';

export default class ChatBotScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation }) => {

    const {naviState} = navigation;

    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

    return { 
      headerRight: <SearchIcon />,
      headerTitle: <BasicText title="혹시나도 챗봇"/>,
      headerLeft: <BackIcon onPress={onBackIconClick} />,
    }    
  };

  // 생성자
  constructor(props) {
    super(props);
    const queryList = ["40대가 받을 수 있는 복지?", "내가 받을 수 있는 복지는?", "수당 검색",
                       "서울시 강남구에 살고 있어요", "상담이 필요해요", "임산부가 받을 수 있는 복지"];
    const initSendList = ["내가 받을 수 있는 복지는?", "수당 검색", "서울시 강남구에 살고 있어요"];
    let WelcomeData = [{sender: "chatbot", sendText: "안녕하세요 나도혹시 챗봇입니다. 챗봇은 현재 개발이 진행 중이므로 불안정 할 수 있습니다."}, {sender: "chatbot", sendList: initSendList}];
    this.state = {msgData: WelcomeData, textInput: "", initSendList: ["내가 받을 수 있는 복지는?", "수당 검색", "서울시 강남구에 살고 있어요"]};
    this.getChatLogs();
    RegionModel.getAddressNameMap((idMap) => {
      this.state.addressNameMap = JSON.parse(idMap);
    });

    EntityModel.getDesireIdMap((idMap) => {
      this.state.desireMap = JSON.parse(idMap);
    });

    EntityModel.getHouseTypesIdMap((idMap) => {
      this.state.houseTypesMap = JSON.parse(idMap);
    });

    EntityModel.getLifeCycleIdMap((idMap) => {
      this.state.lifeCylcesMap = JSON.parse(idMap);
    });

    EntityModel.getTargetCharactersIdMap((idMap) => {
      this.state.targetCharactersMap = JSON.parse(idMap);
    });

  }

  // 사용자 메시지 추가
  addUserMsg = (msg) => {
    let pushedData = this.state.msgData;
    pushedData.push({sender: "user", sendText: msg});
    this.setState({msgData: pushedData});    
  }

  // 챗봇 메시지 추가
  addChatBotMsg = (msg) => {
    let pushedData = this.state.msgData;
    pushedData.push({sender: "chatbot", sendText: msg});
    this.setState({msgData: pushedData});    
  }

  // 챗봇 질의 결과 추가
  addChatBotResultMsg = (msgResultList, _keyword=undefined, _queryString=undefined) => {
    let pushedData = this.state.msgData;
    let message = {
      sender: "chatbot",
      sendResultList: msgResultList,
      keyword: _keyword,
      queryString: _queryString
    }
    pushedData.push(message);
    this.setState({msgData: pushedData}); 
  }

  // 챗봇 가이드 리스트 메시지 추가
  addChatBotListMsg = (msgList) => {
    let pushedData = this.state.msgData;
    pushedData.push({sender: "chatbot", sentList: msgList});
    this.setState({msgData: pushedData});
  }

  // 메시지 입력시 마다 자동으로 밑으로 스크롤
  onChatBotMsgAddComplete = async () => {
    //if (this.refs._chatbotscreen)
      this.refs._chatbotscreen.scrollToEnd();
  }

  onChangeText = (msg) => {
    this.setState({textInput: msg});
  }

  // DialogFlow로 사용자 질의
  queryToDialogFlow = (msg) => {
    ChatBotModel.queryToDF(msg, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log(res.result);
      this.processQuery(res.result.metadata.intentName, res.result.contexts, res.result.action, res.result.actionIncomplete, res.result.parameters, res.result.resolvedQuery);
      this.addChatBotMsg(res.result.fulfillment.speech);
    })
  }

  processQuery = (intent, contexts, action, incomplete, params, speech) => {
    // Entity 추출
    let entity = {
      targetCharacter: [],
      houseType: [],
      lifeCylces: [],
      desire: [],
      sido: "",
      sigungu: "",
      keyword: "",
      sex: "",
      age: "",
    };

    for (var key in params) {
      // Pregnant => 임산부, 육아 관련 Entity 
      if (key == "Pregnant" && params[key] !== "") {
        entity["targetCharacter"].push("003");
      }
      // Bohun => 국가유공자 관련 Entity
      if (key == "Bohun" && params[key] !== "") {
        entity["targetCharacter"].push("005");
      }
      // LifeCycle => 생애주기 관련
      if (key == "LifeCycle" && params[key] !== "") {
        entity["lifeCylces"].push(this.state.lifeCylcesMap[params[key]]);
      }
      // HouseType => 주거 타입 관련 
      if (key == "HouseType" && params[key] !== "") {
        entity["houseType"].push(this.state.desireMap[params[key]]);
      }
      // Desire => 욕구 관련
      if (key == "Desire" && params[key] !== "") {
        entity["desire"].push(this.state.desireMap[params[key]]);
      }
      // Employment => 고용 관련
      if (key == "Employment" && params[key] !== "") {
        entity["desire"].push("");
      }
      if (key == "SearchRequest" && params[key] !== "") {
        console.log(speech.split(params[key]));
        entity.keyword = speech.split(params[key])[0];
      }
      if (key == "SiGunGu" && params[key] !== "") {
        entity["sigungu"] = params[key];
      }
      if (key == "SiDo" && params[key] !== "") {
        entity["sido"] = params[key];
      }
      if (key == "Age" && params[key] !== "") {
        entity["age"] = params[key];
      }
    }

    // console.log(entity)

    // Intent 처리
    switch (intent) {
      // SearchRequestIntent
      case ChatBotModel.ChatBotIntent.SearchRequestIntent:
        WelfareModel.getWelfareWithKeyword(entity.keyword, 1, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }
          console.log(res);
          this.addChatBotResultMsg(res.results, entity.keyword, undefined);
        });
        break;
      // LifeCylceBasedRequest
      case ChatBotModel.ChatBotIntent.LifeCyclelBasedRequest:
      let lifeCycleBasedQS = "?lifeCycle=" + entity.lifeCycles[0];
        WelfareModel.getWelfareWithQS(lifeCycleBasedQS, 1, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }
          this.addChatBotResultMsg(res.result, undefined, lifeCycleBasedQS);
        });
        break;
      // CategoryBasedRequest
      case ChatBotModel.ChatBotIntent.CategoryBasedRequest:
        WelfareModel.getWelfareWithQS("?desire=" + entity.desire[0], 1, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }
          console.log(res);
          this.addChatBotResultMsg(res.results, undefined, "desire=" + entity.desire[0]);
        });
        break;
      // AgeBasedRequest
      case ChatBotModel.ChatBotIntent.AgeBasedRequest:
        console.log(entity.age);
        let ageBasedQS = "?lifeCycle=" + this.ageToLifeCycle(entity.age);
        WelfareModel.getWelfareWithQS(ageBasedQS, 1, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }
          console.log(res);
          this.addChatBotResultMsg(res.results, undefined, ageBasedQS);
        });
        break;
      // RegionBasedRequest
      case ChatBotModel.ChatBotIntent.RegionBasedRequest:
        console.log(entity.sido, entity.sigungu);
        let addressId = this.state.addressNameMap[entity.sido][entity.sigungu];        
        queryString = "?welAddressId=" + addressId;
        WelfareModel.getWelfareWithQS(queryString, 1, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }

          console.log(res);
          this.addChatBotResultMsg(res.results, undefined, queryString);
        });
        break;
      // ComplexWelfareRequest
      case ChatBotModel.ChatBotIntent.ComplexWelfareRequest:
        let complexQS = "";

        break;
      // DefaultWelfareRequest
      case ChatBotModel.ChatBotIntent.DefaultWelfareRequest_Request_Region:
        if (action == "RequestOutput" && !incomplete) {
          let addressId = this.state.addressNameMap[entity.sido][entity.sigungu];
          let lifeCylceId = this.ageToLifeCycle(entity.age);
          let queryString = "?welAddressId=" + addressId + "&lifeCycle=" + lifeCylceId;
          console.log(queryString);
          WelfareModel.getWelfareWithQS(queryString, 1, (err, res) => {
            if (err) {
              console.log(err);
              return;
            }
            console.log(res);
            this.addChatBotResultMsg(res.results, undefined, queryString);
          });
        }
        break;
      // else
      default:
        break;
    }
  }

  ageToLifeCycle = (age) => {
    switch (age) {
      case "10대":
        return "003";
      case "20대":
        return "004";
      case "30대":
        return "004";
      case "40대":
        return "005";
      case "50대":
        return "005";
      case "60대":
        return "005";
      case "70대":
        return "006";
    }
  }
  
  // 서버로부터 이전 챗봇로그 가져옴
  getChatLogs = () => {
//    ChatBotModel.getChatLogs();
  }

  // 서버로 사용자 질의 로그 전송
  postChatLogs = () => {
//    ChatBotModel.postChatLogs();
  }

  // 서버로 챗봇 답변 로그 전송

  // 전송버튼 클릭시
  onSendClick = (textInput) => {
    this.setState({textInput: ""});
    this.addUserMsg(textInput);
    this.queryToDialogFlow(textInput);
  }

  onMessageListClick = (pos) => {
    this.onSendClick(this.state.initSendList[pos]);
  }

  onMessageListResultClick = (pos) => {
    //this.props.navigation.navigate();
  }

  onMoreButtonClick = (params) => {
    console.log(params);
    this.props.navigation.navigate("List", {
      'keyword': params.keyword,
      'queryString': params.queryString,
    });
  }

  render() {
  	return (
	 	 <ChatBotScreen
        ref='_chatbotscreen'
        msgData={this.state.msgData}
        onChatBotMsgAddComplete={this.onChatBotMsgAddComplete}
        onChangeText={this.onChangeText}
        textInput={this.state.textInput}
        onSendClick={this.onSendClick}
			  onPress={this.onPress}
        onMessageListClick={this.onMessageListClick}
        onMessageListResultClick={this.onMessageListResultClick}
        onMoreButtonClick={this.onMoreButtonClick}
		  >
		</ChatBotScreen>
	);
  }
}
