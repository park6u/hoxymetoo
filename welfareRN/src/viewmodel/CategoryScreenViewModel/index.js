/*
프로그램 ID:AC-2020-JS
프로그램명:CategoryScreen/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.10.28
버전:0.6
설명
*/

import React, {Component} from 'react';
import CategoryScreen from 'src/view/screen/CategoryScreen';
import {DrawerActions} from 'react-navigation-drawer';
import BasicText from 'src/view/components/BasicText';
import NotificationIcon from 'src/view/components/NotificationIcon';
import DrawerIcon from 'src/view/components/DrawerIcon';

export default class CategoryScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation  }) => {

    const {state} = navigation;

    if(state.params != undefined){
        return { 
        	headerTitle: <BasicText title="혹시나도" size={18}/>,
            headerLeft: <DrawerIcon openDrawer={state.params.onNavigationClick}/>,
            headerRight: <NotificationIcon onClick={state.params.onAlarmListClick}/>,
            headerStyle: {
              elevation: 0,
              borderBottomWidth: 0,
            }
        }
    }
    
  };

  constructor(props) {
  	super(props);
    this.state = {searchKeyword: ""};
  	this.props.navigation.setParams({
  	  onNavigationClick: this.onNavigationClick,
      onAlarmListClick: this.onAlarmListClick,
  	});
  }

  onCategoryClick = (category) => {
    this.props.navigation.navigate('List', {
      'category': category
    });
  }

  onNavigationClick = () => {
	  this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  onAlarmListClick = () => {
    this.props.navigation.navigate('Alarm');
  }

  onChatBotClick = () => {
    this.props.navigation.navigate('ChatBot');    
  }

  onWelfareSearchIconClick = (keyword) => {      
    this.props.navigation.navigate('List', {
      'keyword': keyword
    });
  }

  onSearchKeywordChange = (text) => {
    this.setState({searchKeyword: text});
  }

  render() {
	return (
		<CategoryScreen
			onPress={this.onPress}
      onWelfareSearchIconClick={this.onWelfareSearchIconClick}
      onChatBotClick={this.onChatBotClick}
      onCategoryClick={this.onCategoryClick}
      onSearchKeywordChange={this.onSearchKeywordChange}
      searchKeyword={this.state.searchKeyword}
		>
		</CategoryScreen>
	);
  }
}
