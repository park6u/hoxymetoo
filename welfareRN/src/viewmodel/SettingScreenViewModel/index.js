import React, {Component} from 'react';
import {Alert} from 'react-native';
import SettingScreen from 'src/view/screen/SettingScreen';
import {DrawerActions} from 'react-navigation-drawer';
import {NavigationActions} from 'react-navigation';
import BasicText from 'src/view/components/BasicText';
import BackIcon from 'src/view/components/BackIcon';
import UserModel from 'src/model/UserModel';

export default class SettingScreenViewModel extends React.Component {
  static navigationOptions = ({ navigation  }) => {

    const {state} = navigation;

    onBackIconClick = () => {
      const backAction = NavigationActions.back();
      navigation.dispatch(backAction);
    }

//    if(state.params != undefined){
        return { 
        	headerTitle: <BasicText title="설정" size={18}/>,
          headerLeft: <BackIcon onPress={onBackIconClick} />,
          headerStyle: {
            elevation: 0,
            borderBottomWidth: 0,
          }
        }
//    }
  };

  constructor(props) {
  	super(props);
    this.state = 
      {settingList: [{title: "앱 정보", subTitle: "0.8", index: 0}, 
      {title: "오픈소스 라이선스", index: 1}, 
      {title: "로그아웃", index: 2}, 
      {title: "문의하기", index: 3}]};
  }

  onSettingPress = (pos) => {
    switch(pos) {
      case 1:
        // 오픈소스 라이선스
        this.props.navigation.navigate('OpenSource');
        break
      case 2:
        // 로그아웃
        Alert.alert("로그아웃", "로그아웃 하시겠습니까?", [
          {
            text: '취소',
            style: 'cancel',
          },
          {text: '확인', onPress: () => {this.logOut()}},
        ]);
        break;
      case 3:
        // 문의하기
        break;
      default:
        break;
    }
  }

  logOut = () => {
    UserModel.deleteInternalMemkey(() => {
      UserModel.deleteInternalName(() => {
        this.props.navigation.navigate('Login');
      });      
    });
  }
 
  render() {
	  return (
		  <SettingScreen
        data={this.state.settingList}
        onSettingPress={this.onSettingPress}
		  >
		  </SettingScreen>
	  );
  }
}
