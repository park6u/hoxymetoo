const baseURL = "https://hoxymetoo.com";
const api = "/api";
const member = "/members";
const welfare = "/welfares";
const sido = "/sido";
const sigungu = "/sigungu";
const address = "/address";
const lifeCycle = "/lifecycles";
const targetCharacter = "/targetcharacters";
const desire = "/desires";
const houseTypes = "/housetypes";
const disabletypes = "/disabletypes";
const chatlogs = "/chatlogs";
const feedback = "/feedbacks";
const registerMember = baseURL + api + member;
const getMember = baseURL + api + member + "/";
const putMember = baseURL + api + member + "/";
const deleteMember = baseURL + api + member + "/";
const getWelfares = baseURL + api + welfare;
const getSiDo = baseURL + api + sido;
const getSiGunGu = baseURL + api + sigungu;
const getAddress = baseURL + api + address + "/";
const getAllAddress = baseURL + api + address;
const getTargetCharacters = baseURL + api + targetCharacter;
const getDesire = baseURL + api + desire;
const getLifeCycle = baseURL + api + lifeCycle;
const getHouseTypes = baseURL + api + houseTypes;
const getDisabled = baseURL + api + disabletypes;

const getChatLogs = baseURL + api + chatlogs;
const postChatLogs = baseURL + api + chatlogs;

const postFeedback = baseURL + api + feedback;

// DialogFlow v1
const dialogFlowBaseURL = "https://api.dialogflow.com/v1/query";

const Base  = {
	BASE_URL: baseURL,
	API: api,
	MEMBER: member,
	WELFARE: welfare,
	REGISTER_MEMBER: registerMember,
	GET_MEMBER: getMember,
	PUT_MEMBER: putMember,
	DELETE_MEMBER: deleteMember,
	GET_WELFARES: getWelfares,
	GET_SIDO: getSiDo,
	GET_SIGUNGU: getSiGunGu,
	GET_ADDRESS: getAddress,
	GET_ALL_ADDRESS: getAllAddress,
	GET_TARGET_CHARACTERS: getTargetCharacters,
	GET_DESIRE: getDesire,
	GET_HOUSE_TYPES: getHouseTypes,
	GET_DISABLED: getDisabled,
	GET_LIFECYCLE: getLifeCycle,
	GET_DIALOGFLOW: dialogFlowBaseURL,
	POST_CHATLOGS: postChatLogs,
	GET_CHATLOGS: getChatLogs,
	POST_FEEDBACK: postFeedback,
}

export default Base;