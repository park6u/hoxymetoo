import Base from 'src/model/base.js';
import Key from 'src/model/key.js';
import AsyncStorage from '@react-native-community/async-storage';

const getHouseType = async (callback) => {
  fetch(GET_HOUSE_TYPES, {
  	method: 'GET',
  	headers: {
	  Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
  .then((resp) => {
  	// 400 ~ 500
  	if (resp.status >= 400 && resp.status <= 500) {
  		let err = new Error(resp.statusText);
  		err.response = resp;
  		throw err;
  	}
  	return resp;
  })
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {
  	callback(err);
  });
}

const getDesires = async (callback) => {
  fetch(GET_DESIRE, {
  	method: 'GET',
  	headers: {
	  Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
  .then((resp) => {
  	// 400 ~ 500
  	if (resp.status >= 400 && resp.status <= 500) {
  		let err = new Error(resp.statusText);
  		err.response = resp;
  		throw err;
  	}
  	return resp;
  })
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {
  	callback(err);
  });
}

const getTargetcharacters = async (callback) => {
  fetch(GET_TARGET_CHARACTER, {
  	method: 'GET',
  	headers: {
	  Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
  .then((resp) => {
  	// 400 ~ 500
  	if (resp.status >= 400 && resp.status <= 500) {
  		let err = new Error(resp.statusText);
  		err.response = resp;
  		throw err;
  	}
  	return resp;
  })
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {
  	callback(err);
  });
}

const InfoModel = {
  getHouseType,
  getDesires,
  getTargetcharacters,

}

export default InfoModel;