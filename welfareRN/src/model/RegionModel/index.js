
import Base from 'src/model/base.js';
import Key from 'src/model/key.js';
import AsyncStorage from '@react-native-community/async-storage';

const respErr = (resp) => {
    // 400 ~ 500
    // console.log(resp.status);
    if (resp.status >= 400 && resp.status <= 500) {
      let err = new Error(resp.statusText);
      err.response = resp;
      throw err;
    }
    return resp;
  };

// 시도 정보 가져오기
const getSiDo = async (page, callback) => {
  fetch(Base.GET_SIDO + '?page=' + page, {
  	method: 'GET',
  	headers: {
	  Accept: 
      'application/json',
      'Content-Type': 'application/json',
    },
  })
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {
  	callback(err);
  });
}

// 시군구 정보 가져오기
const getSiGunGu = async (page, sidoid, callback) => {
  fetch(Base.GET_SIGUNGU + '?page=' + page + '&sidoid=' + sidoid, {
    method: 'GET',
    headers: {
      Accept: 
        'application/json',
        'Content-Type': 'application/json',
    }
  })
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });;
}

// AddressId로 해당 Address 정보를 가져옴 (sidoid, sigunguid)
const getAddressWithid = async (id, callback) => {
  fetch(Base.GET_ADDRESS + id, {
    method: 'GET',
    headers: {
      Accept: 
        'application/json',
        'Content-Type': 'application/json',
    }
  })
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, err);
  })
  .catch((err) => {
    callback(err);
  })
}

// getAllAddress의 서브 메서드 (모든 Address를 불러올 때까지 재귀적으로 호출함)
const _getNextAddress = async (callback, page=1) => {
  fetch(Base.GET_ALL_ADDRESS + '?page=' + page, {
    method: 'GET',
    headers: {
      Accept: 
        'application/json',
        'Content-Type': 'application/json',      
    }
  })
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(respJson);
    if (respJson.next != null) {
       _getNextAddress(callback, page+1);
     }
  });
}

// 모든 Address를 가져옴
const getAllAddress = async (callback) => {
  const res = {results: []};
  _getNextAddress((respJson) => {
    res.results = res.results.concat(respJson.results);
    if (res.results.length == respJson.count) {
      saveInternalAddressMap(res);
      callback(res);
    }
  });
}

// 내부저장소에 (sidoId, sigunguId) => (addressId) Map을 저장
const saveInternalAddressMap = async (res) => {
  let addrIdMap = {};
  let addrNameMap = {};
  let sidoSigunguList = [];

  for (var i = 0; i < res.results.length; ++i) {
    addrIdMap[res.results[i].siDoId] = {};
    sidoSigunguList.push(res.results[i].siDoSiGunGuName.split(" "));
    addrNameMap[sidoSigunguList[i][0]] = {};
  }

  for (var i = 0; i < res.results.length; ++i) {  
    addrIdMap[res.results[i].siDoId][res.results[i].siGunGuId] = res.results[i].addressId;
    addrNameMap[sidoSigunguList[i][0]][sidoSigunguList[i][1]] = res.results[i].addressId;
  }

  const addrNameReturn = await AsyncStorage.setItem(Key.ADDRESS_NAME_KEY, JSON.stringify(addrNameMap));
  const addrIdReturn = await AsyncStorage.setItem(Key.ADDRESS_ID_KEY, JSON.stringify(addrIdMap));
}

// sidoId 와 sigunuguId 로 해당하는 AddressId값을 반환
const getAddressId = async (sidoid, sigunguid ,callback) => {
  const addrMap = await AsyncStorage.getItem(Key.ADDRESS_ID_KEY);
  let map = JSON.parse(addrMap);
  callback(map[sidoid][sigunguid]);
}

const getAddressIdMap = async (callback) => {
  const addrMap = await AsyncStorage.getItem(Key.ADDRESS_ID_KEY);
  callback(addrMap);
}

const getAddressNameMap = async (callback) => {
  const addrMap = await AsyncStorage.getItem(Key.ADDRESS_NAME_KEY);
  callback(addrMap);
}

const RegionModel = {
  getSiDo,
  getSiGunGu,
  getAddressWithid,
  getAllAddress,
  getAddressId,
  getAddressIdMap,
  getAddressNameMap,
}

export default RegionModel;
