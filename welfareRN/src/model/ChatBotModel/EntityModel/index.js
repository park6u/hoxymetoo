import Base from 'src/model/base.js';
import Key from 'src/model/key.js';
import AsyncStorage from '@react-native-community/async-storage';

const respErr = (resp) => {
    // 400 ~ 500
    if (resp.status >= 400 && resp.status <= 500) {
      let err = new Error(resp.statusText);
      err.response = resp;
      throw err;
    }
    return resp;
  };

const fetchConfig = {
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  },
};

const getDesire = async (callback) => {
  fetch(Base.GET_DESIRE, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
  	saveInternalDesire(respJson.results);
  	callback(null ,respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

const getHouseTypes = async (callback) => {
  fetch(Base.GET_HOUSE_TYPES, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
  	saveInternalHouseTypes(respJson.results);
  	callback(null ,respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

const getLifeCycle = async (callback) => {
  fetch(Base.GET_LIFECYCLE, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
  	saveInternalLifeCycle(respJson.results);
  	callback(null ,respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

const getTargetCharacters = async (callback) => {
  fetch(Base.GET_TARGET_CHARACTERS, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
  	saveInternalTargetCharacters(respJson.results);
  	callback(null ,respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

const getDisabled = async (callback) => {
  fetch(Base.GET_DISABLED, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    saveInternalTargetCharacters(respJson.results);
    callback(null ,respJson);
  })
  .catch((err) => {
    callback(err);
  });  
}

const saveInternalDesire = async (res) => {
  let desireMap = {};
  for (var i = 0; i < res.length; ++i ){
  	desireMap[res[i].desireName] = res[i].desireId;
  }
  const ret = await AsyncStorage.setItem(Key.DESIRE_KEY, JSON.stringify(desireMap));
};

const saveInternalLifeCycle = async (res) => {
  let lifeCycleMap = {};
  for (var i = 0; i < res.length; ++i) {
  	lifeCycleMap[res[i].lifeCycleName] = res[i].lifeCycleId;
  }
  const ret = await AsyncStorage.setItem(Key.LIFECYCLE_KEY, JSON.stringify(lifeCycleMap));
};

const saveInternalHouseTypes = async (res) => {
  let houseTypesMap = {};
  for (var i = 0; i < res.length; ++i) {
  	houseTypesMap[res[i].houseTypeName] = res[i].houseTypeId;
  }
  const ret = await AsyncStorage.setItem(Key.HOUSE_TYPES_KEY, JSON.stringify(houseTypesMap));
};

const saveInternalTargetCharacters = async(res) => {
  let targetCharactersMap = {};
  for (var i = 0; i < res.length; ++i) {
  	targetCharactersMap[res[i].targetCharacterName] = res[i].targetCharacterId;
  }
  const ret = await AsyncStorage.setItem(Key.TARGET_CHARACTERS_KEY, JSON.stringify(targetCharactersMap));
};

const saveInternalDisabled = async (res) => {
  let disabledMap = {};
  for (var i = 0; i < res.length; ++i) {
    disabledMap[res[i].disableTypeName] = res[i].disableTypeId;
  }
  const ret = await AsyncStorage.setItem(Key.DISABLED_KEY, JSON.stringify(disabledMap));
};

const getDesireIdMap = async (callback) => {
  const desireMap = await AsyncStorage.getItem(Key.DESIRE_KEY);
  callback(desireMap);
};

const getHouseTypesIdMap = async (callback) => {
  const houseTypesMap = await AsyncStorage.getItem(Key.HOUSE_TYPES_KEY);
  callback(houseTypesMap);
};

const getLifeCycleIdMap = async (callback) => {
  const lifeCycleMap = await AsyncStorage.getItem(Key.LIFECYCLE_KEY);
  callback(lifeCycleMap);
};

const getTargetCharactersIdMap = async (callback) => {
  const targetCharactersMap = await AsyncStorage.getItem(Key.TARGET_CHARACTERS_KEY);
  callback(targetCharactersMap);
};

const getDisabledIdMap = async (callback) => {
  const disabledMap = await AsyncStorage.getItem(Key.DISABLED_KEY);
  callback(disabledMap);
};

const getDesireId = async (desire, callback) => {
  const desireMap = await AsyncStorage.getItem(Key.DESIRE_KEY);
  callback(desireMap[desire]);
};

const getHouseTypesId = async (houseType, callback) => {
  const houseTypesMap = await AsyncStorage.getItem(Key.HOUSE_TYPES_KEY);
  callback(houseTypesMap[houseType]);
};

const getLifeCycleId = async (lifeCycle, callback) => {
  const lifeCycleMap = await AsyncStorage.getItem(Key.LIFECYCLE_KEY);
  callback(lifeCycleMap[lifeCycle]);
};

const getTargetCharactersId = async (targetCharacters, callback) => {
  const targetCharactersMap = await AsyncStorage.getItem(Key.TARGET_CHARACTERS_KEY);
  callback(targetCharactersMap[targetCharacters]);
};

const getDisabledId = async (disabled, callback) => {
  const disabledMap = await AsyncStorage.getItem(Key.DISABLED_KEY);
  callback(disabledMap[disabled]);
};

const EntityModel = {
  getDesire,
  getHouseTypes,
  getLifeCycle,
  getTargetCharacters,
  getDisabled,
  getDesireIdMap,
  getHouseTypesIdMap,
  getLifeCycleIdMap,
  getTargetCharactersIdMap,
  getDisabledIdMap,
  getDesireId,
  getHouseTypesId,
  getLifeCycleId,
  getTargetCharactersId,
  getDisabledId,
}

export default EntityModel;