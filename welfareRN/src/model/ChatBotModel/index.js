/*
프로그램 ID:AC-2020-JS
프로그램명:ChatBotModel/index.js
작성자:박준우(pkjoho95@gmail.com)
버전:0.6
설명
 - 챗봇 스크린 ViewModel
*/

import Base from 'src/model/base.js';
import Key from 'src/model/key.js';
import AsyncStorage from '@react-native-community/async-storage';

const respErr = (resp) => {
    // 400 ~ 500
    if (resp.status >= 400 && resp.status <= 500) {
      let err = new Error(resp.statusText);
      err.response = resp;
      throw err;
    }
    return resp;
  };

// DialogFlow 쿼리 응답 Intent
const ChatBotIntent = {
  SearchRequestIntent: "SearchRequestIntent",
  RegionBasedRequest: "RegionBasedWelfareRequest",
  DefaultWelfareRequest: "DefaultWelfareRequest",
  DefaultWelfareRequest_Request_Region: "DefaultWelfareRequest-Request-Region",
  DefaultFallbackRequest: "DefaultFallbackRequest",
  ComplexWelfareRequest: "ComplexWelfareRequest",
  CategoryBasedRequest: "CategoryBasedRequest",
  ComplexWelfareRequest: "ComplexWelfareRequest",
  AgeBasedRequest: "AgeBasedWelfareRequest",
  DefaultWelfareRequest: "DefaultWelfareRequest",
  IncomeBasedRequest: "IncomeBasedRequest",
  LifeCyclelBasedRequest: "LifeCyclelBasedRequest",
  // DefaultWelfareRequest-Request-Age: "DefaultWelfareRequest-Request-Age",
  // DefaultWelfareRequest-Request-Region: "DefaultWelfareRequest-Request-Region"
}

// DialogFlow에 채팅내용 쿼리
const queryToDF = async (queryString, callback) => {
  var url = new URL(Base.GET_DIALOGFLOW);  
  const params = {
    v: 20150910,
    lang: 'ko',
    timeZone: 'Asia/Seoul',
    sessionId: 12345,
    query: encodeURI(queryString),
  };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer 326f4c35f14442c596c7c47e93bd82c7', 
    },
  })
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
}

const getChatLogs = async (queryString, memkey, callback) => {
  var url = new URL(Base.GET_CHATLOGS);

  const params = {
    page: 1,
    senderMemeky: memkey,
  };

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  fetch(url, {
    method
  })
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
}

const postChatLogs = async (content, memkey, callback) => {

}

const ChatBotModel = {
  getChatLogs,
  postChatLogs,
  queryToDF,
  ChatBotIntent,
}

export default ChatBotModel;
