/*
프로그램 ID:AC-2020-JS
프로그램명: WelfareModel/index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:2019.11.28
버전:0.6
설명
*/

import Base from 'src/model/base.js';
import Key from 'src/model/key.js';
import AsyncStorage from '@react-native-community/async-storage';

const respErr = (resp) => {
    // 400 ~ 500
    if (resp.status >= 400 && resp.status <= 500) {
      let err = new Error(resp.statusText);
      err.response = resp;
      throw err;
    }
    return resp;
  };

const fetchConfig = {
    method: 'GET',
    headers: {
    Accept: 'application/json',
      'Content-Type': 'application/json',
    },
};

// 사용자별 맞춤 추천 복지 아이디 리스트 가져오기
const getMemberWelfare = async (memKey, callback) => {
  fetch(Base.GET_MEMBER + memKey, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson.welfares);
  })
  .catch((err) => {
    callback(err);
  });
};

// 해당 id 복지정보 가져오기
const getWelfareWithId = async (id, callback) => {
  fetch(Base.GET_WELFARES  + '/' +  id, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

// 복지정보 가져오기; 페이지 순서대로
const getWelfare = async (page, callback) => {
  fetch(Base.GET_WELFARES + '?page=' + page, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {
  	callback(err);
  });
};

// 복지정보 가져오기; QueryString으로
const getWelfareWithQS = async (queryString, page, callback) => {
  fetch(Base.GET_WELFARES + queryString + "&page=" + page , fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

// 복지정보 가져오기; Search Keyword로
const getWelfareWithKeyword = async (keywords, page, callback) => {
  fetch(Base.GET_WELFARES + '?search=' + keywords + "&page=" + page, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

const getWithURL = async (url, callback) => {
  fetch(url, fetchConfig)
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson); 
  })
  .catch((err) => {
    callback(err);  
  })
}

const WelfareModel = {
  getWelfare,
  getMemberWelfare,
  getWelfareWithId,
  getWelfareWithQS,
  getWelfareWithKeyword,
  getWithURL,
};

export default WelfareModel;