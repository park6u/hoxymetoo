export const REFRESH = 'REFRESH';

export function drawerChanged() {
  return {
    type: REFRESH
  };
};