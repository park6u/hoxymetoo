import { REFRESH } from '../DrawerAction';
import { combineReducers } from 'redux';

const initState = {
  count: 0
};

const refresh = (state=initState, action) => {
  switch(action.type) {
  	case REFRESH:
      return Object.assign({}, state, {
        count: state.count + 1
      });
    default:
      return state;
  }
};

const drawer = combineReducers({
  refresh
});

export default drawer;