
import Base from 'src/model/base.js';
import Key from 'src/model/key.js';
import AsyncStorage from '@react-native-community/async-storage';

const fetchConfig = (_method, _body) => {
  let config = {
    method: _method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
  }
  if (_body) {
    config.body = _body;
  }
  return config;
};

const respErr = (resp) => {
    // 400 ~ 500
    if (resp.status >= 400 && resp.status <= 500) {
      let err = new Error(resp.statusText);
      err.response = resp;
      throw err;
    }
    return resp;
};

// 디바이스 내부에 memkey를 저장
const registerInternalMemkey = async (memkey, callback) => {
  const ret = await AsyncStorage.setItem(Key.MEM_KEY, memkey);
}

// 디바이스 내부에 name을 저장
const registerInternalName = async (name, callback) => {
  const ret = await AsyncStorage.setItem(Key.NAME, name);
}

// 디바이스 내부에 passowrd 저장
const registerInternalPassword = async (password, callback) => {
  const ret = await AsyncStorage.setItem(Key.PASSOWRD, password);
}

// 디바이스 내부에 저장된 memkey를 가져옴
const getInternalMemkey = async (callback) => {
	const memkey = await AsyncStorage.getItem(Key.MEM_KEY);
	callback(memkey);
};

// 디바이스 내부에 저장되 memkey를 삭제
const deleteInternalMemkey = async (callback) => {
  const memkey = await AsyncStorage.removeItem(Key.MEM_KEY);
  callback();
}

// 디바이스 내부에 저장된 name을 가져옴
const getInternalName = async (callback) => {
	const name = await AsyncStorage.getItem(Key.NAME);
	callback(name);
};

// 디바이스 내부의 이름 삭제
const deleteInternalName = async (callback) => {
  const name = await AsyncStorage.removeItem(Key.NAME);
  callback();
};

// 디바이스 내부에 password를 가져옴
const getInternalPassword = async (callback) => {
  const password = await AsyncStorage.getItem(Key.PASSOWRD);
  callback(password);
};

// 디바이스 내부의 password 삭제
const deleteInternalPassword = async (callback) => {
  const password = await AsyncStorage.removeItem(Key.NAME);
  callback(password);
};

/*
	info : {
		pushToken,
		email,
		birthday,
		gender,
		family,
		income,
		bohun,
		sido,
		sigungu,
	}
*/

// 서버로에 socailid를 입력해 memkey를 가져옴; 로그인
const getMember = async (memkey, callback) => {
  fetch(Base.GET_MEMBER + memkey, fetchConfig("GET"))
  .then((resp) => {
	  // 401 BAD REQUEST 이미 socialId가 존재
	  if (resp.status == 400) {
	  	let err = new Error(resp.statusText);
      console.log(resp.status, "Already Exist");
	  	err.response = resp;
	  	callback(resp.status);
	  }
	  // 200 OK
	  if (resp.status == 200) {
	  	return resp;
	  }
	})
	.then((resp) => resp.json())
	.then((respJson) => {
	  callback(null, respJson);
	})
	.catch((err) => {
	  callback(err);
	});
}

// 회원 등록; 회원가입
const registerUser = async (info, callback) => {
  fetch(Base.REGISTER_MEMBER, fetchConfig("POST", JSON.stringify(info)))
  	.then((resp) => {
  	  // 401 BAD REQUEST 이미 socialId가 존재
      if (resp.status == 400) {
      	let err = new Error(resp.statusText);
      	err.response = resp;
      	throw err;
      }

      // 200 OK 성공적으로 만들어짐
      if (resp.status == 200) {
      	console.log('AC-20-JS: created successfully');
      	return resp;
      }
  	})
  	.then((resp) => resp.json())
  	.then((respJson) => {
  	  // console.log('AC-20-JS: '+ respJson);
  	  registerInternalMemkey(respJson.memKey);
  	  callback(null, respJson);
  	})
  	.catch((err) => {
  	  callback(err);
  	});
};

// 가입 여부 판단
const postSocialId = async (_socialId, callback) => {
  fetch(Base.REGISTER_MEMBER + '?socialId=' + _socialId, fetchConfig("GET"))
  .then((resp) => {
    if (resp.status == 404) {
      return resp;
    }
    return resp;
  })
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {
  	callback(err);
  });
}

const getUser = async (memkey) => {
  if (memkey == undefined) return; 
  fetch(Base.GET_USER + memKey, fetchConfig("GET"))
  .then((resp) => resp.json())
  .then((respJson) => {
  	callback(null, respJson);
  })
  .catch((err) => {

  }); ;
}

// 회원정보 수정
/*
    "socialId": "kakao_pkjoho95@naver.com", (required)
    "disables": [], (optional)
    "houseTypes": [], (optional)
    "desires": [], (optional)
    "targetCharacters": [], (optional)
    "lifeCycles": [], (optional)
    "families": [], (optional)
    "memBirthday": "19980405", (optional)
    "memGender": "1", (optional)
    "memFamily": "1", (optional)
    "memIncome": "1", (optional)
    "memBohun": "1", (optional)
*/
const putMember = async (memKey, info, callback) => {
  if (memKey == undefined) return;
  console.log(JSON.stringify(info));
  fetch(Base.PUT_MEMBER + memKey, fetchConfig("PUT", JSON.stringify(info)))
  .then((resp) => {
    console.log(resp);
    // 400 ~ 500
    if (resp.status >= 400 && resp.status <= 500) {
      let err = new Error(resp.statusText);
      err.response = resp;
      throw err;
    }
    return resp;
  })
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

// 회원정보 삭제
const deleteMember = async (memkey) => {
  if (memkey == undefined) return;
  fetch(Base.DELETE_USER, this.fetchConfig("DELETE"))
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson);
  })
  .catch((err) => {
    callback(err);
  });
};

const postFeedBack = async (memKey, callback) => {
  if (memkey == undefined) return;
  fetch(Base.POST_FEEDBACK, this.fetchConfig())
  .then(respErr)
  .then((resp) => resp.json())
  .then((respJson) => {
    callback(null, respJson)
  })
  .catch((err) => {
    callback(err);
  });
}


const UserModel = {
	getInternalMemkey,
	registerInternalMemkey,
	registerInternalName,
  deleteInternalMemkey,
  deleteInternalName,
	postSocialId,
	getMember,
  deleteMember,
  putMember,
	getInternalName,
	registerUser,
}

export default UserModel;
