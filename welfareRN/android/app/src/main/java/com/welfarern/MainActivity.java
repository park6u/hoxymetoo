package com.welfarern;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.ReactActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "welfareRN";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
      return new ReactActivityDelegate(this, getMainComponentName()) {
        @Override
        protected ReactRootView createRootView() {
            //byte[] pair = {0x38, (byte)0xB4, (byte)0xF6, (byte)0xAA, 0x0F, (byte)0xA1, (byte)0xFB, 0x07, (byte)0xD7, 0x5C, 0x63, (byte)0xEC, 0x6A, 0x01, (byte)0xB4, (byte)0xE2, (byte)0xD7, 0x11, (byte)0xAF, 0x34};
            //Log.d("tag", Base64.encodeToString(pair, Base64.NO_WRAP));
         return new RNGestureHandlerEnabledRootView(MainActivity.this);
        }
      };
    }

    // Code for Hash Key (for Kakao Login Authentication)
    // @Override
    // protected void onCreate(Bundle savedInstanceState) {
    //     super.onCreate(savedInstanceState);
    //     try {
    //         Log.d("rndebug", getKeyHash(this));
    //     } catch (PackageManager.NameNotFoundException e){
    //         e.printStackTrace();
    //     }
    // }

    // public static String getKeyHash(final Context context) throws PackageManager.NameNotFoundException {
    //     PackageManager pm = context.getPackageManager();
    //     PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
    //     if (packageInfo == null)
    //         return null;

    //     for (Signature signature : packageInfo.signatures) {
    //         try {
    //             MessageDigest md = MessageDigest.getInstance("SHA");
    //             md.update(signature.toByteArray());
    //             return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
    //         } catch (NoSuchAlgorithmException e) {
    //         }
    //     }
    //     return null;
    // }

}
