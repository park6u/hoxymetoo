Overview Of Application
=============

## App Architecture
- MVVM
	- MODEL
	- VIEWMODEL
	- VIEW

- Redux

## Code Convention
- Main Comment
~~~
/*
프로그램 ID:AC-NNNN-JS
프로그램명:index.js
작성자:박준우(pkjoho95@gmail.com)
생성일자:YYYY.MM.DD
버전:0.X
설명
- (DESC...)
*/
~~~

## Indentation
- 2 Spacebar (JS)
- Tab with 4 space (JSX)

## USED LIBRARY
- [react-navigation](https://reactnavigation.org/en/)
- [react-native-kakao-logins](https://github.com/react-native-seoul/react-native-kakao-login)
- [react-native-device-info](https://github.com/react-native-community/react-native-device-info)
- [react-native-google-singin](https://github.com/react-native-community/react-native-google-signin)
- [react-native-naver-login](https://github.com/react-native-seoul/react-native-naver-login)
- [react-native-material-ripple](https://github.com/n4kz/react-native-material-ripple)
- [react-native-material-dropdown]
- [react-native-material-textfield]
- [react-native-linear-gradient](https://github.com/brentvatne/react-native-linear-gradient)
- [react-native-check-box]
- [react-native-fs]
- [react-native-push-notification](https://github.com/zo0r/react-native-push-notification)
- [redux]
- [react-redux]

## Set up & Build

- Overview
execute 

`npm install`

`sh setup.sh`
or 
`setup.bat`

You must follow Post Install instruction before install cocoaPod

`cd ios`

`pod install`

- Socail Login Set up
	- Kakao

	- Naver

	- Google

- In Android Build
	- Debug
	`react-native run-android`

	- Release 
	`react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/`

- In iOS Build
	- Debug
	`react-native run-ios`

	- Release 
	`react-native bundle --entry-file='index.js' --bundle-output='./ios/main.jsbundle' --dev=false --platform='ios' --assets-dest='./ios'`

- Reset Cache
	`react-native start --reset-cache`

## Post Install ..

1. react-native-naver-login

in `node_modules/react-native-naver-login/ios/RNNaverLogin.podspec`

modify react-native-naver-login podspec

`s.homepage = "https://github.com/react-native-seoul/react-native-naver-login.git"`
`s.source = { :git => "https://github.com/author/RNNaverLogin.git", :tag => "master" }`

modify project podspec

`pod 'RNNaverLogin', :podspec => '../node_modules/react-native-naver-login/ios/RNNaverLogin.podspec'`
 
and execute `pod install`

2. react-native-fs

in `node_modules/react-native-fs/RNFSManager.m`

 
`#import <React/RCTImageLoaderProtocol.h>`

replace with
 
`#import <React/RCTImageLoader.h>`

## TODO
 - Networking Multiplexing (Concurrency, Not using For loop in get Region All in RegionModel) 
